from pathlib import Path

import pytest

from yatbaf.input_file import InputFile


def test_no_path_content_error():
    with pytest.raises(ValueError):
        InputFile("file")


def test_path_and_content_error():
    with pytest.raises(ValueError):
        InputFile("file", path="/tmp/tmp.file", content=b"content")


async def read(it):
    result = b""
    async for data in it:
        result += data
    return result


@pytest.mark.asyncio
async def test_iter_file(tmp_path):
    content = "content"
    file_path = tmp_path / "file"
    Path.write_text(file_path, content)

    input_file = InputFile("file", path=file_path)
    result = await read(input_file)
    assert result == content.encode()


@pytest.mark.asyncio
async def test_iter_buffer():
    content = "content"
    input_file = InputFile("file", content=content)
    result = await read(input_file)
    assert result == content.encode()


@pytest.mark.asyncio
async def test_iterator_as_content():

    async def gen():
        yield b"foo"
        yield b"bar"

    input_file = InputFile("file", content=gen())
    result = await read(input_file)
    assert result == b"foobar"
