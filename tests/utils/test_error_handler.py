import pytest

from yatbaf.exceptions import BotException
from yatbaf.exceptions import InvalidTokenError
from yatbaf.utils import get_error_handler

handler1 = object()
handler2 = object()

handlers = {
    BotException: handler1,
    ValueError: handler2,
}


@pytest.mark.parametrize(
    "exc,handler",
    [
        [InvalidTokenError("error"), handler1],
        [ValueError("error"), handler2],
        [ZeroDivisionError("error"), None],
    ]
)
def test_get_handler(exc, handler):
    assert get_error_handler(handlers, exc) is handler


def test_empty():
    assert get_error_handler({}, ValueError("1")) is None
