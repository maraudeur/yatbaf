import sys
from unittest import mock

import httpx
import pytest
import pytest_asyncio
from msgspec.json import encode

from yatbaf.client.multipart import MultipartStream
from yatbaf.config import WebhookConfig
from yatbaf.exceptions import JSONDecodeError
from yatbaf.handlers.base import Result
from yatbaf.input_file import InputFile
from yatbaf.methods import GetMe
from yatbaf.methods import SendDocument
from yatbaf.methods import SendMessage
from yatbaf.runner.webhook.asgi import Asgi
from yatbaf.runner.webhook.asgi import _prepare_response
from yatbaf.runner.webhook.asgi import _read_body
from yatbaf.runner.webhook.asgi import _send_error
from yatbaf.runner.webhook.asgi import _send_response

module_obj = sys.modules["yatbaf.runner.webhook.asgi"]


@pytest.fixture(autouse=True)
def __set_webhook_config(bot_mock):
    bot_mock.config.webhook = WebhookConfig("https://example.com/foo")


CONTENT = b"content"
RAW_HEADERS = (
    (b"content-length", str(len(CONTENT)).encode("ascii")),
    (b"content-type", b"application/json"),
)


def test_no_config(bot_mock):
    bot_mock.config.webhook = None
    with pytest.raises(ValueError):
        Asgi(bot_mock)


def test_prepare_response_empty():
    headers, content = _prepare_response(None)
    assert headers == (
        (b"content-length", b"0"),
        (b"content-type", b"application/json"),
    )
    assert content == b""


@pytest.mark.parametrize(
    "method,result",
    [
        [
            GetMe(),
            b'{"method":"getme"}',
        ],
        [
            SendMessage(chat_id=123, text="foo"),
            SendMessage(
                chat_id=123,
                text="foo",
                _method="sendmessage",
            )._encode_params()[0]
        ],
    ]
)
def test_prepare_response_method(method, result):
    headers, content = _prepare_response(method)
    assert headers == (
        (b"content-length", str(len(result)).encode("ascii")),
        (b"content-type", b"application/json"),
    )
    assert content == result


def test_prepare_response_method_file():
    method = SendDocument(
        chat_id=123,
        document=InputFile(
            name="file.txt",
            content=b"foo-bar.",
        ),
    )
    headers, content = _prepare_response(method)
    assert isinstance(content, MultipartStream)
    assert headers == ((b"content-type", content.content_type.encode()),)


@pytest.mark.asyncio
async def test_response():
    send = mock.AsyncMock()
    await _send_response(send, 200, RAW_HEADERS, CONTENT)
    assert send.mock_calls == [
        mock.call.__call__({
            "type": "http.response.start",
            "status": 200,
            "headers": RAW_HEADERS,
        }),
        mock.call.__call__({
            "type": "http.response.body",
            "body": CONTENT,
            "more_body": False,
        }),
    ]


@pytest.mark.asyncio
async def test_response_stream():
    send = mock.AsyncMock()

    async def stream_gen():
        for _ in range(3):
            yield CONTENT

    await _send_response(send, 200, RAW_HEADERS, stream_gen())

    assert send.mock_calls == [
        mock.call.__call__({
            "type": "http.response.start",
            "status": 200,
            "headers": RAW_HEADERS,
        }),
        mock.call.__call__({
            "type": "http.response.body",
            "body": CONTENT,
            "more_body": True,
        }),
        mock.call.__call__({
            "type": "http.response.body",
            "body": CONTENT,
            "more_body": True,
        }),
        mock.call.__call__({
            "type": "http.response.body",
            "body": CONTENT,
            "more_body": True,
        }),
        mock.call.__call__({
            "type": "http.response.body",
            "body": b"",
            "more_body": False,
        }),
    ]


@pytest.mark.asyncio
async def test_send_error():
    send = mock.AsyncMock()
    await _send_error(send, 403)
    assert send.mock_calls == [
        mock.call.__call__({
            "type": "http.response.start",
            "status": 403,
            "headers": (
                (b"content-length", b"0"),
                (b"content-type", b"application/json"),
            ),
        }),
        mock.call.__call__({
            "type": "http.response.body",
            "body": b"",
            "more_body": False,
        }),
    ]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "resp,body",
    [
        [
            [
                {
                    "type": "http.request",
                    "body": b"content",
                    "more_body": False,
                },
            ],
            b"content",
        ],
        [
            [
                {
                    "type": "http.request",
                    "body": b"content",
                    "more_body": True,
                },
                {
                    "type": "http.request",
                    "body": b"content",
                    "more_body": True,
                },
                {
                    "type": "http.request",
                    "body": b"",
                    "more_body": False,
                },
            ],
            b"contentcontent",
        ],
        [
            [
                {
                    "type": "http.request",
                    "body": b"content",
                    "more_body": True,
                },
                {
                    "type": "http.disconnect",
                },
                {
                    "type": "http.request",
                    "body": b"",
                    "more_body": False,
                },
            ],
            None,
        ],
    ]
)
async def test_read_body(resp, body):
    receive = mock.AsyncMock(side_effect=resp)
    request_body = await _read_body(receive)
    assert request_body == body


@pytest.mark.parametrize(
    "method,path,path_cfg,result",
    [
        ("GET", "/foo", None, (False, 405)),
        ("GET", "/foo", "/bar", (False, 405)),
        ("POST", "/foo", "/bar", (False, 404)),
        ("POST", "/foo", "/foo", (True, None)),
        ("POST", "/foo", None, (True, None)),
    ]
)
def test_match(bot_mock, method, path, path_cfg, result):
    app = Asgi(bot_mock)
    bot_mock.config.webhook.path = path_cfg
    scope = {"method": method, "path": path}
    assert app._match(scope) == result


@pytest.mark.parametrize(
    "headers,secret,result",
    [
        ([], None, True),
        ([], "foobar", False),
        ([(b"x-telegram-bot-api-secret-token", b"foobar")], None, True),
        ([(b"x-telegram-bot-api-secret-token", b"foobar")], "foobar", True),
        ([(b"x-telegram-bot-api-secret-token", b"barbaz")], "foobar", False),
    ]
)
def test_is_authenticated(bot_mock, headers, secret, result):
    app = Asgi(bot_mock)
    bot_mock.config.webhook.secret = secret
    scope = {"headers": headers}
    assert app._is_authenticated(scope) is result


@pytest_asyncio.fixture(params=["app", "handler"])
async def http_client(request, bot_mock):
    asgi = Asgi(bot_mock)
    if request.param == "handler":
        if request.node.get_closest_marker("skip_http_handler"):
            pytest.skip("skipped http_handler")
        asgi = asgi.http_handler

    transport = httpx.ASGITransport(asgi)
    base_url = "https://example.com"
    async with httpx.AsyncClient(transport=transport, base_url=base_url) as c:
        yield c


@pytest.mark.asyncio
@pytest.mark.skip_http_handler
async def test_http_handler_404(http_client, bot_mock):
    bot_mock.config.webhook.path = "/foo"
    assert (await http_client.post("/bar")).status_code == 404


@pytest.mark.asyncio
@pytest.mark.skip_http_handler
async def test_http_handler_405(http_client):
    assert (await http_client.get("/foo")).status_code == 405
    assert (await http_client.put("/foo")).status_code == 405
    assert (await http_client.delete("/foo")).status_code == 405
    assert (await http_client.head("/foo")).status_code == 405


@pytest.mark.asyncio
async def test_http_handler_403(http_client, bot_mock):
    bot_mock.config.webhook.secret = "secret"
    assert (await http_client.post("/foo")).status_code == 403


@pytest.mark.asyncio
async def test_http_handler_400_empty_body(http_client):
    assert (await http_client.post("/foo")).status_code == 400


@pytest.mark.asyncio
async def test_http_handler_400_invalid_body(http_client, bot_mock):
    bot_mock.process_update.side_effect = JSONDecodeError("test", b"{}")
    assert (await http_client.post("/foo", content=b"{}")).status_code == 400


@pytest.mark.asyncio
async def test_http_handler_200_empty(http_client, bot_mock, update):
    bot_mock.process_update.return_value = Result()
    resp = await http_client.post("/foo", content=encode(update))
    assert resp.status_code == 200
    assert resp.content == b""


@pytest.mark.asyncio
async def test_http_handler_200_response(http_client, bot_mock, update):
    method = SendMessage(chat_id=123, text="hello")
    bot_mock.process_update.return_value = Result(response=method)
    resp = await http_client.post("/foo", content=encode(update))
    assert resp.status_code == 200
    assert resp.content == method._encode_params()[0]
