import pytest

from yatbaf.config import WebhookConfig
from yatbaf.runner.webhook.asgi import Asgi
from yatbaf.runner.webhook.asgi import shutdown_delete
from yatbaf.runner.webhook.asgi import startup_check
from yatbaf.runner.webhook.asgi import startup_setup

URL = "https://example.com"


@pytest.fixture(autouse=True)
def __setup_bot(bot_mock):
    bot_mock._on_startup = []
    bot_mock._on_shutdown = []


def test_startup_check(bot_mock):
    bot_mock.config.webhook = WebhookConfig(url=URL, set_on_startup=False)
    _ = Asgi(bot_mock)
    assert bot_mock._on_startup == [startup_check]
    assert bot_mock._on_shutdown == []


def test_startup_setup(bot_mock):
    bot_mock.config.webhook = WebhookConfig(url=URL, set_on_startup=True)
    _ = Asgi(bot_mock)
    assert bot_mock._on_startup == [startup_setup, startup_check]
    assert bot_mock._on_shutdown == []


def test_shutdown_delete(bot_mock):
    bot_mock.config.webhook = WebhookConfig(
        url=URL,
        set_on_startup=False,
        delete_on_shutdown=True,
    )
    _ = Asgi(bot_mock)
    assert bot_mock._on_startup == [startup_check]
    assert bot_mock._on_shutdown == [shutdown_delete]


def test_startup_shutdown(bot_mock):
    bot_mock.config.webhook = WebhookConfig(
        url=URL,
        set_on_startup=True,
        delete_on_shutdown=True,
    )
    _ = Asgi(bot_mock)
    assert bot_mock._on_startup == [startup_setup, startup_check]
    assert bot_mock._on_shutdown == [shutdown_delete]
