import pytest

from yatbaf.filters.base import BaseFilter
from yatbaf.filters.base import filter


@pytest.mark.asyncio
async def test_filter():

    @filter
    async def myfilter(_):
        return True

    assert isinstance(myfilter, BaseFilter)
    assert myfilter.fn is not None
    assert myfilter.metadata == {"content": (1, 100)}
    assert await myfilter.check(None)


def test_filter_args():

    @filter(category="chat", priority=1000)
    async def myfilter(_):
        return True

    assert myfilter.fn is not None
    assert myfilter.metadata == {"chat": (1, 1000)}
