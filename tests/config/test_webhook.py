import pytest

from yatbaf.config import WebhookConfig


@pytest.mark.parametrize(
    "url,path",
    [
        ["example.com:8000", None],
        ["example.com:8000/", "/"],
        ["example.com:8000/foo", "/foo"],
        ["example.com/foo/bar", "/foo/bar"],
        ["//example.com/foo", "/foo"],
        ["http://example.com:8000/foo", "/foo"],
        ["https://example.com/foo", "/foo"],
    ]
)
def test_path(url, path):
    conf = WebhookConfig(url)
    assert conf.path == path


@pytest.mark.parametrize(
    "url,ip",
    [
        ["example.com:8000", None],
        ["127.0.0.1:8000", "127.0.0.1"],
        ["http://127.0.0.1:8000", "127.0.0.1"],
        ["//127.0.0.1:8000", "127.0.0.1"],
        ["https://127.0.0.1:8000", "127.0.0.1"],
    ]
)
def test_ip(url, ip):
    conf = WebhookConfig(url)
    assert conf.ip == ip


@pytest.mark.parametrize(
    "url,port",
    [
        ["example.com", 8443],
        ["127.0.0.1", 8443],
        ["example.com:8000", 8000],
        ["127.0.0.1:8000", 8000],
        ["//127.0.0.1:8000", 8000],
        ["http://127.0.0.1:8080", 8080],
        ["https://127.0.0.1:8081", 8081],
    ]
)
def test_port(url, port):
    conf = WebhookConfig(url)
    assert conf.port == port


@pytest.mark.parametrize(
    "url,expect",
    [
        ["127.0.0.1", "https://127.0.0.1:8443"],
        ["127.0.0.1:8000", "https://127.0.0.1:8000"],
        ["http://127.0.0.1", "http://127.0.0.1:8443"],
        ["http://127.0.0.1/foo/bar", "http://127.0.0.1:8443/foo/bar"],
        ["example.com", "https://example.com:8443"],
        ["example.com/", "https://example.com:8443/"],
        ["example.com:8000", "https://example.com:8000"],
        ["example.com:8000/", "https://example.com:8000/"],
        ["example.com:8000/foo", "https://example.com:8000/foo"],
        ["example.com/foo", "https://example.com:8443/foo"],
        ["//example.com/foo/bar", "https://example.com:8443/foo/bar"],
        ["//example.com:8000", "https://example.com:8000"],
        ["https://example.com:8081", "https://example.com:8081"],
    ]
)
def test_url(url, expect):
    conf = WebhookConfig(url)
    assert conf.url == expect


def test_force_startup():
    conf = WebhookConfig(
        url="https://example.com",
        force_set=True,
        set_on_startup=False,
    )
    assert conf.set_on_startup
