import asyncio
import sys
import unittest.mock as mock
from collections.abc import Iterator
from contextlib import asynccontextmanager
from copy import deepcopy
from typing import Any

import pytest

from yatbaf.client.models import ResponseOk
from yatbaf.exceptions import NetworkError
from yatbaf.exceptions import RequestTimeoutError
from yatbaf.methods import GetUpdates
from yatbaf.runner.long_polling import LongPolling
from yatbaf.runner.long_polling import _default_error_handler

MODULE = "yatbaf.runner.long_polling"
module_obj = sys.modules[MODULE]


class AwaitableMock(mock.AsyncMock):

    def __await__(self) -> Iterator[Any]:
        self.await_count += 1
        return iter([])


class AsyncMockCopy(mock.AsyncMock):

    def __call__(self, *args, **kwargs):
        args = deepcopy(args)
        kwargs = deepcopy(kwargs)
        return super().__call__(*args, **kwargs)


@pytest.fixture
def asyncio_mock():
    asyncio_module = mock.Mock()
    asyncio_module.gather = mock.AsyncMock()
    asyncio_module.sleep = mock.AsyncMock()
    asyncio_module.wait_for = mock.AsyncMock()
    asyncio_module.CancelledError = asyncio.CancelledError
    return asyncio_module


@pytest.fixture(autouse=True)
def __mock(monkeypatch, asyncio_mock):
    monkeypatch.setattr(module_obj, "asyncio", asyncio_mock)


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "drop_pending,limit,allowed_updates,timeout",
    [
        [True, None, ["message"], 10.0],
        [False, 10, None, 80.0],
        [True, 20, ["message", "callback_qeury"], 24.5],
    ]
)
async def test_get_updates(
    bot_mock, update, drop_pending, limit, allowed_updates, timeout
):
    process_update_result = object()
    bot_mock.process_update = mock.Mock(return_value=process_update_result)
    bot_mock._api_client.invoke = AsyncMockCopy(
        side_effect=[
            ResponseOk(ok=True, result=[update]),
            ResponseOk(ok=True, result=[]),
        ]
    )
    bot_mock.config.drop_pending_updates = drop_pending
    bot_mock.config.allowed_updates = allowed_updates
    bot_mock.config.polling.limit = limit
    bot_mock.config.polling.timeout = timeout
    task_group = mock.Mock()

    polling = LongPolling(bot_mock)
    prop = mock.PropertyMock(side_effect=[True, True, False])
    with mock.patch.object(LongPolling, "_running", prop, create=True):
        await polling._get_updates(task_group)

    bot_mock.process_update.assert_called_once_with(update)
    task_group.create_task.assert_called_once_with(
        process_update_result,
        name=f"update-{update.update_id}",
    )
    bot_mock._on_error.assert_not_awaited()

    cfg = bot_mock.config
    assert bot_mock._api_client.invoke.call_count == 2
    assert bot_mock._api_client.invoke.call_args_list == [
        mock.call(
            GetUpdates(
                timeout=cfg.polling.timeout,
                offset=(-1 if drop_pending else None),
                limit=cfg.polling.limit,
                allowed_updates=cfg.allowed_updates,
            ),
            timeout=cfg.polling.timeout + 5.0,
        ),
        mock.call(
            GetUpdates(
                timeout=cfg.polling.timeout,
                offset=update.update_id + 1,
                limit=cfg.polling.limit,
                allowed_updates=cfg.allowed_updates,
            ),
            timeout=cfg.polling.timeout + 5.0,
        ),
    ]


@pytest.mark.asyncio
@mock.patch(f"{MODULE}._default_error_handler")
async def test_get_updates_error(error_handler, bot_mock):
    polling = LongPolling(bot_mock)
    task_group = mock.Mock()

    exc = ValueError()
    bot_mock._api_client.invoke.side_effect = [exc, mock.Mock(result=[])]

    prop = mock.PropertyMock(side_effect=[True, True, False])
    with mock.patch.object(LongPolling, "_running", prop, create=True):
        await polling._get_updates(task_group)

    assert bot_mock._api_client.invoke.call_count == 2
    task_group.create_task.assert_not_called()
    bot_mock._on_error.assert_awaited_once_with(exc, error_handler)


@pytest.mark.asyncio
@mock.patch.object(LongPolling, "_get_updates", new_callable=mock.Mock)
async def test_loop(get_updates, bot_mock, asyncio_mock):
    task_group = mock.AsyncMock()
    task_group.__aenter__.return_value = task_group
    asyncio_mock.TaskGroup.return_value = task_group

    get_updates_coro = object()
    get_updates.return_value = get_updates_coro
    get_updates_task = AwaitableMock(cancel=mock.Mock())
    asyncio_mock.create_task.return_value = get_updates_task

    polling = LongPolling(bot_mock)
    prop = mock.PropertyMock(side_effect=[True, True, False])
    with mock.patch.object(LongPolling, "_running", prop, create=True):
        await polling._loop()

    get_updates.assert_called_once_with(task_group)
    asyncio_mock.create_task.assert_called_once_with(get_updates_coro)
    assert asyncio_mock.sleep.call_args_list == [mock.call(1), mock.call(1)]
    get_updates_task.cancel.assert_called_once()
    get_updates_task.assert_awaited_once()


@pytest.mark.asyncio
@mock.patch.object(LongPolling, "_set_signals")
@mock.patch.object(LongPolling, "_loop")
async def test_run(loop, set_signals, bot_mock):
    order = []

    @asynccontextmanager
    async def bot_lifespan():
        order.append(1)
        yield
        order.append(4)

    set_signals.side_effect = lambda: order.append(2)
    loop.side_effect = lambda: order.append(3)

    bot_mock.lifespan.return_value = bot_lifespan()

    polling = LongPolling(bot_mock)
    await polling.run()
    assert order == [1, 2, 3, 4]


@pytest.mark.asyncio
@mock.patch.object(LongPolling, "_set_signals")
@mock.patch.object(LongPolling, "_loop")
async def test_run_startup_error(loop, set_signals, bot_mock):

    @asynccontextmanager
    async def lifespan_error():
        raise ValueError()
        yield

    bot_mock.lifespan.return_value = lifespan_error()

    polling = LongPolling(bot_mock)
    with pytest.raises(ValueError):
        await polling.run()

    assert not polling._running
    set_signals.assert_not_called()
    loop.assert_not_awaited()


def test_stop(bot_mock):
    polling = LongPolling(bot_mock)
    polling._running = True
    polling.stop()
    assert not polling._running


@mock.patch.object(LongPolling, "stop")
def test_signal_handler(stop_mock, bot_mock):
    polling = LongPolling(bot_mock)
    polling._signal_handler(2, None)
    stop_mock.assert_called_once()


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "error,delay",
    [
        (RequestTimeoutError(ValueError("1")), 5.0),
        (NetworkError(ValueError("2")), 20.0),
        (ValueError("3"), 5.0),
    ]
)
async def test_error_handler(asyncio_mock, error, delay):
    await _default_error_handler(None, error)
    asyncio_mock.sleep.assert_awaited_once_with(delay)
