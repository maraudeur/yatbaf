import pytest

from yatbaf.handlers.base import _UpdateHandler
from yatbaf.handlers.group import OnMessage
from yatbaf.handlers.group import OnPoll
from yatbaf.handlers.handler import Handler


@pytest.mark.asyncio
async def test_handle(handler_fn, update, mock_mark):
    handler = _UpdateHandler({
        "message": OnMessage(
            handlers=[
                Handler(
                    fn=handler_fn,
                    update_type="message",
                ),
            ]
        )
    })
    await handler.handle(update)
    mock_mark.assert_called_once_with(update.event)


@pytest.mark.asyncio
async def test_handle_none(handler_fn, update, mock_mark):
    handler = _UpdateHandler({
        "poll": OnPoll(
            handlers=[
                Handler(
                    fn=handler_fn,
                    update_type="poll",
                ),
            ],
        )
    })
    await handler.handle(update)
    mock_mark.assert_not_called()
