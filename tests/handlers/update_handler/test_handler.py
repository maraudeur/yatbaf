import unittest.mock as mock

from yatbaf.handlers.base import _UpdateHandler


def test_on_registration(monkeypatch):
    # yapf: disable
    monkeypatch.setattr(_UpdateHandler, "_resolve_middleware", rm := mock.Mock())  # noqa: E501
    monkeypatch.setattr(_UpdateHandler, "_resolve_guards", rg := mock.Mock())
    monkeypatch.setattr(_UpdateHandler, "_prepare_handlers", ph := mock.Mock())
    monkeypatch.setattr(_UpdateHandler, "_check_frozen_handler", cf := mock.Mock())  # noqa: E501
    # yapf: enable

    handler = _UpdateHandler({})
    handler.on_registration(guards=1, middleware=2, dependencies=3)
    rg.assert_called_once_with(1)
    rm.assert_called_once_with(2)
    ph.assert_called_once_with(3)
    cf.assert_called_once()
