from yatbaf.handlers.base import BaseHandler
from yatbaf.handlers.base import Result


class BaseHandlerTestImpl(BaseHandler):

    def __eq__(self, _):  # noqa: U101
        return NotImplemented

    def on_registration(self, g, m, d):  # noqa; U100
        pass

    async def _process_update(self, update):
        update.ctx["test"].append("h")
        return Result()
