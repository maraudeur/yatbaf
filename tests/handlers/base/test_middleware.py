import pytest

from .handler import BaseHandlerTestImpl


def create_middleware(obj):

    def middleware(handler):

        async def wrapper(update):
            update.ctx["test"].append(obj)
            return await handler(update)

        return wrapper

    return middleware


def test_resolve_middleware_empty():
    handler = BaseHandlerTestImpl("update")
    handler._resolve_middleware()
    assert handler._middleware_stack == handler._handle


@pytest.mark.asyncio
async def test_resolve_middleware(update):
    handler = BaseHandlerTestImpl(
        "update",
        middleware=[
            create_middleware("m1"),
            create_middleware("m2"),
            create_middleware("m3"),
        ]
    )
    handler._resolve_middleware()
    result = await handler.handle(update)
    assert result is not None
    assert update.ctx["test"] == ["m1", "m2", "m3", "h"]


@pytest.mark.asyncio
async def test_resolve_middleware_order_parent(update):
    handler = BaseHandlerTestImpl(
        update_type="update",
        middleware=[
            create_middleware("m3"),
            create_middleware("m4"),
            create_middleware("m5"),
        ],
    )
    handler._resolve_middleware([
        create_middleware("m1"),
        create_middleware("m2"),
    ])
    await handler.handle(update)
    assert update.ctx["test"] == ["m1", "m2", "m3", "m4", "m5", "h"]


@pytest.mark.asyncio
async def test_resolve_middleware_order_dups(update):
    dup = create_middleware("dup")
    handler = BaseHandlerTestImpl(
        update_type="update",
        middleware=[
            create_middleware("m3"),
            create_middleware("m4"),
            create_middleware("m5"),
            dup,
        ],
    )
    handler._resolve_middleware([
        dup,
        create_middleware("m1"),
        create_middleware("m2"),
    ])
    await handler.handle(update)
    assert update.ctx["test"] == ["dup", "m1", "m2", "m3", "m4", "m5", "h"]
