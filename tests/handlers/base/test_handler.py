from .handler import BaseHandlerTestImpl


def test_new():
    handler = BaseHandlerTestImpl("update")
    assert handler.update_type == "update"
    assert not handler._filters
    assert not handler._guards
    assert not handler._dependencies
    assert handler._middleware_stack == handler._handle
    assert handler._priority == ((0, (0, 0)), (0, (0, 0)), (0, (0, 0)))
    assert not handler._frozen
    assert handler._stop_propagate
