import pytest

from .handler import BaseHandlerTestImpl


@pytest.mark.asyncio
async def test_match_empty_true(update):
    handler = BaseHandlerTestImpl("update")
    assert await handler.match(update)


@pytest.mark.asyncio
async def test_match_true(update, filter_true):
    handler = BaseHandlerTestImpl("update", filters=[filter_true])
    assert await handler.match(update)


@pytest.mark.asyncio
async def test_match_false(update, filter_false):
    handler = BaseHandlerTestImpl("update", filters=[filter_false])
    assert not await handler.match(update)


@pytest.mark.asyncio
async def test_match_true_false(update, filter_false, filter_true):
    handler = BaseHandlerTestImpl("update", filters=[filter_true, filter_false])
    assert not await handler.match(update)
