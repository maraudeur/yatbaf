import pytest

from .handler import BaseHandlerTestImpl


def test_resolve_guards(guard_true):
    handler = BaseHandlerTestImpl("update")
    handler._resolve_guards([guard_true])
    assert handler._guards == [guard_true]


def test_resolve_guards_empty():
    handler = BaseHandlerTestImpl("update")
    handler._resolve_guards()
    assert handler._guards == []


def test_resolve_guards_order(guard_false, guard_true):
    handler = BaseHandlerTestImpl("update", guards=[guard_false])
    handler._resolve_guards([guard_true])
    assert handler._guards == [guard_true, guard_false]


def test_resolve_guards_order_dups(guard_false, guard_true):
    handler = BaseHandlerTestImpl("update", guards=[guard_false, guard_true])
    handler._resolve_guards([guard_true])
    assert handler._guards == [guard_true, guard_false]


@pytest.mark.asyncio
async def test_handle_process_update(update):
    handler = BaseHandlerTestImpl("update")
    result = await handler._handle(update)
    assert result is not None
    assert update.ctx["test"] == ["h"]


@pytest.mark.asyncio
async def test_handle_guard_false(update, guard_false):
    handler = BaseHandlerTestImpl("update", guards=[guard_false])
    result = await handler._handle(update)
    assert result is not None
    assert update.ctx["test"] == []


@pytest.mark.asyncio
async def test_handle_guard_true(update, guard_true):
    handler = BaseHandlerTestImpl("update", guards=[guard_true])
    result = await handler._handle(update)
    assert result is not None
    assert update.ctx["test"] == ["h"]
