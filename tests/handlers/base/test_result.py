from yatbaf.handlers.base import Result


def test_new():
    result = Result()
    assert result.stop_propagate
    assert result.response is None


def test_bool():
    result = Result()
    assert result
    result = Result(False)
    assert not result
