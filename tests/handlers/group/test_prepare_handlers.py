import unittest.mock as mock

from yatbaf.di import Provide
from yatbaf.handlers.group import HandlerGroup
from yatbaf.handlers.group import OnUpdate
from yatbaf.handlers.handler import on_update


def test_middleware(monkeypatch):
    local_middleware = object()
    middleware = object()
    global_middleware = object()

    monkeypatch.setattr(OnUpdate, "on_registration", gr := mock.Mock())
    monkeypatch.setattr(on_update, "on_registration", hr := mock.Mock())

    handler = HandlerGroup(
        "update",
        handler_middleware=[
            middleware,
            (local_middleware, "local"),
        ],
        handlers=[
            OnUpdate(),
            on_update(),
        ],
    )
    handler._prepare_handlers(middleware=[global_middleware])
    # yapf: disable
    gr.assert_called_once_with((), (global_middleware, middleware), {})
    hr.assert_called_once_with((), (global_middleware, middleware, local_middleware), {})  # noqa: E501
    # yapf: enable


def test_guard(monkeypatch):
    local_guard = object()
    guard = object()
    global_guard = object()

    monkeypatch.setattr(OnUpdate, "on_registration", gr := mock.Mock())
    monkeypatch.setattr(on_update, "on_registration", hr := mock.Mock())

    handler = HandlerGroup(
        "update",
        handler_guards=[
            guard,
            (local_guard, "local"),
        ],
        handlers=[
            OnUpdate(),
            on_update(),
        ],
    )
    handler._prepare_handlers(guards=[global_guard])
    gr.assert_called_once_with((global_guard, guard), (), {})
    hr.assert_called_once_with((global_guard, guard, local_guard), (), {})


def test_deps(monkeypatch):
    monkeypatch.setattr(OnUpdate, "on_registration", gr := mock.Mock())
    monkeypatch.setattr(on_update, "on_registration", hr := mock.Mock())

    def foo():
        return "foo"

    def bar():
        return "bar"

    handler = HandlerGroup(
        "update",
        dependencies={"foo": Provide(foo)},
        handlers=[
            OnUpdate(),
            on_update(),
        ],
    )
    handler._prepare_handlers(dependencies={"bar": Provide(bar)})
    # yapf: disable
    gr.assert_called_once_with((), (), {"foo": Provide(foo), "bar": Provide(bar)})  # noqa: E501
    hr.assert_called_once_with((), (), {"foo": Provide(foo), "bar": Provide(bar)})  # noqa: E501
    # yapf: enable
