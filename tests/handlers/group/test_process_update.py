import pytest

from yatbaf.handlers.group import OnUpdate
from yatbaf.handlers.handler import on_update


def create_handler(mark, filters=None):

    @on_update(filters=filters)
    async def handler(update):
        update.ctx["test"].append(mark)

    return handler


@pytest.mark.asyncio
async def test_fallback(update):
    handler = OnUpdate(handlers=[create_handler("h")])
    assert await handler._process_update(update)
    assert update.ctx["test"] == ["h"]


@pytest.mark.asyncio
async def test_resolve_filters(update, filter_true):
    hadnler = OnUpdate(
        handlers=[
            create_handler(1),
            create_handler(2, filters=[filter_true]),
        ]
    )
    assert await hadnler._process_update(update)
    assert update.ctx["test"] == [1]


@pytest.mark.asyncio
async def test_resolve_stop_propagate(update, filter_true):
    handler = OnUpdate(
        handlers=[
            create_handler(1, filters=[filter_true]),
            create_handler(2, filters=[filter_true]),
            OnUpdate(handlers=[create_handler(3, filters=[filter_true])]),
        ],
    )
    assert await handler._process_update(update)
    assert update.ctx["test"] == [1]


@pytest.mark.asyncio
async def test_resolve_nested_vert(update, filter_true, filter_false):
    handler = OnUpdate(
        handlers=[
            create_handler(1, filters=[filter_false]),
            OnUpdate(
                handlers=[
                    create_handler(2, filters=[filter_false]),
                    OnUpdate(
                        handlers=[
                            create_handler(3, filters=[filter_false]),
                            OnUpdate(
                                handlers=[
                                    create_handler(4, filters=[filter_true]),
                                ]
                            ),
                        ],
                    ),
                ],
            ),
        ],
    )
    assert await handler._process_update(update)
    assert update.ctx["test"] == [4]


@pytest.mark.asyncio
async def test_resolve_nested_horiz(update, filter_false, filter_true):
    handler = OnUpdate(
        handlers=[
            OnUpdate(handlers=[create_handler(1, filters=[filter_false])]),
            OnUpdate(handlers=[create_handler(2, filters=[filter_false])]),
            OnUpdate(handlers=[create_handler(3, filters=[filter_false])]),
            OnUpdate(handlers=[create_handler(4, filters=[filter_true])]),
        ],
    )
    assert await handler._process_update(update)
    assert update.ctx["test"] == [4]


@pytest.mark.asyncio
async def test_stop_propagate_true_param(update):
    handler = OnUpdate(
        handlers=[
            OnUpdate(stop_propagate=True),
            OnUpdate(handlers=[create_handler(1)]),
        ],
    )
    assert await handler._process_update(update)
    assert update.ctx["test"] == []


@pytest.mark.asyncio
async def test_stop_propagate_true_filter(update, filter_true):
    handler = OnUpdate(
        handlers=[
            OnUpdate(filters=[filter_true]),
            OnUpdate(handlers=[create_handler(1)]),
        ],
    )
    assert await handler._process_update(update)
    assert update.ctx["test"] == []


@pytest.mark.asyncio
async def test_stop_propagate_true_filter_guard(
    update, guard_false, filter_true
):
    handler = OnUpdate(
        handlers=[
            OnUpdate(filters=[filter_true], guards=[guard_false]),
            OnUpdate(handlers=[create_handler(1)]),
        ],
    )
    assert await handler._process_update(update)
    assert update.ctx["test"] == []


@pytest.mark.asyncio
async def test_stop_propagate_false_filter(update, filter_true):
    handler = OnUpdate(
        handlers=[
            OnUpdate(filters=[filter_true], stop_propagate=False),
            OnUpdate(handlers=[create_handler(1)]),
        ],
    )
    assert await handler._process_update(update)
    assert update.ctx["test"] == [1]
