import sys
import unittest.mock as mock

import pytest

from yatbaf.enums import Event
from yatbaf.exceptions import FrozenInstanceError
from yatbaf.handlers.group import HandlerGroup
from yatbaf.handlers.group import OnBusinessConnection
from yatbaf.handlers.group import OnBusinessMessage
from yatbaf.handlers.group import OnCallbackQuery
from yatbaf.handlers.group import OnChannelPost
from yatbaf.handlers.group import OnChatBoost
from yatbaf.handlers.group import OnChatJoinRequest
from yatbaf.handlers.group import OnChatMember
from yatbaf.handlers.group import OnChosenInlineResult
from yatbaf.handlers.group import OnDeletedBusinessMessages
from yatbaf.handlers.group import OnEditedBusinessMessage
from yatbaf.handlers.group import OnEditedChannelPost
from yatbaf.handlers.group import OnEditedMessage
from yatbaf.handlers.group import OnInlineQuery
from yatbaf.handlers.group import OnMessage
from yatbaf.handlers.group import OnMessageReaction
from yatbaf.handlers.group import OnMessageReactionCount
from yatbaf.handlers.group import OnMyChatMember
from yatbaf.handlers.group import OnPaidMediaPurchased
from yatbaf.handlers.group import OnPoll
from yatbaf.handlers.group import OnPollAnswer
from yatbaf.handlers.group import OnPreCheckoutQuery
from yatbaf.handlers.group import OnRemovedChatBoost
from yatbaf.handlers.group import OnShippingQuery
from yatbaf.handlers.group import OnUpdate
from yatbaf.handlers.handler import Handler
from yatbaf.middleware import Middleware

MODULE = sys.modules["yatbaf.handlers.group"]


def test_update_type():
    # yapf: disable
    assert OnUpdate().update_type == "update"
    assert OnCallbackQuery().update_type == Event.CALLBACK_QUERY
    assert OnChannelPost().update_type == Event.CHANNEL_POST
    assert OnChatJoinRequest().update_type == Event.CHAT_JOIN_REQUEST
    assert OnChatMember().update_type == Event.CHAT_MEMBER
    assert OnChosenInlineResult().update_type == Event.CHOSEN_INLINE_RESULT
    assert OnEditedChannelPost().update_type == Event.EDITED_CHANNEL_POST
    assert OnEditedMessage().update_type == Event.EDITED_MESSAGE
    assert OnInlineQuery().update_type == Event.INLINE_QUERY
    assert OnMessage().update_type == Event.MESSAGE
    assert OnMyChatMember().update_type == Event.MY_CHAT_MEMBER
    assert OnPoll().update_type == Event.POLL
    assert OnPollAnswer().update_type == Event.POLL_ANSWER
    assert OnPreCheckoutQuery().update_type == Event.PRE_CHECKOUT_QUERY
    assert OnShippingQuery().update_type == Event.SHIPPING_QUERY
    assert OnChatBoost().update_type == Event.CHAT_BOOST
    assert OnRemovedChatBoost().update_type == Event.REMOVED_CHAT_BOOST
    assert OnMessageReaction().update_type == Event.MESSAGE_REACTION
    assert OnMessageReactionCount().update_type == Event.MESSAGE_REACTION_COUNT  # noqa: E501
    assert OnBusinessConnection().update_type == Event.BUSINESS_CONNECTION
    assert OnBusinessMessage().update_type == Event.BUSINESS_MESSAGE
    assert OnEditedBusinessMessage().update_type == Event.EDITED_BUSINESS_MESSAGE  # noqa: 501
    assert OnDeletedBusinessMessages().update_type == Event.DELETED_BUSINESS_MESSAGES  # noqa: 501
    assert OnPaidMediaPurchased().update_type == Event.PAID_MEDIA_PURCHASED
    # yapf: enable


def test_new():
    router = OnMessage(name="foo")
    assert not router._frozen
    assert not router._handlers
    assert not router._handler_guards
    assert not router._handler_middleware
    assert not router._guards
    assert not router._middleware
    assert not router._filters
    assert not router._stop_propagate
    assert not router._dependencies
    assert router.name == "foo"


def test_on_registration(monkeypatch):
    # yapf: disable
    monkeypatch.setattr(HandlerGroup, "_resolve_guards", rg := mock.Mock())
    monkeypatch.setattr(HandlerGroup, "_check_frozen_handler", cf := mock.Mock())  # noqa: E501
    monkeypatch.setattr(HandlerGroup, "_resolve_filters", rf := mock.Mock())
    monkeypatch.setattr(HandlerGroup, "_resolve_middleware", rmw := mock.Mock())
    monkeypatch.setattr(HandlerGroup, "_prepare_handlers", ph := mock.Mock())
    # yapf: enable
    router = HandlerGroup("message")
    router.on_registration()

    cf.assert_called_once()
    rmw.assert_called_once()
    ph.assert_called_once()
    rg.assert_called_once()
    rf.assert_called_once()
    assert router._frozen


def test_on_registration_twice():
    g = OnMessage()
    g.on_registration()
    with pytest.raises(FrozenInstanceError):
        g.on_registration()


async def fn(_):
    pass


def _handler(fn):
    return Handler("message", fn=fn)


async def guard(_):
    pass


def middleware(h):

    async def w(u):
        await h(u)

    return w


@pytest.mark.parametrize(
    "objs",
    [
        (
            OnMessage(),
            OnMessage(),
        ),
        (
            OnMessage(handler_guards=[guard]),
            OnMessage(handler_guards=[guard]),
        ),
        (
            OnMessage(handlers=[_handler(fn)]),
            OnMessage(handlers=[_handler(fn)]),
        ),
        (
            OnMessage(handler_guards=[guard], handlers=[_handler(fn)]),
            OnMessage(handler_guards=[guard], handlers=[_handler(fn)]),
        ),
        (
            OnMessage(
                handler_middleware=[middleware],
                handler_guards=[guard],
                handlers=[_handler(fn)]
            ),
            OnMessage(
                handler_middleware=[middleware],
                handler_guards=[guard],
                handlers=[_handler(fn)],
            ),
        ),
        (
            OnMessage(
                handler_middleware=[Middleware(middleware)],
                handler_guards=[guard],
                handlers=[_handler(fn)]
            ),
            OnMessage(
                handler_middleware=[Middleware(middleware)],
                handler_guards=[guard],
                handlers=[_handler(fn)],
            ),
        ),
    ]
)
def test_eq(objs):
    router1, router2 = objs
    assert router1 == router2


@pytest.mark.parametrize("r1", [OnMessage()])
@pytest.mark.parametrize(
    "r2",
    [
        OnMessage(handlers=[_handler(fn)]),
        OnMessage(handler_middleware=[middleware]),
        OnMessage(handler_guards=[guard]),
        OnMessage(
            handlers=[_handler(fn)],
            handler_middleware=[middleware],
        ),
        OnMessage(
            handlers=[_handler(fn)],
            handler_middleware=[middleware],
            handler_guards=[middleware],
        ),
        OnPoll(),
        OnCallbackQuery(),
    ]
)
def test_not_eq(r1, r2):
    assert r1 != r2
