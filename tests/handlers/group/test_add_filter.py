from yatbaf import filters as f
from yatbaf.handlers.group import OnMessage


def test_init():
    filter = f.User(123)
    router = OnMessage(filters=[filter])
    assert router._filters == [filter]


def test_add_filter():
    router = OnMessage()
    assert router._filters == []
    filter = f.User(123)
    router.add_filter(filter)
    assert router._filters == [filter]
