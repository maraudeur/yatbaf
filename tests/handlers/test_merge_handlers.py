import pytest

from yatbaf.handlers.handler import Handler
from yatbaf.handlers.handler import on_message
from yatbaf.handlers.handler import on_update
from yatbaf.handlers.utils import _UpdateHandler
from yatbaf.handlers.utils import merge_handlers


async def fn(_):
    pass


def test_update_handler():
    handler = Handler(fn=fn, update_type="update")
    result = merge_handlers([handler])
    assert result is handler


def test_event_handler():
    handler = on_message(fn)
    result = merge_handlers([handler])
    assert result == _UpdateHandler({
        "message": Handler(fn=fn, update_type="message"),
    })


def test_update_event_mix():
    message_h = on_message(fn)
    update_h = on_update(fn)
    with pytest.raises(ValueError):
        merge_handlers([message_h, update_h])
