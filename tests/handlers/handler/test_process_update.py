import pytest

from yatbaf.di import Provide
from yatbaf.handlers.handler import on_update


@pytest.mark.asyncio
async def test_provide(update):

    async def get_data():
        return 1

    @on_update(dependencies={"data": Provide(get_data)})
    async def handler(message, data):
        message.ctx["test"].append(data)

    handler._resolve_dependencies()
    await handler._process_update(update)
    assert update.ctx["test"] == [1]


@pytest.mark.asyncio
async def test_provide_update_kwarg(update):

    async def get_data(update):
        update.ctx["test"].append(1)
        return 2

    @on_update(dependencies={"data": Provide(get_data)})
    async def handler(update, data):
        update.ctx["test"].append(data)

    handler._resolve_dependencies()
    await handler._process_update(update)
    assert update.ctx["test"] == [1, 2]


@pytest.mark.asyncio
async def test_dependency_exception(update):

    async def get_data():
        raise ValueError

    @on_update(dependencies={"data": Provide(get_data)})
    async def handler(update, data):
        update.ctx["test"].append(data)

    handler._resolve_dependencies()
    with pytest.raises(ValueError):
        await handler._process_update(update)
    assert update.ctx["test"] == []


@pytest.mark.asyncio
async def test_handler_exception(update):

    async def get_data(update):
        try:
            yield 1
        except ValueError:
            update.ctx["test"].append(2)

    @on_update(dependencies={"data": Provide(get_data)})
    async def handler(update, data):
        update.ctx["test"].append(data)
        raise ValueError()

    handler._resolve_dependencies()
    with pytest.raises(ValueError):
        await handler._process_update(update)
    assert update.ctx["test"] == [1, 2]


@pytest.mark.asyncio
async def test_dependency_cleanup(update):

    async def get_data(update):
        yield 1
        update.ctx["test"].append(2)

    @on_update(dependencies={"data": Provide(get_data)})
    async def handler(update, data):
        update.ctx["test"].append(data)

    handler._resolve_dependencies()
    await handler._process_update(update)
    assert update.ctx["test"] == [1, 2]


@pytest.mark.asyncio
async def test_handler_no_kwargs(update):

    async def get_data(update):
        update.ctx["test"].append(1)
        return 1

    @on_update(dependencies={"data": Provide(get_data)})
    async def handler(update):
        update.ctx["test"].append(2)

    handler._resolve_dependencies()
    await handler._process_update(update)
    assert update.ctx["test"] == [2]
