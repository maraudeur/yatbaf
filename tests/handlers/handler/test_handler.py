import unittest.mock as mock

import pytest

from yatbaf.enums import Event
from yatbaf.exceptions import FrozenInstanceError
from yatbaf.filters import Chat
from yatbaf.handlers.handler import Handler
from yatbaf.handlers.handler import on_business_connection
from yatbaf.handlers.handler import on_business_message
from yatbaf.handlers.handler import on_callback_query
from yatbaf.handlers.handler import on_channel_post
from yatbaf.handlers.handler import on_chat_boost
from yatbaf.handlers.handler import on_chat_join_request
from yatbaf.handlers.handler import on_chat_member
from yatbaf.handlers.handler import on_chosen_inline_result
from yatbaf.handlers.handler import on_deleted_business_messages
from yatbaf.handlers.handler import on_edited_business_message
from yatbaf.handlers.handler import on_edited_channel_post
from yatbaf.handlers.handler import on_edited_message
from yatbaf.handlers.handler import on_inline_query
from yatbaf.handlers.handler import on_message
from yatbaf.handlers.handler import on_message_reaction
from yatbaf.handlers.handler import on_message_reaction_count
from yatbaf.handlers.handler import on_my_chat_member
from yatbaf.handlers.handler import on_paid_media_purchased
from yatbaf.handlers.handler import on_poll
from yatbaf.handlers.handler import on_poll_answer
from yatbaf.handlers.handler import on_pre_checkout_query
from yatbaf.handlers.handler import on_removed_chat_boost
from yatbaf.handlers.handler import on_shipping_query
from yatbaf.handlers.handler import on_update


async def fn(_):
    pass


def middleware(handler):

    async def wrapper(update):
        return await handler(update)

    return wrapper


def test_update_type():
    # yapf: disable
    assert on_update().update_type == "update"
    assert on_callback_query().update_type == Event.CALLBACK_QUERY
    assert on_channel_post().update_type == Event.CHANNEL_POST
    assert on_chat_join_request().update_type == Event.CHAT_JOIN_REQUEST
    assert on_chat_member().update_type == Event.CHAT_MEMBER
    assert on_chosen_inline_result().update_type == Event.CHOSEN_INLINE_RESULT
    assert on_edited_channel_post().update_type == Event.EDITED_CHANNEL_POST
    assert on_edited_message().update_type == Event.EDITED_MESSAGE
    assert on_inline_query().update_type == Event.INLINE_QUERY
    assert on_message().update_type == Event.MESSAGE
    assert on_my_chat_member().update_type == Event.MY_CHAT_MEMBER
    assert on_poll().update_type == Event.POLL
    assert on_poll_answer().update_type == Event.POLL_ANSWER
    assert on_pre_checkout_query().update_type == Event.PRE_CHECKOUT_QUERY
    assert on_shipping_query().update_type == Event.SHIPPING_QUERY
    assert on_chat_boost().update_type == Event.CHAT_BOOST
    assert on_removed_chat_boost().update_type == Event.REMOVED_CHAT_BOOST
    assert on_message_reaction().update_type == Event.MESSAGE_REACTION
    assert on_message_reaction_count().update_type == Event.MESSAGE_REACTION_COUNT  # noqa: E501
    assert on_business_connection().update_type == Event.BUSINESS_CONNECTION
    assert on_business_message().update_type == Event.BUSINESS_MESSAGE
    assert on_edited_business_message().update_type == Event.EDITED_BUSINESS_MESSAGE  # noqa: 501
    assert on_deleted_business_messages().update_type == Event.DELETED_BUSINESS_MESSAGES  # noqa: 501
    assert on_paid_media_purchased().update_type == Event.PAID_MEDIA_PURCHASED
    # yapf: enable


def test_new_handler(handler_fn):
    handler = Handler(update_type=Event.MESSAGE, fn=handler_fn)
    assert not handler._filters
    assert not handler._middleware
    assert not handler._guards
    assert not handler._dependencies
    assert not handler._kwargs
    assert not handler._frozen
    assert handler._fn is handler_fn
    assert handler._update_type == "message"
    assert str(handler) == f"Handler[message](name={handler_fn.__name__})"


def test_new_handler_decorator():

    @Handler("message")
    async def handler(_):
        pass

    assert handler._fn is not None


def test_on_registration(monkeypatch, handler_fn):
    monkeypatch.setattr(Handler, "_resolve_filters", rf := mock.Mock())
    monkeypatch.setattr(Handler, "_check_callback", cc := mock.Mock())
    monkeypatch.setattr(Handler, "_check_frozen_handler", cf := mock.Mock())
    monkeypatch.setattr(Handler, "_resolve_guards", rg := mock.Mock())
    monkeypatch.setattr(Handler, "_resolve_dependencies", rd := mock.Mock())
    monkeypatch.setattr(Handler, "_resolve_middleware", rm := mock.Mock())
    handler = Handler("message", fn=handler_fn)
    handler.on_registration()
    assert handler._frozen
    cc.assert_called_once()
    cf.assert_called_once()
    rg.assert_called_once()
    rm.assert_called_once()
    rd.assert_called_once()
    rf.assert_called_once()


def test_check_callback():
    handler = Handler("message")
    with pytest.raises(ValueError):
        handler._check_callback()

    handler(fn)
    handler._check_callback()


def test_handler_name():
    not_initialized = Handler("message")

    @Handler("message")
    async def callback_name(_):
        pass

    @Handler("message", name="myname")
    async def custom_name(_):
        pass

    assert not_initialized.name == "undefined"
    assert callback_name.name == "callback_name"
    assert custom_name.name == "myname"


def test_fn():
    handler = Handler("message")
    with pytest.raises(ValueError):
        handler.fn

    handler(fn)
    assert handler.fn is fn


def test_check_frozen():
    handler = Handler("message")
    handler._check_frozen_handler()
    handler._frozen = True

    with pytest.raises(FrozenInstanceError):
        handler._check_frozen_handler()


def test_on_registration_twice(handler_fn):
    h = Handler("message", fn=handler_fn)
    h.on_registration()
    with pytest.raises(FrozenInstanceError):
        h.on_registration()


@pytest.mark.parametrize(
    "objs",
    [
        (
            Handler(fn=fn, update_type=Event.MESSAGE),
            Handler(fn=fn, update_type=Event.MESSAGE),
        ),
        (
            Handler(
                fn=fn,
                update_type=Event.MESSAGE,
            ),
            Handler(
                fn=fn,
                update_type=Event.MESSAGE,
                filters=[Chat("group")],
            ),
        ),
        (
            Handler(
                fn=fn,
                update_type=Event.MESSAGE,
                middleware=[middleware],
            ),
            Handler(
                fn=fn,
                update_type=Event.MESSAGE,
                middleware=[middleware],
            ),
        ),
    ]
)
def test_eq(objs):
    handler1, handler2 = objs
    assert handler1 == handler2


async def fn2(_):
    pass


@pytest.mark.parametrize("h1", [Handler(fn=fn, update_type=Event.MESSAGE)])
@pytest.mark.parametrize(
    "h2",
    [
        Handler(fn=fn, update_type=Event.EDITED_MESSAGE),
        Handler(fn=fn, update_type=Event.MESSAGE, middleware=[object()]),
        Handler(fn=fn2, update_type=Event.MESSAGE),
    ]
)
def test_not_eq(h1, h2):
    assert h1 != h2


def test_no_callback():
    handler = Handler("message")
    with pytest.raises(ValueError):
        handler.on_registration()
