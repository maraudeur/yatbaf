import pytest

from yatbaf.di import Dependency
from yatbaf.di import Provide
from yatbaf.exceptions import DependencyError
from yatbaf.handlers.handler import on_update


async def get_foo():
    return 1


async def get_bar(foo):  # noqa: U100
    return 2


async def get_baz():
    pass


DEPENDENCY_FOO = Dependency("foo", Provide(get_foo), [])
DEPENDENCY_BAR = Dependency("bar", Provide(get_bar), [DEPENDENCY_FOO])
DEPENDENCY_BAZ = Dependency("baz", Provide(get_baz), [])


def test_resolve_dependencies():

    @on_update(
        dependencies={
            "foo": Provide(get_foo),
            "bar": Provide(get_bar),
            "baz": Provide(get_baz),
        }
    )
    async def handler(_, foo, bar, baz):  # noqa: U100
        pass

    handler._resolve_dependencies()
    assert handler._resolved_dependencies == [
        {DEPENDENCY_FOO, DEPENDENCY_BAZ},
        {DEPENDENCY_BAR},
    ]


def test_no_kwargs():

    @on_update(
        dependencies={
            "foo": Provide(get_foo),
            "bar": Provide(get_bar),
            "baz": Provide(get_baz),
        }
    )
    async def handler(_):
        pass

    handler._resolve_dependencies()
    assert handler._resolved_dependencies == []


def test_missing_provider():

    @on_update
    async def handler(_, foo):  # noqa: U100
        pass

    with pytest.raises(DependencyError):
        handler._resolve_dependencies()


def test_duplicate_provider():

    @on_update(
        dependencies={
            "foo": Provide(get_bar),
            "bar": Provide(get_bar),
        }
    )
    async def handler(_, foo, bar):  # noqa: U100
        pass

    with pytest.raises(DependencyError):
        handler._resolve_dependencies()


def test_override_order():

    @on_update(dependencies={"foo": Provide(get_foo)})
    async def handler(_, foo):  # noqa: U100
        pass

    handler._resolve_dependencies({"foo": Provide(get_baz)})
    assert handler._resolved_dependencies == [{
        Dependency("foo", Provide(get_foo), [])
    }]
