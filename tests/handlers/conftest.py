import pytest

from yatbaf.filters.base import BaseFilter


@pytest.fixture(autouse=True)
def __prepare_update(update):
    update.ctx["test"] = []


class Filter(BaseFilter):
    priority = 100

    def __init__(self, result):
        self.result = result

    async def check(self, _):  # noqa: U101
        return self.result


@pytest.fixture(scope="module")
def filter_true():
    return Filter(True)


@pytest.fixture(scope="module")
def filter_false():
    return Filter(False)
