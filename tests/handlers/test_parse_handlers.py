import pytest

from yatbaf.handlers.group import OnCallbackQuery
from yatbaf.handlers.group import OnMessage
from yatbaf.handlers.group import OnPoll
from yatbaf.handlers.handler import on_business_connection
from yatbaf.handlers.handler import on_business_message
from yatbaf.handlers.handler import on_callback_query
from yatbaf.handlers.handler import on_channel_post
from yatbaf.handlers.handler import on_chat_boost
from yatbaf.handlers.handler import on_chat_join_request
from yatbaf.handlers.handler import on_chat_member
from yatbaf.handlers.handler import on_chosen_inline_result
from yatbaf.handlers.handler import on_deleted_business_messages
from yatbaf.handlers.handler import on_edited_business_message
from yatbaf.handlers.handler import on_edited_channel_post
from yatbaf.handlers.handler import on_edited_message
from yatbaf.handlers.handler import on_inline_query
from yatbaf.handlers.handler import on_message
from yatbaf.handlers.handler import on_message_reaction
from yatbaf.handlers.handler import on_message_reaction_count
from yatbaf.handlers.handler import on_my_chat_member
from yatbaf.handlers.handler import on_poll
from yatbaf.handlers.handler import on_poll_answer
from yatbaf.handlers.handler import on_pre_checkout_query
from yatbaf.handlers.handler import on_removed_chat_boost
from yatbaf.handlers.handler import on_shipping_query
from yatbaf.handlers.handler import on_update
from yatbaf.handlers.utils import parse_handlers


async def fn(_):
    pass


@pytest.mark.parametrize(
    "handler,exp",
    [
        [
            on_update(fn),
            {
                "update": on_update(fn)
            },
        ],
        [
            on_message(fn),
            {
                "message": on_message(fn)
            },
        ],
        [
            on_edited_message(fn),
            {
                "edited_message": on_edited_message(fn)
            },
        ],
        [
            on_channel_post(fn),
            {
                "channel_post": on_channel_post(fn)
            },
        ],
        [
            on_edited_channel_post(fn),
            {
                "edited_channel_post": on_edited_channel_post(fn)
            },
        ],
        [
            on_inline_query(fn),
            {
                "inline_query": on_inline_query(fn)
            },
        ],
        [
            on_chosen_inline_result(fn),
            {
                "chosen_inline_result": on_chosen_inline_result(fn)
            },
        ],
        [
            on_callback_query(fn),
            {
                "callback_query": on_callback_query(fn)
            },
        ],
        [
            on_shipping_query(fn),
            {
                "shipping_query": on_shipping_query(fn),
            },
        ],
        [
            on_pre_checkout_query(fn),
            {
                "pre_checkout_query": on_pre_checkout_query(fn),
            },
        ],
        [
            on_poll(fn),
            {
                "poll": on_poll(fn),
            },
        ],
        [
            on_poll_answer(fn),
            {
                "poll_answer": on_poll_answer(fn)
            },
        ],
        [
            on_my_chat_member(fn),
            {
                "my_chat_member": on_my_chat_member(fn)
            },
        ],
        [
            on_chat_member(fn),
            {
                "chat_member": on_chat_member(fn),
            },
        ],
        [
            on_chat_join_request(fn),
            {
                "chat_join_request": on_chat_join_request(fn)
            },
        ],
        [
            on_message_reaction(fn),
            {
                "message_reaction": on_message_reaction(fn)
            },
        ],
        [
            on_message_reaction_count(fn),
            {
                "message_reaction_count": on_message_reaction_count(fn)
            },
        ],
        [
            on_chat_boost(fn),
            {
                "chat_boost": on_chat_boost(fn)
            },
        ],
        [
            on_removed_chat_boost(fn),
            {
                "removed_chat_boost": on_removed_chat_boost(fn)
            },
        ],
        [
            on_business_connection(fn),
            {
                "business_connection": on_business_connection(fn)
            },
        ],
        [
            on_business_message(fn),
            {
                "business_message": on_business_message(fn)
            },
        ],
        [
            on_edited_business_message(fn),
            {
                "edited_business_message": on_edited_business_message(fn)
            },
        ],
        [
            on_deleted_business_messages(fn),
            {
                "deleted_business_messages": on_deleted_business_messages(fn)
            },
        ],
    ]
)
def test_parse(handler, exp):
    assert parse_handlers([handler]) == exp


def test_one_router(handler_fn):
    router = OnMessage(handlers=[on_message(handler_fn)])
    result = parse_handlers([router])
    assert result == {"message": router}
    assert result["message"] is router


def test_routers(handler_fn):
    result = parse_handlers([
        OnMessage(),
        OnMessage(handlers=[on_message(handler_fn)]),
        OnPoll(handlers=[on_poll(handler_fn)]),
        OnCallbackQuery(handlers=[on_callback_query(handler_fn)])
    ])
    assert result == {
        "message": OnMessage(
            handlers=[
                OnMessage(),
                OnMessage(handlers=[on_message(handler_fn)]),
            ],
        ),
        "callback_query": OnCallbackQuery(
            handlers=[on_callback_query(handler_fn)]
        ),
        "poll": OnPoll(handlers=[on_poll(handler_fn)]),
    }


def test_handlers_routers(handler_fn):
    result = parse_handlers([
        on_poll(handler_fn),
        on_message(handler_fn),
        on_callback_query(handler_fn),
        OnMessage(handlers=[on_message(handler_fn)]),
        OnCallbackQuery(handlers=[on_callback_query(handler_fn)]),
    ])
    assert result == {
        "message": OnMessage(
            handlers=[
                on_message(handler_fn),
                OnMessage(handlers=[on_message(handler_fn)])
            ],
        ),
        "poll": on_poll(handler_fn),
        "callback_query": OnCallbackQuery(
            handlers=[
                on_callback_query(handler_fn),
                OnCallbackQuery(handlers=[on_callback_query(handler_fn)]),
            ]
        ),
    }
