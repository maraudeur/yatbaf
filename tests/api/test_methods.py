import re
import unittest.mock as mock

import pytest

from yatbaf import api
from yatbaf import methods
from yatbaf import on_message


@on_message
async def handler(_):
    pass


def capitalize(w):
    return f"{w[0].upper()}{w[1:]}"


def snake_case(s):
    return '_'.join(
        re.sub(
            '([A-Z][a-z]+)',
            r' \1',
            re.sub('([A-Z]+)', r' \1', s.replace('-', ' '))
        ).split()
    ).lower()


@pytest.fixture
def api_methods(api_spec):
    return api_spec["methods"]


def test_method_object(api_methods):
    for api_method in api_methods.values():
        assert hasattr(methods, capitalize(api_method["name"]))


def test_method_object_fields(api_methods):
    for api_method in api_methods.values():
        method_obj = getattr(methods, capitalize(api_method["name"]))
        for field in api_method.get("fields", []):
            assert hasattr(method_obj, field["name"])


@pytest.mark.asyncio
async def test_bot_methods(monkeypatch, api_methods):
    monkeypatch.setattr(api.ApiMethods, "_call", _call := mock.AsyncMock())
    methods = api.ApiMethods()
    obj = object()
    for api_method in api_methods.values():
        _call.reset_mock()
        with mock.patch(f"yatbaf.api.{capitalize(api_method['name'])}") as o:
            o.return_value = obj
            method = getattr(methods, snake_case(api_method["name"]))
            fields = {}
            for i, field in enumerate(api_method.get("fields", [])):
                fields[field["name"]] = i
            await method(**fields)
            if fields:
                o.assert_called_once_with(**fields)
            else:
                o.assert_called_once()
        _call.assert_awaited_once_with(obj)
