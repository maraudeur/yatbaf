from yatbaf.version import __api_version__


def test_version(api_spec):
    assert __api_version__ == api_spec["version"].split(" ")[-1]
