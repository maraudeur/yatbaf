import httpx
import pytest
import pytest_asyncio

from yatbaf.client.http import HttpClient
from yatbaf.client.multipart import MultipartStream
from yatbaf.client.telegram import SERVER_URL
from yatbaf.exceptions import NetworkError
from yatbaf.exceptions import RequestTimeoutError
from yatbaf.input_file import InputFile


@pytest.fixture
def request_json(token):
    return (
        f"{SERVER_URL}/bot{token}/sendmessage",
        b'{"chat_id":123,"text":"hello"}',
    )


@pytest.fixture
def request_stream(token):
    return f"{SERVER_URL}/file/bot{token}/fileid123"


@pytest_asyncio.fixture
async def client() -> HttpClient:
    client = HttpClient()
    yield client
    await client.close()


@pytest.mark.asyncio
async def test_post_json(token, respx_mock, request_json, client):
    response_content = b'{"ok":true}'
    url, json = request_json

    route = respx_mock.post(
        f"{SERVER_URL}/bot{token}/sendmessage",
        content__eq=json,
        headers__contains={
            "Content-Type": "application/json"
        },
    ).respond(
        status_code=200,
        content=response_content,
    )
    status, result = await client.send_post(
        url, json, headers={"Content-Type": "application/json"}
    )

    assert route.called
    assert status == 200
    assert result == response_content


@pytest.mark.asyncio
async def test_post_multipart(token, respx_mock, client):
    response_content = b'{"ok":true}'

    file_content = b"hello\nworld!"
    f = InputFile("file.txt", content=file_content)

    route = respx_mock.post(
        f"{SERVER_URL}/bot{token}/senddocument",
        files__eq={
            f.attach_id: (f.name, file_content)
        },
        data__eq={
            "chat_id": 123,
            "document": f"attach://{f.attach_id}",
        },
    ).respond(
        status_code=200,
        content=response_content,
    )

    stream = MultipartStream(
        data={
            "chat_id": b"123",
            "document": f"attach://{f.attach_id}".encode(),
        },
        files={f.attach_id: (f.name, f)},
    )
    status, result = await client.send_post(
        f"{SERVER_URL}/bot{token}/senddocument",
        stream,
        headers={"Content-Type": stream.content_type},
    )

    assert route.called
    assert status == 200
    assert result == response_content


@pytest.mark.asyncio
async def test_stream(respx_mock, request_stream, client):
    content = [b'chunk' + str(x).encode() for x in range(5)]

    async def aiter_stream():
        for chunk in content:
            yield chunk

    url = request_stream
    _ = respx_mock.get(url).respond(status_code=200, stream=aiter_stream())
    async with client.file_stream(url, len(content[0])) as response:
        status, stream = response
        assert status == 200
        assert [x async for x in stream] == content


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "exc,orig",
    [
        (RequestTimeoutError, httpx.ReadTimeout("error")),
        (NetworkError, httpx.RequestError("error")),
    ]
)
async def test_post_error(respx_mock, client, exc, orig, request_json):
    _ = respx_mock.post().mock(side_effect=orig)
    with pytest.raises(exc) as error:
        await client.send_post(*request_json)

    assert error.value.orig is orig


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "exc,orig",
    [
        (RequestTimeoutError, httpx.ReadTimeout("error")),
        (NetworkError, httpx.RequestError("error")),
    ]
)
async def test_stream_error(respx_mock, exc, orig, request_stream, client):
    url = request_stream
    _ = respx_mock.get().mock(side_effect=orig)
    with pytest.raises(exc) as error:
        async with client.file_stream(url, 64):
            pass

    assert error.value.orig is orig
