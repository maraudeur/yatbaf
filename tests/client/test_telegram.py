import unittest.mock as mock

import pytest
import pytest_asyncio

from yatbaf.client.http import HttpClient
from yatbaf.client.models import ResponseOk
from yatbaf.client.telegram import SERVER_URL
from yatbaf.client.telegram import TelegramClient
from yatbaf.exceptions import ChatMigratedError
from yatbaf.exceptions import FileDownloadError
from yatbaf.exceptions import FloodError
from yatbaf.exceptions import InternalError
from yatbaf.exceptions import MethodInvokeError
from yatbaf.exceptions import TokenError
from yatbaf.exceptions import WebhookConflictError
from yatbaf.input_file import InputFile
from yatbaf.methods import GetMe
from yatbaf.methods import SendDocument
from yatbaf.types import Message
from yatbaf.types import User

TOKEN = "123456789:totkentest"
URL = f"{SERVER_URL}/bot{TOKEN}"


@pytest_asyncio.fixture
async def tg() -> TelegramClient:
    tg_client = TelegramClient(
        token=TOKEN,
        client=HttpClient(),
    )
    yield tg_client
    await tg_client.close()


@pytest.fixture
def json_error():
    return {
        "ok": False,
        "description": "description",
        "error_code": 400,
    }


def test_url():
    client = TelegramClient(TOKEN)
    assert client._meth_url == f"https://api.telegram.org/bot{TOKEN}"
    assert client._file_url == f"https://api.telegram.org/file/bot{TOKEN}"


def test_url_custom():
    client = TelegramClient(TOKEN, api_url="http://127.0.0.1:8080")
    assert client._meth_url == f"http://127.0.0.1:8080/bot{TOKEN}"
    assert client._file_url == f"http://127.0.0.1:8080/file/bot{TOKEN}"


def test_url_test_environment():
    client = TelegramClient(TOKEN, test_environment=True)
    assert client._meth_url == f"https://api.telegram.org/bot{TOKEN}/test"
    assert client._file_url == f"https://api.telegram.org/file/bot{TOKEN}/test"


@pytest.mark.asyncio
async def test_invoke(respx_mock, tg):
    json = {
        "ok": True,
        "result": {
            "id": 123456789,
            "is_bot": True,
            "first_name": "Test First Name",
        },
    }
    respx_mock.post(f"{URL}/getme").respond(status_code=200, json=json)
    resp = await tg.invoke(GetMe())

    assert isinstance(resp, ResponseOk)
    assert isinstance(resp.result, User)
    assert resp.result.id == json["result"]["id"]
    assert resp.result.is_bot


@pytest.mark.asyncio
async def test_invoke_files(respx_mock, tg):
    file_content = b"hello\nworld!"
    f = InputFile("file.txt", content=file_content)

    respx_mock.post(
        f"{URL}/senddocument",
        files__eq={
            f.attach_id: (f.name, file_content)
        },
        data__eq={
            "chat_id": 1,
            "document": f"attach://{f.attach_id}",
        },
    ).respond(
        status_code=200,
        json={
            "ok": True,
            "result": {
                "message_id": 1,
                "chat": {
                    "id": 1,
                    "type": "private",
                },
                "from": {
                    "id": 1,
                    "first_name": "test",
                    "is_bot": True,
                },
                "date": 1234567,
                "document": {
                    "file_id": "1",
                    "file_unique_id": "1",
                },
            },
        },
    )
    resp = await tg.invoke(SendDocument(chat_id=1, document=f))
    assert isinstance(resp, ResponseOk)
    assert isinstance(resp.result, Message)


@pytest.mark.asyncio
@mock.patch.object(TelegramClient, "invoke")
async def test_call(invoke_mock, tg):
    tg = TelegramClient(TOKEN)
    result = object()
    invoke_mock.return_value = mock.Mock(result=result)
    assert await tg(GetMe()) is result
    invoke_mock.assert_awaited_once_with(GetMe())


@pytest.mark.asyncio
async def test_file_stream(respx_mock, tg):
    content = b"hello world"

    async def stream_iter():
        for chunk in [content]:
            yield chunk

    respx_mock.get(f"{SERVER_URL}/file/bot{TOKEN}/file-path").respond(
        status_code=200, stream=stream_iter()
    )
    result = b""
    async for data in tg.iter_file("file-path"):
        result += data
    assert result == content


@pytest.mark.asyncio
async def test_token_error(respx_mock, tg, json_error):
    status = 401
    json_error["error_code"] = status

    respx_mock.post().respond(status_code=status, json=json_error)
    with pytest.raises(TokenError):
        await tg.invoke(GetMe())


@pytest.mark.asyncio
async def test_webhook_error(respx_mock, tg, json_error):
    status = 409
    json_error["error_code"] = status

    respx_mock.post().respond(status_code=status, json=json_error)
    with pytest.raises(WebhookConflictError):
        await tg.invoke(GetMe())


@pytest.mark.asyncio
async def test_flood_error(respx_mock, tg, json_error):
    status = 429
    json_error["error_code"] = status
    json_error["parameters"] = {"retry_after": 123}

    respx_mock.post().respond(status_code=status, json=json_error)
    with pytest.raises(FloodError) as error:
        await tg.invoke(GetMe())

    assert error.value.retry_after == 123


@pytest.mark.asyncio
async def test_chat_migrated_error(respx_mock, tg, json_error):
    status = 400
    json_error["error_code"] = status
    json_error["parameters"] = {"migrate_to_chat_id": 123}

    respx_mock.post().respond(status_code=status, json=json_error)
    with pytest.raises(ChatMigratedError) as error:
        await tg.invoke(GetMe())

    assert error.value.migrate_to_chat_id == 123


@pytest.mark.asyncio
async def test_file_error(respx_mock, tg, json_error):
    status = 400
    json_error["error_code"] = status

    respx_mock.get().respond(status_code=status, json=json_error)
    with pytest.raises(FileDownloadError) as error:
        async for data in tg.iter_file("file-path"):
            pass

    assert error.value.error_code == status
    assert error.value.description == json_error["description"]


@pytest.mark.asyncio
async def test_internal_error(respx_mock, tg, json_error):
    status = 500
    json_error["error_code"] = status

    respx_mock.post().respond(status_code=status, json=json_error)
    with pytest.raises(InternalError):
        await tg.invoke(GetMe())


@pytest.mark.asyncio
async def test_method_error(respx_mock, tg, json_error):
    status = 400
    json_error["error_code"] = status

    respx_mock.post().respond(status_code=status, json=json_error)
    with pytest.raises(MethodInvokeError) as error:
        await tg.invoke(GetMe())

    assert error.value.error_code == status
    assert error.value.description == json_error["description"]
