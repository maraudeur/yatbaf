import pytest

from yatbaf.client.multipart import MultipartStream
from yatbaf.input_file import InputFile


@pytest.mark.asyncio
async def test_multipart():
    data = {"k1": b"v1", "k2": b"v2"}
    files = {"fid1": ("filename", InputFile("filename", content=b"content"))}
    multipart = MultipartStream(data, files)

    result = b""
    async for chunk in multipart:
        result += chunk

    boundary = multipart._boundary.encode()
    expect = b"".join([
        b'--%s\r\n' % boundary,
        b'Content-Disposition: form-data; name="k1"\r\n\r\n',
        b'v1\r\n'
        b'--%s\r\n' % boundary,
        b'Content-Disposition: form-data; name="k2"\r\n\r\n',
        b'v2\r\n'
        b'--%s\r\n' % boundary,
        b'Content-Disposition: form-data; name="fid1"; filename="filename"\r\n\r\n',  # noqa: E501
        b'content\r\n'
        b'--%s--\r\n' % boundary,
    ])

    assert result == expect
