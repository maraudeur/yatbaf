import pytest

from yatbaf.state import State


def test_init():
    state = State({"x": 1})
    assert state._data == {"x": 1}


def test_set():
    state = State()
    state.x = 1
    state["y"] = 2
    assert state._data == {"x": 1, "y": 2}


def test_get():
    state = State({"x": 1, "y": 2})
    assert state.x == 1
    assert state["y"] == 2

    with pytest.raises(AttributeError):
        state.z

    with pytest.raises(KeyError):
        state["z"]


def test_del():
    state = State({"x": 1, "y": 2})
    del state.x
    del state["y"]
    assert state._data == {}


def test_len():
    state = State({"x": 1, "y": 2})
    assert len(state) == 2


def test_iter():
    state = State({"x": 1, "y": 2})
    assert list(iter(state)) == ["x", "y"]


def test_contains():
    state = State({"x": 1, "y": 2})
    assert "x" in state
