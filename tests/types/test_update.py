import pytest

from yatbaf.types import CallbackQuery
from yatbaf.types import Chat
from yatbaf.types import Message
from yatbaf.types import Update
from yatbaf.types import User


@pytest.mark.parametrize(
    "event_name,event_model",
    [
        [
            "message",
            Message(
                message_id=1,
                chat=Chat(
                    id=1,
                    type="private",
                ),
                from_=User(
                    id=1,
                    is_bot=False,
                    first_name="user",
                ),
                date=123456789,
            )
        ],
        [
            "callback_query",
            CallbackQuery(
                id=1,
                from_=User(
                    id=1,
                    is_bot=False,
                    first_name="user",
                ),
                chat_instance="1",
            ),
        ],
    ]
)
def test_event(event_name, event_model):
    id = 123
    update = Update(**{"update_id": id, event_name: event_model})
    assert update.update_id == id
    assert update.event_type == event_name
    assert update.event is event_model
