import pytest

from yatbaf.helpers.states import ChatState
from yatbaf.helpers.states import ConversationState
from yatbaf.helpers.states import UserState
from yatbaf.storage.memory import Memory
from yatbaf.storage.registry import StorageRegistry
from yatbaf.types import CallbackQuery
from yatbaf.types.abc import _bot_ctx

STORAGE = Memory()


@pytest.fixture(autouse=True)
def __setup_bot(bot_mock):
    bot_mock.storage = StorageRegistry({"state": STORAGE})
    token = _bot_ctx.set(bot_mock)
    yield
    _bot_ctx.reset(token)


def test_chat_state(chat):
    assert chat._objdata.get("state") is None
    state = chat.state
    assert isinstance(state, ChatState)
    assert chat._objdata.get("state") is state
    assert state._storage is STORAGE


def test_user_state(user):
    assert user._objdata.get("state") is None
    state = user.state
    assert isinstance(state, UserState)
    assert user._objdata.get("state") is state
    assert state._storage is STORAGE


def test_message_conversation_state(message):
    assert message._objdata.get("state") is None
    state = message.state
    assert isinstance(state, ConversationState)
    assert message._objdata.get("state") is state
    assert state._storage is STORAGE


def test_callback_query_conversation_state(message, user):
    query = CallbackQuery(
        id="1",
        from_=user,
        message=message,
        chat_instance="1",
    )

    assert query._objdata.get("state") is None
    state = query.state
    assert isinstance(state, ConversationState)
    assert query._objdata.get("state") is state
    assert state._storage is STORAGE
