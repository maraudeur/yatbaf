import unittest.mock as mock

import pytest

from yatbaf.types import File
from yatbaf.types.abc import _bot_ctx


async def read(it):
    result = b""
    async for data in it:
        result += data
    return result


async def mock_iter_file():
    yield b"foo"
    yield b"bar"


@pytest.fixture(autouse=True)
def __setup_bot(bot_mock):
    bot_mock._api_client.iter_file = mock.Mock(
        side_effect=lambda *_: mock_iter_file()
    )

    token = _bot_ctx.set(bot_mock)
    yield
    _bot_ctx.reset(token)


@pytest.mark.asyncio
async def test_iter_read_file(bot_mock):
    expect = b"foobar"

    file_path = "path/to/file"
    file = File(file_id="1", file_unique_id="1", file_path=file_path)
    result_iter = await read(file)
    result_read = await file.read()
    bot_mock._api_client.iter_file.assert_any_call(file_path)

    assert result_read == expect
    assert result_iter == expect


@pytest.mark.asyncio
async def test_iter_read_local_file(tmp_path, bot_mock):
    expect = b"foobar"
    file_path = tmp_path / "test_read"
    file_path.write_bytes(expect)

    bot_mock.config.local_mode = True
    file = File(file_id="1", file_unique_id="1", file_path=str(file_path))
    result_iter = await read(file)
    result_read = await file.read()

    assert result_iter == expect
    assert result_read == expect


@pytest.mark.asyncio
async def test_save(tmp_path):
    file_path = tmp_path / "test_save"
    file = File(file_id="1", file_unique_id="1", file_path=file_path)
    await file.save(str(file_path))

    assert file_path.read_bytes() == b"foobar"
