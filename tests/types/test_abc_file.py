import unittest.mock as mock

import pytest

from yatbaf.types.abc import TelegramTypeFile
from yatbaf.types.abc import _bot_ctx


@pytest.fixture(autouse=True)
def __setup_bot_ctx(bot_mock):
    token = _bot_ctx.set(bot_mock)
    yield
    _bot_ctx.reset(token)


class _FileObj(TelegramTypeFile):
    file_id: str


@pytest.mark.asyncio
async def test_read(bot_mock):
    bot_mock.read_file = mock.AsyncMock(return_value=b"content")
    obj = _FileObj("1")
    assert await obj.read() == b"content"
    bot_mock.read_file.assert_awaited_once_with("1")


@pytest.mark.asyncio
async def test_save_file(bot_mock):
    obj = _FileObj("1")
    await obj.save_file("/tmp")
    bot_mock.save_file.assert_awaited_once_with("1", "/tmp")
