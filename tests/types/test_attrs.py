def test_attr_is_private(message):
    assert "__usrctx__" not in message.__struct_fields__
    assert "__usrctx__" not in message.__slots__
