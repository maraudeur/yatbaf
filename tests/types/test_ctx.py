from yatbaf.state import State
from yatbaf.types import Update


def test_lazy_ctx():
    obj = Update(update_id=1)
    assert not hasattr(obj, "__usrctx__")
    assert obj._objdata == {}
    assert hasattr(obj, "__usrctx__")
    assert obj._objdata is obj.__usrctx__
    assert obj.ctx == State()
    assert obj.__usrctx__ == {"ctx": State()}


def test_ctx():
    obj = Update(update_id=1)
    obj.ctx.foo = "bar"
    assert obj._objdata["ctx"]["foo"] == "bar"
    assert obj.__usrctx__["ctx"]["foo"] == "bar"
