import unittest.mock as mock
from contextvars import ContextVar

import pytest


def test_bot_not_bound(message):
    with pytest.raises(RuntimeError):
        message.bot


@mock.patch("yatbaf.types.abc._bot_ctx", ContextVar("_bot_ctx_test"))
def test_bind_bot_obj(message):
    bot = object()
    message._bind_bot_obj(bot)
    assert message.bot is bot
    assert message.from_.bot is bot
    assert message.chat.bot is bot
