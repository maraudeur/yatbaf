import unittest.mock as mock

import pytest

from yatbaf.api import ApiMethods
from yatbaf.bot import Bot


@pytest.fixture
def file_obj():
    return mock.AsyncMock(read=mock.AsyncMock(return_value=b"content"))


@pytest.fixture
def bot_mock(monkeypatch, token, file_obj):
    monkeypatch.setattr(
        ApiMethods,
        "get_file",
        mock.AsyncMock(return_value=file_obj),
    )
    return Bot(token=token)


@pytest.mark.asyncio
async def test_read_file(bot_mock):
    content = await bot_mock.read_file("1")
    assert content == b"content"


@pytest.mark.asyncio
async def test_save_file(bot_mock, file_obj):
    await bot_mock.save_file("1", "/tmp")
    file_obj.save.assert_awaited_once_with("/tmp")
