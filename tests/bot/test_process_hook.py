import unittest.mock as mock
from contextvars import ContextVar

import pytest

from yatbaf.bot import Bot
from yatbaf.handlers import on_update
from yatbaf.types import abc as types_abc


@pytest.fixture(autouse=True)
def __setup_bot_ctx(monkeypatch):
    monkeypatch.setattr(types_abc, "_bot_ctx", ContextVar("_bot_ctx_test"))


@pytest.fixture(autouse=True)
def __setup_update_ctx(update):
    update.ctx.test = []


def get_handler():

    @on_update
    async def _handler(update):
        update.ctx.test.append("handler")

    return _handler


async def before_hook(update):
    update.ctx.test.append("before")


async def after_hook(update, _):
    update.ctx.test.append("after")


async def hook_error(*_):
    raise ValueError


@pytest.mark.asyncio
async def test_before_process_update(token, update):
    bot = Bot(
        token=token,
        handlers=[get_handler()],
        before_process_update=before_hook,
    )
    await bot.process_update(update)
    assert update.ctx.test == ["before", "handler"]


@pytest.mark.asyncio
async def test_after_process_update(token, update):
    bot = Bot(
        token=token,
        handlers=[get_handler()],
        after_process_update=after_hook,
    )
    await bot.process_update(update)
    assert update.ctx.test == ["handler", "after"]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "args,result",
    [  # yapf: disable
        [{"before_process_update": hook_error}, []],
        [{"after_process_update": hook_error}, ["handler"]],
    ]
)
async def test_hook_error(token, args, result, update):
    error_handler = mock.AsyncMock()
    bot = Bot(
        token=token,
        handlers=[get_handler()],
        error_handlers={ValueError: error_handler},
        **args,
    )
    await bot.process_update(update)
    assert update.ctx.test == result
    error_handler.assert_awaited_once()
