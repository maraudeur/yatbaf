import unittest.mock as mock

import pytest

from yatbaf.bot import Bot
from yatbaf.methods.send_message import SendMessage


@pytest.mark.asyncio
async def test_hook(monkeypatch, token):

    async def before(method):
        method.text = "bar"

    bot = Bot(token, [], before_method_call=before)
    monkeypatch.setattr(bot, "_api_client", client := mock.AsyncMock())

    await bot.send_message(1, "foo")
    client.assert_awaited_once_with(SendMessage(chat_id=1, text="bar"))
