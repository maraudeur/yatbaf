from unittest import mock

import pytest

from yatbaf.bot import Bot
from yatbaf.bot import _default_error_handler
from yatbaf.exceptions import MethodInvokeError
from yatbaf.handlers.base import Result
from yatbaf.handlers.handler import on_update

exc = ValueError("1")
default_error_handler = mock.AsyncMock()


@pytest.fixture(autouse=True)
def __reset_mock():
    default_error_handler.reset_mock()


@pytest.mark.asyncio
async def test_on_error_default(token):
    bot = Bot(token=token)
    await bot._on_error(exc, default_error_handler)
    default_error_handler.assert_awaited_once_with(bot, exc)


@pytest.mark.asyncio
async def test_on_error(token):
    error_handler = mock.AsyncMock()
    bot = Bot(
        token=token,
        error_handlers={ValueError: error_handler},
    )
    await bot._on_error(exc, default_error_handler)
    error_handler.assert_awaited_once_with(bot, exc)
    default_error_handler.assert_not_awaited()


@pytest.mark.asyncio
async def test_error_in_error_handler(token):
    bot = Bot(token=token)

    async def error_handler(b, exc):  # noqa: U100
        raise ValueError

    await bot._on_error(ValueError("1"), error_handler)


@pytest.mark.asyncio
@mock.patch.object(Bot, "_on_error", mock.AsyncMock())
async def test_error_in_handler(token, update):

    @on_update
    async def handler(_):
        raise exc

    bot = Bot(token=token, handlers=[handler])
    result = await bot.process_update(update)
    bot._on_error.assert_awaited_once_with(exc, _default_error_handler)
    assert isinstance(result, Result)
    assert result.response is None


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "error,has_update", [
        [ValueError("1"), False],
        [MethodInvokeError(400, "error"), True],
    ]
)
@mock.patch.object(Bot, "_on_error")
async def test_update_obj(on_error_mock, token, update, error, has_update):
    on_error_mock.reset_mock()

    @on_update
    async def handler(_):
        raise error

    bot = Bot(token=token, handlers=[handler])
    await bot.process_update(update)

    bot._on_error.assert_awaited_once_with(error, _default_error_handler)
    assert hasattr(error, "update") is has_update
    if has_update:
        assert error.update is update
