from contextlib import asynccontextmanager
from unittest import mock

import pytest

from yatbaf.bot import Bot


@pytest.fixture
def shutdown_mock():
    return mock.AsyncMock()


@pytest.mark.asyncio
async def test_lifespan(monkeypatch, token, shutdown_mock):
    monkeypatch.setattr(Bot, "shutdown", shutdown_mock)

    @asynccontextmanager
    async def lifespan(bot):
        assert isinstance(bot, Bot)
        yield

    async with Bot(token, [], lifespan=lifespan).lifespan():
        pass


@pytest.mark.asyncio
async def test_bot_shutdown(monkeypatch, token, shutdown_mock):
    monkeypatch.setattr(Bot, "shutdown", shutdown_mock)
    async with Bot(token, []).lifespan():
        pass
    shutdown_mock.assert_awaited_once()


@pytest.mark.asyncio
async def test_startup_error(monkeypatch, token, shutdown_mock):
    monkeypatch.setattr(Bot, "shutdown", shutdown_mock)

    @asynccontextmanager
    async def error(_):
        raise ValueError()
        yield

    with pytest.raises(ValueError):
        async with Bot(token, [], lifespan=error).lifespan():
            pass
    shutdown_mock.assert_awaited_once()


@pytest.mark.asyncio
async def test_shutdown_error(monkeypatch, token, shutdown_mock):
    monkeypatch.setattr(Bot, "shutdown", shutdown_mock)

    @asynccontextmanager
    async def error(_):
        yield
        raise ValueError()

    with pytest.raises(ValueError):
        async with Bot(token, [], lifespan=error).lifespan():
            pass
    shutdown_mock.assert_awaited_once()


@pytest.mark.asyncio
async def test_startup_shutdown(monkeypatch, token):
    order = []
    monkeypatch.setattr(
        Bot, "shutdown", mock.AsyncMock(side_effect=lambda: order.append(3))
    )

    @asynccontextmanager
    async def lifespan(_):
        order.append(1)
        yield
        order.append(2)

    async with Bot(token, [], lifespan=lifespan).lifespan():
        pass

    assert order == [1, 2, 3]


@pytest.mark.asyncio
async def test_lifespan_stack(monkeypatch, token):
    order = []
    monkeypatch.setattr(
        Bot, "shutdown", mock.AsyncMock(side_effect=lambda: order.append("sh"))
    )

    @asynccontextmanager
    async def lifespan1(_):
        order.append(1)
        yield
        order.append(4)

    @asynccontextmanager
    async def lifespan2(_):
        order.append(2)
        yield
        order.append(3)

    bot = Bot(token, [], lifespan=[lifespan1, lifespan2])
    async with bot.lifespan():
        pass

    assert order == [1, 2, 3, 4, "sh"]


@pytest.mark.asyncio
async def test_func(token):
    order = []

    def create_fn(mark):

        async def _fn(_):
            order.append(mark)

        return _fn

    @asynccontextmanager
    async def lifespan1(_):
        order.append(3)
        yield
        order.append(6)

    @asynccontextmanager
    async def lifespan2(_):
        order.append(4)
        yield
        order.append(5)

    bot = Bot(
        token,
        [],
        lifespan=[
            lifespan1,
            lifespan2,
        ],
        on_startup=[
            create_fn(1),
            create_fn(2),
        ],
        on_shutdown=[
            create_fn(7),
            create_fn(8),
        ],
    )
    async with bot.lifespan():
        pass

    assert order == [1, 2, 3, 4, 5, 6, 7, 8]
