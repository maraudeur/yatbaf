from os import environ

import pytest

from yatbaf.bot import Bot
from yatbaf.exceptions import InvalidTokenError


def test_token_error():
    with pytest.raises(InvalidTokenError):
        Bot()


def test_token_env(token):
    environ["YATBAF_TOKEN"] = token
    bot = Bot()
    assert bot.config.token == token


def test_token_param(token):
    environ["YATBAF_TOKEN"] = "12345:mytoken"
    bot = Bot(token)
    assert bot.config.token == token
