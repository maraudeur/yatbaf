import pytest

from yatbaf.methods import Close
from yatbaf.methods import GetMe
from yatbaf.methods import GetUpdates
from yatbaf.methods import StopMessageLiveLocation
from yatbaf.methods.abc import TelegramMethodWithFile
from yatbaf.types import Message
from yatbaf.types import Update
from yatbaf.types import User


def test_base_subclass_attrs():
    assert not hasattr(TelegramMethodWithFile, "__meth_name__")
    assert not hasattr(TelegramMethodWithFile, "__meth_result_model__")


def test_attrs():
    assert hasattr(GetMe, "__meth_name__")
    assert hasattr(GetMe, "__meth_result_model__")


def test_method_name():
    assert GetMe.__meth_name__ == "getme"
    assert str(GetMe()) == "getme"
    assert GetMe()._get_name() == "getme"


@pytest.mark.parametrize(
    "method,model",
    (
        (GetMe, User),
        (Close, bool),
        (GetUpdates, list[Update]),
        (StopMessageLiveLocation, Message | bool),
    ),
)
def test_method_result_model(method, model):
    assert method.__meth_result_model__ == model
    assert method._get_result_model() == model
