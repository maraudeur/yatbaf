from secrets import token_urlsafe
from uuid import uuid4

from msgspec import json as jsonlib

from yatbaf.enums import ParseMode
from yatbaf.input_file import InputFile
from yatbaf.methods import EditMessageMedia
from yatbaf.methods import GetMe
from yatbaf.methods import SendMediaGroup
from yatbaf.methods import SendMessage
from yatbaf.methods import SendPaidMedia
from yatbaf.methods import SendPhoto
from yatbaf.types import InputMediaDocument
from yatbaf.types import InputMediaPhoto
from yatbaf.types import InputPaidMediaPhoto
from yatbaf.types import ReplyParameters

encoder = jsonlib.Encoder().encode
decoder = jsonlib.Decoder().decode


def new_file() -> InputFile:
    file_content = uuid4().hex.encode("utf_8")
    file_name = f"{token_urlsafe(6)}.txt"
    return InputFile(file_name, content=file_content)


def test_encode_empty():
    method = GetMe()
    data, files = method._encode_params()
    assert data == b"{}"
    assert files is None


def test_encode_params():
    method = SendMessage(
        chat_id=12345,
        text="*foo bar*",
        disable_notification=True,
        parse_mode=ParseMode.MARKDOWN,
    )
    data, files = method._encode_params()
    assert decoder(data) == {
        "chat_id": 12345,
        "text": "*foo bar*",
        "disable_notification": True,
        "parse_mode": "MarkdownV2",
    }
    assert files is None


def test_encode_file_id():
    method = SendPhoto(chat_id=123, photo="fileid")
    data, files = method._encode_params()
    assert data == b'{"chat_id":123,"photo":"fileid"}'
    assert files is None


def test_encode_file_content():
    photo = new_file()
    method = SendPhoto(chat_id=123, photo=photo)
    data, files = method._encode_params()
    assert data == {
        "chat_id": b"123",
        "photo": f"attach://{photo.attach_id}".encode(),
    }
    assert files[photo.attach_id] == (photo.name, photo)


def test_encode_media_file_content():
    photo = new_file()
    method = EditMessageMedia(
        chat_id=12345,
        message_id=12345,
        media=InputMediaPhoto(media=photo),
    )
    data, files = method._encode_params()

    assert data == {  # yapf: disable
        "chat_id": b"12345",
        "message_id": b"12345",
        "media": f'{{"media":"attach://{photo.attach_id}","type":"photo"}}'.encode(),  # noqa: E501
    }
    assert files[photo.attach_id] == (photo.name, photo)


def test_encode_media_filed_id():
    method = EditMessageMedia(
        chat_id=12345,
        message_id=12345,
        media=InputMediaPhoto(media="fileid"),
    )
    data, files = method._encode_params()

    assert decoder(data) == {
        "chat_id": 12345,
        "message_id": 12345,
        "media": {
            "media": "fileid",
            "type": "photo",
        }
    }
    assert files is None


def test_encode_media_group_file_content():
    photo = new_file()
    method = SendMediaGroup(
        media=[InputMediaPhoto(media=photo)],
        chat_id=12345,
        disable_notification=True,
        reply_parameters=ReplyParameters(
            message_id=1234,
            chat_id=12345,
        ),
    )
    data, files = method._encode_params()

    assert data == {  # yapf: disable
        "chat_id": b"12345",
        "media": f'[{{"media":"attach://{photo.attach_id}","type":"photo"}}]'.encode(),  # noqa: E501
        "disable_notification": b"true",
        "reply_parameters": b'{"message_id":1234,"chat_id":12345}',
    }
    assert files[photo.attach_id] == (photo.name, photo)


def test_encode_media_group_file_content_1():
    doc1 = new_file()
    doc2 = new_file()
    doc2_th = new_file()
    doc3 = new_file()

    method = SendMediaGroup(
        media=[
            InputMediaDocument(media=doc1),
            InputMediaDocument(media=doc2, thumbnail=doc2_th),
            InputMediaDocument(media=doc3),
        ],
        chat_id=12345,
    )
    data, files = method._encode_params()

    assert files is not None
    assert isinstance(data, dict)

    assert data == {
        "chat_id": b"12345",
        "media": encoder([
            {
                "media": f"attach://{doc1.attach_id}",
                "type": "document",
            },
            {
                "media": f"attach://{doc2.attach_id}",
                "thumbnail": f"attach://{doc2_th.attach_id}",
                "type": "document",
            },
            {
                "media": f"attach://{doc3.attach_id}",
                "type": "document",
            },
        ])
    }
    assert files[doc1.attach_id] == (doc1.name, doc1)
    assert files[doc2.attach_id] == (doc2.name, doc2)
    assert files[doc2_th.attach_id] == (doc2_th.name, doc2_th)
    assert files[doc3.attach_id] == (doc3.name, doc3)


def test_encode_media_group_file_id_content_mix():
    doc_th = new_file()
    doc = new_file()

    method = SendMediaGroup(
        media=[
            InputMediaDocument(media="fileid1"),
            InputMediaDocument(media="fileid2", thumbnail=doc_th),
            InputMediaDocument(media=doc),
        ],
        chat_id=12345,
    )
    data, files = method._encode_params()

    assert data == {
        "chat_id": b"12345",
        "media": encoder([
            {
                "media": "fileid1",
                "type": "document",
            },
            {
                "media": "fileid2",
                "thumbnail": f"attach://{doc_th.attach_id}",
                "type": "document",
            },
            {
                "media": f"attach://{doc.attach_id}",
                "type": "document",
            },
        ]),
    }

    assert files[doc_th.attach_id] == (doc_th.name, doc_th)
    assert files[doc.attach_id] == (doc.name, doc)


def test_encode_media_group_file_id():
    method = SendMediaGroup(
        media=[
            InputMediaDocument(media="fileid1"),
            InputMediaDocument(media="fileid2", thumbnail="thid"),
            InputMediaDocument(media="fileid3"),
        ],
        chat_id=12345,
    )
    data, files = method._encode_params()

    assert decoder(data) == {
        "chat_id": 12345,
        "media": [
            {
                "media": "fileid1",
                "type": "document",
            },
            {
                "media": "fileid2",
                "thumbnail": "thid",
                "type": "document",
            },
            {
                "media": "fileid3",
                "type": "document",
            },
        ],
    }
    assert files is None


def test_paid_media_group_content():
    photo1 = new_file()
    photo2 = new_file()
    method = SendPaidMedia(
        chat_id=12345,
        star_count=50,
        media=[
            InputPaidMediaPhoto(media=photo1),
            InputPaidMediaPhoto(media=photo2),
        ],
        disable_notification=True,
        protect_content=True,
    )
    data, files = method._encode_params()
    assert data == {
        "chat_id": b"12345",
        "star_count": b"50",
        "media": (
            f'[{{"type":"photo","media":"attach://{photo1.attach_id}"}},'
            f'{{"type":"photo","media":"attach://{photo2.attach_id}"}}]'
        ).encode(),
        "disable_notification": b"true",
        "protect_content": b"true",
    }
    assert files[photo1.attach_id] == (photo1.name, photo1)
    assert files[photo2.attach_id] == (photo2.name, photo2)


def test_paid_media_group_id():
    method = SendPaidMedia(
        chat_id=12345,
        star_count=50,
        media=[
            InputPaidMediaPhoto(media="file1"),
            InputPaidMediaPhoto(media="file2"),
        ],
        disable_notification=True,
        protect_content=True,
    )
    data, files = method._encode_params()
    assert decoder(data) == {
        "chat_id": 12345,
        "star_count": 50,
        "media": [
            {
                "type": "photo",
                "media": "file1",
            },
            {
                "type": "photo",
                "media": "file2",
            },
        ],
        "disable_notification": True,
        "protect_content": True,
    }
    assert files is None
