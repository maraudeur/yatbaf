import pytest

from yatbaf.helpers.keyboard import InlineKeyboardBuilder
from yatbaf.types import CallbackGame
from yatbaf.types import CopyTextButton
from yatbaf.types import InlineKeyboardButton
from yatbaf.types import InlineKeyboardMarkup
from yatbaf.types import LoginUrl
from yatbaf.types import SwitchInlineQueryChosenChat
from yatbaf.types import WebAppInfo


def test_keyboard():
    keyboard = InlineKeyboardBuilder().build()
    assert keyboard == InlineKeyboardMarkup(inline_keyboard=[[]])


@pytest.mark.parametrize(
    "text,data,data_expect", [
        ("foo", "data", "data"),
        ("bar", None, "bar"),
    ]
)
def test_callback_button(text, data, data_expect):
    keyboard = InlineKeyboardBuilder().callback(text, data=data).build()
    assert keyboard == InlineKeyboardMarkup(
        inline_keyboard=[[
            InlineKeyboardButton(
                text=text,
                callback_data=data_expect,
            )
        ]]
    )


def test_url_button():
    keyboard = InlineKeyboardBuilder().url("btn", "https://example.com").build()
    assert keyboard == InlineKeyboardMarkup(
        inline_keyboard=[[
            InlineKeyboardButton(
                text="btn",
                url="https://example.com",
            )
        ]]
    )


def test_app_button():
    keyboard = InlineKeyboardBuilder().app("btn", "app.example.com").build()
    assert keyboard == InlineKeyboardMarkup(
        inline_keyboard=[[
            InlineKeyboardButton(
                text="btn",
                web_app=WebAppInfo(url="app.example.com"),
            )
        ]]
    )


def test_login_button():
    keyboard = (  # yapf: disable
        InlineKeyboardBuilder()
        .login(
            "btn", "https://example.com",
            forward_text="foo",
            bot_username="bar",
            request_write_access=True,
        )
    ).build()
    assert keyboard == InlineKeyboardMarkup(
        inline_keyboard=[[
            InlineKeyboardButton(
                text="btn",
                login_url=LoginUrl(
                    url="https://example.com",
                    forward_text="foo",
                    bot_username="bar",
                    request_write_access=True,
                ),
            )
        ]]
    )


def test_inline_query_button():
    keyboard = InlineKeyboardBuilder().inline_query("btn", "q").build()
    assert keyboard == InlineKeyboardMarkup(
        inline_keyboard=[[
            InlineKeyboardButton(
                text="btn",
                switch_inline_query="q",
            )
        ]]
    )


def test_inline_query_current_button():
    keyboard = InlineKeyboardBuilder().inline_query_current("btn", "q").build()
    assert keyboard == InlineKeyboardMarkup(
        inline_keyboard=[[
            InlineKeyboardButton(
                text="btn",
                switch_inline_query_current_chat="q",
            )
        ]]
    )


def test_inline_query_chosen_button():
    keyboard = InlineKeyboardBuilder().inline_query_chosen(
        "btn",
        "q",
        allow_user_chats=True,
        allow_bot_chats=True,
        allow_group_chats=True,
        allow_channel_chats=True,
    ).build()
    assert keyboard == InlineKeyboardMarkup(
        inline_keyboard=[[
            InlineKeyboardButton(
                text="btn",
                switch_inline_query_chosen_chat=SwitchInlineQueryChosenChat(
                    query="q",
                    allow_user_chats=True,
                    allow_bot_chats=True,
                    allow_group_chats=True,
                    allow_channel_chats=True,
                ),
            )
        ]]
    )


def test_copy_text_button():
    keyboard = InlineKeyboardBuilder().copy_text("btn", "foo").build()
    assert keyboard == InlineKeyboardMarkup(
        inline_keyboard=[[
            InlineKeyboardButton(
                text="btn",
                copy_text=CopyTextButton(text="foo"),
            )
        ]]
    )


def test_game_button():
    keyboard = InlineKeyboardBuilder().game("btn").build()
    assert keyboard == InlineKeyboardMarkup(
        inline_keyboard=[[
            InlineKeyboardButton(
                text="btn",
                callback_game=CallbackGame(),
            )
        ]]
    )


def test_pay_button():
    keyboard = InlineKeyboardBuilder().pay("btn").build()
    assert keyboard == InlineKeyboardMarkup(
        inline_keyboard=[[InlineKeyboardButton(text="btn", pay=True)]]
    )


def test_new_row():
    keyboard = (  # yapf: disable
        InlineKeyboardBuilder()
        .url("btn1", "example.com").url("btn2", "example.com")
        .new_row()
        .url("btn3", "example.com")
        .new_row()
        .url("btn4", "example.com")
    ).build()
    assert keyboard == InlineKeyboardMarkup(
        inline_keyboard=[
            [
                InlineKeyboardButton(text="btn1", url="example.com"),
                InlineKeyboardButton(text="btn2", url="example.com"),
            ],
            [
                InlineKeyboardButton(text="btn3", url="example.com"),
            ],
            [
                InlineKeyboardButton(text="btn4", url="example.com"),
            ],
        ]
    )
