from yatbaf.enums import PollType
from yatbaf.helpers.keyboard import KeyboardBuilder
from yatbaf.types import ChatAdministratorRights
from yatbaf.types import KeyboardButton
from yatbaf.types import KeyboardButtonPollType
from yatbaf.types import KeyboardButtonRequestChat
from yatbaf.types import KeyboardButtonRequestUsers
from yatbaf.types import ReplyKeyboardMarkup
from yatbaf.types import WebAppInfo


def test_keyboard():
    keyboard = KeyboardBuilder().build()
    assert keyboard == ReplyKeyboardMarkup(keyboard=[[]])


def test_row_length():
    keyboard = KeyboardBuilder()
    assert keyboard.row_length == 0
    keyboard.text("foo")
    assert keyboard.row_length == 1


def test_keyboard_opts():
    keyboard = (  # yapf: disable
        KeyboardBuilder()
        .opts(
            is_persistent=True,
            resize_keyboard=True,
            one_time_keyboard=True,
            input_field_placeholder="foo",
            selective=False,
        )
    ).build()
    assert keyboard == ReplyKeyboardMarkup(
        is_persistent=True,
        resize_keyboard=True,
        one_time_keyboard=True,
        input_field_placeholder="foo",
        selective=False,
        keyboard=[[]],
    )


def test_text_button():
    keyboard = KeyboardBuilder().text("foo").build()
    assert keyboard == ReplyKeyboardMarkup(keyboard=[["foo"]])


def test_request_users_button():
    keyboard = KeyboardBuilder().request_users(
        "select",
        1,
        user_is_bot=True,
        user_is_premium=True,
        max_quantity=10,
        request_name=True,
        request_username=True,
        request_photo=True
    ).build()
    assert keyboard == ReplyKeyboardMarkup(
        keyboard=[[
            KeyboardButton(
                text="select",
                request_users=KeyboardButtonRequestUsers(
                    request_id=1,
                    user_is_bot=True,
                    user_is_premium=True,
                    max_quantity=10,
                    request_name=True,
                    request_username=True,
                    request_photo=True
                )
            )
        ]]
    )


def test_request_chat_button():
    user_rights = ChatAdministratorRights(
        is_anonymous=False,
        can_manage_chat=False,
        can_delete_messages=True,
        can_manage_video_chats=True,
        can_restrict_members=True,
        can_promote_members=True,
        can_change_info=True,
        can_invite_users=True,
    )
    bot_rights = ChatAdministratorRights(
        is_anonymous=False,
        can_manage_chat=False,
        can_delete_messages=True,
        can_manage_video_chats=True,
        can_restrict_members=True,
        can_promote_members=True,
        can_change_info=True,
        can_invite_users=True,
        can_post_messages=True,
        can_edit_messages=True,
    )

    keyboard = KeyboardBuilder().request_chat(
        "select",
        1,
        chat_is_channel=True,
        chat_is_forum=True,
        chat_has_username=True,
        chat_is_created=True,
        user_administrator_rights=user_rights,
        bot_administrator_rights=bot_rights,
        bot_is_member=False,
        request_title=True,
        request_username=True,
        request_photo=True,
    ).build()
    assert keyboard == ReplyKeyboardMarkup(
        keyboard=[[
            KeyboardButton(
                text="select",
                request_chat=KeyboardButtonRequestChat(
                    request_id=1,
                    chat_is_channel=True,
                    chat_is_forum=True,
                    chat_has_username=True,
                    chat_is_created=True,
                    user_administrator_rights=user_rights,
                    bot_administrator_rights=bot_rights,
                    bot_is_member=False,
                    request_title=True,
                    request_username=True,
                    request_photo=True,
                )
            )
        ]]
    )


def test_request_contact_button():
    keyboard = KeyboardBuilder().request_contact("btn").build()
    assert keyboard == ReplyKeyboardMarkup(
        keyboard=[[KeyboardButton(text="btn", request_contact=True)]]
    )


def test_request_location_button():
    keyboard = KeyboardBuilder().request_location("btn").build()
    assert keyboard == ReplyKeyboardMarkup(
        keyboard=[[KeyboardButton(text="btn", request_location=True)]]
    )


def test_request_poll_button():
    keyboard = KeyboardBuilder().request_poll("btn", PollType.REGULAR).build()
    assert keyboard == ReplyKeyboardMarkup(
        keyboard=[[
            KeyboardButton(
                text="btn",
                request_poll=KeyboardButtonPollType(PollType.REGULAR),
            )
        ]]
    )


def test_web_app_button():
    keyboard = KeyboardBuilder().web_app("btn",
                                         "https://example.com/app").build()
    assert keyboard == ReplyKeyboardMarkup(
        keyboard=[[
            KeyboardButton(
                text="btn",
                web_app=WebAppInfo(url="https://example.com/app"),
            )
        ]]
    )


def test_new_row():
    keyboard = (  # yapf: disable
        KeyboardBuilder()
        .text("btn1").text("btn2")
        .new_row()
        .text("btn3")
        .new_row()
        .text("btn4")
    ).build()
    assert keyboard == ReplyKeyboardMarkup(
        keyboard=[
            ["btn1", "btn2"],
            ["btn3"],
            ["btn4"],
        ]
    )
