from pathlib import Path

import pytest

from yatbaf.io import AsyncFile
from yatbaf.io import aopen


@pytest.mark.asyncio
async def test_aopen():
    async with aopen(__file__, "rb") as f:
        assert isinstance(f, AsyncFile)

        orig = f._fp
        assert not orig.closed
        assert orig.readable()
        assert not orig.writable()

    assert orig.closed


class TestAsynFile:

    @pytest.mark.asyncio
    async def test_read(self, tmp_path):
        file = tmp_path / "file.b"
        Path(file).write_text("content\n")

        async with aopen(file, "rb") as f:
            assert await f.read() == b"content\n"

        async with aopen(file, "rb") as f:
            assert await f.read(1) == b"c"

        async with aopen(file, "r") as f:
            assert await f.read() == "content\n"

        async with aopen(file, "r") as f:
            assert await f.read(1) == "c"

    @pytest.mark.asyncio
    async def test_write(self, tmp_path):
        file = tmp_path / "file.b"

        async with aopen(file, "w") as f:
            await f.write("content")

        assert Path(file).read_text() == "content"
