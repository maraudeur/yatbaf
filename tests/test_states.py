import pytest

from yatbaf.helpers.states import ChatState
from yatbaf.helpers.states import ConversationState
from yatbaf.helpers.states import State
from yatbaf.helpers.states import UserState
from yatbaf.storage.memory import Memory
from yatbaf.storage.memory import StorageObject


@pytest.fixture
def storage():
    return Memory()


@pytest.mark.parametrize(
    "value,expect",
    [
        [b"foo", ("foo", None)],
        [b"foo=bar", ("foo", "bar")],
        [b"foo=bar=baz", ("foo", "bar=baz")],
    ]
)
def test_parse_state_value(value, expect):
    assert State._parse_state_value(value) == expect


@pytest.mark.parametrize(
    "value,data,expect", [
        ["foo", None, "foo"],
        ["foo", "bar", "foo=bar"],
    ]
)
def test_create_state_value(value, data, expect):
    assert State._create_state_value(value, data) == expect


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "value,data,expect", [
        ["foo", None, b"foo"],
        ["foo", "bar", b"foo=bar"],
    ]
)
async def test_set(storage, value, data, expect):
    state = State(storage=storage)
    await state.set(value, data)
    assert storage._data.get("state").value == expect


@pytest.mark.asyncio
async def test_set_none(storage):
    state = State(storage=storage)
    storage._data["state"] = StorageObject(b"foo")

    await state.set(None)
    assert storage._data.get("state") is None


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "value,expect",
    [
        ["foo", ("foo", None)],
        ["foo=bar", ("foo", "bar")],
        ["foo=bar=baz", ("foo", "bar=baz")],
    ]
)
async def test_get(storage, value, expect):
    state = State(storage=storage)
    storage._data["state"] = StorageObject(value.encode("utf-8"))
    assert await state.get() == expect


@pytest.mark.asyncio
async def test_get_empty(storage):
    state = State(storage=storage)
    assert await state.get() == (None, None)


def test_user_state_key(storage):
    state = UserState(user_id=1, storage=storage)
    assert state._key == "user:1"


def test_chat_state_key(storage):
    state = ChatState(chat_id=1, storage=storage)
    assert state._key == "chat:1"


def test_conversation_state_key(storage):
    state = ConversationState(
        chat_id=1,
        user_id=2,
        business_id=3,
        thread_id=None,
        storage=storage,
    )
    assert state._key == "conv:1.2.3.-"

    state = ConversationState(
        chat_id=1,
        user_id=2,
        business_id=None,
        thread_id=None,
        storage=storage,
    )
    assert state._key == "conv:1.2.-.-"

    state = ConversationState(
        chat_id=1,
        user_id=2,
        business_id=None,
        thread_id=4,
        storage=storage,
    )
    assert state._key == "conv:1.2.-.4"
