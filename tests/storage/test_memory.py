import time

import pytest

from yatbaf.storage.memory import Memory
from yatbaf.storage.memory import StorageObject

TIME_NOW = int(time.time())


@pytest.mark.parametrize(
    "ex,result", [
        [TIME_NOW + 5, False],
        [TIME_NOW - 5, True],
    ]
)
def test_storage_object_expired(ex, result):
    obj = StorageObject(b"foo", ex)
    assert obj.expired is result


def test_storage_object_set_ttl():
    obj = StorageObject(b"foo")
    assert obj.expires_at is None
    obj.set_ttl(100)
    assert obj.expires_at == int(time.time()) + 100


@pytest.mark.asyncio
async def test_set():
    storage = Memory()
    await storage.set("key", "foo")
    assert storage._data["key"] == StorageObject(b"foo", None)


@pytest.mark.asyncio
async def test_set_ex():
    storage = Memory()
    await storage.set("key", "foo", 200)
    assert storage._data["key"] == StorageObject(b"foo", int(time.time()) + 200)


@pytest.mark.asyncio
async def test_get():
    storage = Memory()
    storage._data["key"] = StorageObject(b"foo")
    assert await storage.get("key") == b"foo"
    assert "key" in storage._data


@pytest.mark.asyncio
async def test_get_none():
    storage = Memory()
    assert await storage.get("key") is None


@pytest.mark.asyncio
async def test_get_expired():
    storage = Memory()
    storage._data["key"] = StorageObject(b"foo", int(time.time()) - 5)
    assert await storage.get("key") is None
    assert not storage._data


@pytest.mark.asyncio
async def test_get_ex():
    storage = Memory()
    storage._data["key"] = StorageObject(b"foo", int(time.time()) + 10)
    assert await storage.get("key", 100) == b"foo"
    assert storage._data["key"].expires_at == int(time.time()) + 100


@pytest.mark.asyncio
async def test_delete():
    storage = Memory()
    storage._data["key"] = StorageObject(b"foo")
    await storage.delete("key")
    assert not storage._data


@pytest.mark.asyncio
async def test_clear():
    storage = Memory()
    storage._data = {
        "key1": StorageObject(b"foo"),
        "key2": StorageObject(b"foo"),
    }
    await storage.clear()
    assert not storage._data


@pytest.mark.asyncio
async def test_delete_expired():
    time_now = int(time.time())
    storage = Memory()
    storage._data = {
        "key1": StorageObject(b"val1", time_now + 100),
        "key2": StorageObject(b"val2", time_now - 100),
        "key3": StorageObject(b"val3", time_now + 100),
        "key4": StorageObject(b"val4", time_now - 100),
        "key5": StorageObject(b"val5", time_now + 100),
    }

    await storage.delete_expired()
    assert "key1" in storage._data
    assert "key2" not in storage._data
    assert "key3" in storage._data
    assert "key4" not in storage._data
    assert "key5" in storage._data
