from yatbaf.storage.memory import Memory
from yatbaf.storage.registry import StorageRegistry
from yatbaf.storage.registry import _default_factory


def test_instance():
    registry = StorageRegistry()
    assert registry._storage == {}
    assert registry._default_factory is _default_factory


def test_init_storage():
    storage = Memory()
    registry = StorageRegistry({"foo": storage})
    assert registry._storage == {"foo": storage}
    assert registry._default_factory is _default_factory


def test_default():
    registry = StorageRegistry()
    assert not registry._storage
    assert isinstance(registry.get("foo"), Memory)
    assert "foo" in registry._storage


def test_default_factory():
    memory = Memory()

    def factory():
        return memory

    registry = StorageRegistry(default_factory=factory)
    assert not registry._storage
    assert registry.get("foo") is memory
