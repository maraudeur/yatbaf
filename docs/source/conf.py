from yatbaf.version import __version__

project = "yatbaf"
author = "maraudeur"
copyright = f"2023, {author}"
version = ".".join(__version__.split(".")[:2])
release = __version__

extensions = [
    "sphinx.ext.intersphinx",
    "sphinx.ext.autodoc",
    "sphinx.ext.autosectionlabel",
    "sphinx_copybutton",
    "sphinx_design",
]

intersphinx_mapping = {
    "python": ("https://docs.python.org/3", None),
    "msgspec": ("https://jcristharif.com/msgspec/", None),
}

source_suffix = {
    '.rst': 'restructuredtext',
}

source_encoding = "utf-8"

autodoc_member_order = "bysource"
autodoc_default_options = {
    "special-members": "__init__",
    "show-inheritance": True,
    "members": True,
}
autodoc_typehint_format = "short"
autodoc_typehints = "signature"
autodoc_class_signature = "separated"

nitpicky = True

html_theme = "pydata_sphinx_theme"
html_show_sourcelink = False

html_theme_options = {
    "logo": {
        "text": "yatbaf",
    },
    "icon_links": [
        {
            "name": "Codeberg",
            "url": "https://codeberg.org/maraudeur/yatbaf",
            "icon": "fa-brands fa-git",
        },
    ],
    "use_edit_page_button": False,
    "show_toc_level": 2,
    "footer_start": ["copyright"],
    "footer_center": ["sphinx-version"],
}

html_static_path = ["_static"]
# html_css_files = ["custom.css"]
