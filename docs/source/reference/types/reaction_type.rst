ReactionType
============

.. automodule:: yatbaf.types.reaction_type

    .. autoclass:: yatbaf.types.reaction_type.ReactionType
