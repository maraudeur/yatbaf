MenuButton
==========

.. automodule:: yatbaf.types.menu_button

    .. autoclass:: yatbaf.types.menu_button.MenuButton
