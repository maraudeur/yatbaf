BotCommandScope
=================

.. automodule:: yatbaf.types.bot_command_scope

    .. autoclass:: yatbaf.types.bot_command_scope.BotCommandScope
