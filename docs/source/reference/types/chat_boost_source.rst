ChatBoostSource
================

.. automodule:: yatbaf.types.chat_boost_source

   .. autoclass:: yatbaf.types.chat_boost_source.ChatBoostSource
