MessageOrigin
=============

.. automodule:: yatbaf.types.message_origin

    .. autoclass:: yatbaf.types.message_origin.MessageOrigin
