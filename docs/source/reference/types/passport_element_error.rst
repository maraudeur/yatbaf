PassportElementError
====================

.. automodule:: yatbaf.types.passport_element_error

    .. autoclass:: yatbaf.types.passport_element_error.PassportElementError
