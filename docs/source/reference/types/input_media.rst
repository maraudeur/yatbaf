InputMedia
==========

.. automodule:: yatbaf.types.input_media

    .. autoclass:: yatbaf.types.input_media.InputMedia
