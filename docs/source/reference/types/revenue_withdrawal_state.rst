RevenueWithdrawalState
======================

.. automodule:: yatbaf.types.revenue_withdrawal_state

    .. autoclass:: yatbaf.types.revenue_withdrawal_state.RevenueWithdrawalState
