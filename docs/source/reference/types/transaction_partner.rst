TransactionPartner
==================

.. automodule:: yatbaf.types.transaction_partner

    .. autoclass:: yatbaf.types.transaction_partner.TransactionPartner
