enums
=====

.. automodule:: yatbaf.enums
    :members:
    :undoc-members:
    :exclude-members: __new__
