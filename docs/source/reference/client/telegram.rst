telegram
========

.. automodule:: yatbaf.client.telegram
    :show-inheritance:

    .. autodata:: yatbaf.client.telegram.SERVER_URL
