group
=====

.. automodule:: yatbaf.handlers.group
    :show-inheritance:

    .. autoclass:: yatbaf.handlers.group.HandlerGroup
        :special-members: __call__
