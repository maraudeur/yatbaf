DeleteChatPhoto
===============

.. automodule:: yatbaf.methods.delete_chat_photo
    :undoc-members:
