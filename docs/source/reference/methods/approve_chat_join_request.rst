ApproveChatJoinRequest
======================

.. automodule:: yatbaf.methods.approve_chat_join_request
    :undoc-members:
