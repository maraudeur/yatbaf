DeleteWebhook
=============

.. automodule:: yatbaf.methods.delete_webhook
    :undoc-members:
