GetMyDefaultAdministratorRights
===============================

.. automodule:: yatbaf.methods.get_my_default_administrator_rights
    :undoc-members:
