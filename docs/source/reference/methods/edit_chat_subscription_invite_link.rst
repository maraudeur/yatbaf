EditChatSubscriptionInviteLink
==============================

.. automodule:: yatbaf.methods.edit_chat_subscription_invite_link
    :undoc-members:
