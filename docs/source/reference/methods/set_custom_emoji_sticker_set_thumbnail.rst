SetCustomEmojiStickerSetThumbnail
=================================

.. automodule:: yatbaf.methods.set_custom_emoji_sticker_set_thumbnail
    :undoc-members:
