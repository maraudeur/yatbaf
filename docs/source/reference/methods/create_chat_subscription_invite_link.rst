CreateChatSubscriptionInviteLink
================================

.. automodule:: yatbaf.methods.create_chat_subscription_invite_link
    :undoc-members:
