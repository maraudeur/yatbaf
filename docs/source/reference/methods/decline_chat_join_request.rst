DeclineChatJoinRequest
======================

.. automodule:: yatbaf.methods.decline_chat_join_request
    :undoc-members:
