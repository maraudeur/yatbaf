GetMyShortDescription
=====================

.. automodule:: yatbaf.methods.get_my_short_description
    :undoc-members:
