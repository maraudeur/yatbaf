SavePreparedInlineMessage
=========================

.. automodule:: yatbaf.methods.save_prepared_inline_message
    :undoc-members:
