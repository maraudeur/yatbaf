GetUserProfilePhotos
====================

.. automodule:: yatbaf.methods.get_user_profile_photos
    :undoc-members:
