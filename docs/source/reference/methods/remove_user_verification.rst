RemoveUserVerification
======================

.. automodule:: yatbaf.methods.remove_user_verification
    :undoc-members:
