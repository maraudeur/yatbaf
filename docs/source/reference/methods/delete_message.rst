DeleteMessage
=============

.. automodule:: yatbaf.methods.delete_message
    :undoc-members:
