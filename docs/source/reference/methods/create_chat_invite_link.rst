CreateChatInviteLink
====================

.. automodule:: yatbaf.methods.create_chat_invite_link
    :undoc-members:
