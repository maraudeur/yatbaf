SetChatPermissions
==================

.. automodule:: yatbaf.methods.set_chat_permissions
    :undoc-members:
