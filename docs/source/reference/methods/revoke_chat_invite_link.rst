RevokeChatInviteLink
====================

.. automodule:: yatbaf.methods.revoke_chat_invite_link
    :undoc-members:
