EditChatInviteLink
==================

.. automodule:: yatbaf.methods.edit_chat_invite_link
    :undoc-members:
