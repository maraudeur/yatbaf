DeleteChatStickerSet
====================

.. automodule:: yatbaf.methods.delete_chat_sticker_set
    :undoc-members:
