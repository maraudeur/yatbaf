SetMyDefaultAdministratorRights
===============================

.. automodule:: yatbaf.methods.set_my_default_administrator_rights
    :undoc-members:
