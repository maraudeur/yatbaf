DeleteStickerFromSet
====================

.. automodule:: yatbaf.methods.delete_sticker_from_set
    :undoc-members:
