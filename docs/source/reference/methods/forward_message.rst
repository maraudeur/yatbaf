ForwardMessage
==============

.. automodule:: yatbaf.methods.forward_message
    :undoc-members:
