SetUserEmojiStatus
==================

.. automodule:: yatbaf.methods.set_user_emoji_status
    :undoc-members:
