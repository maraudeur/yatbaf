RestrictChatMember
==================

.. automodule:: yatbaf.methods.restrict_chat_member
    :undoc-members:
