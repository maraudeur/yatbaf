typing
======

.. autoclass:: yatbaf.typing.EventT
.. autoclass:: yatbaf.typing.ResultT
.. autoclass:: yatbaf.typing.EventModel
.. autoclass:: yatbaf.typing.ReplyMarkup

.. autoclass:: yatbaf.typing.NoneInt
.. autoclass:: yatbaf.typing.NoneStr
.. autoclass:: yatbaf.typing.NoneBool

.. autoclass:: yatbaf.typing.CallableAny
.. autoclass:: yatbaf.typing.AsyncCallableNone
.. autoclass:: yatbaf.typing.Wrapper

.. autoclass:: yatbaf.typing.MiddlewareCallable
.. autoclass:: yatbaf.typing.MiddlewareCallableArgs

.. autoclass:: yatbaf.typing.HandlerCallback
.. autoclass:: yatbaf.typing.HandlerCallable
.. autoclass:: yatbaf.typing.HandlerGuard
.. autoclass:: yatbaf.typing.HandlerMiddleware
.. autoclass:: yatbaf.typing.HandlerMiddlewareArgs
.. autoclass:: yatbaf.typing.HandlerDependency
.. autoclass:: yatbaf.typing.HandlerPriority

.. autoclass:: yatbaf.typing.Scope

.. autoclass:: yatbaf.typing.PollingErrorHandler

.. autoclass:: yatbaf.typing.FilterMetadata
.. autoclass:: yatbaf.typing.FilterWeight

.. autoclass:: yatbaf.typing.Lifespan
