:html_theme.sidebar_secondary.remove:

yatbaf
======

Asynchronous Telegram Bot API framework.

.. code-block:: python

    from yatbaf import Bot, on_message


    @on_message
    async def echo(message):
        await message.answer(message.text)


    Bot("<replace-with-your-token>", [echo]).run()

.. toctree::
    :hidden:

    usage/index
    reference/index
