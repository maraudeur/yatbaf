from __future__ import annotations

from typing import TYPE_CHECKING

from yatbaf import Bot
from yatbaf import on_message
from yatbaf.filters import text

if TYPE_CHECKING:
    from yatbaf.types import Message


@on_message(filters=[text])
async def echo(message: Message) -> None:
    await message.answer(message.text)


if __name__ == "__main__":
    import os

    from yatbaf.exceptions import InvalidTokenError

    try:
        Bot(os.getenv("BOT_TOKEN", ""), [echo]).run()
    except InvalidTokenError:
        print("token empty")
