from __future__ import annotations

from pathlib import Path
from time import time
from typing import TYPE_CHECKING

import yatbaf.filters as f
from yatbaf import Bot
from yatbaf import on_message
from yatbaf.input_file import InputFile

if TYPE_CHECKING:
    from yatbaf.types import Message

CWD = Path(__file__).parent


@on_message(filters=[f.Command("file")])
async def upload(message: Message) -> None:
    file = message.bot.state.get("file")
    if file is None:
        file_path = CWD / "media/img.png"
        file = InputFile("img.png", path=file_path)
    result = await message.answer_photo(file)
    message.bot.state.file = result.photo[-1].file_id


@on_message(filters=[f.photo])
async def download(message: Message) -> None:
    if not message.bot.config.local_mode:
        file_path = CWD / f"media/{int(time())}"
        try:
            await message.photo[-1].save_file(file_path)
        except Exception:
            return

    await message.set_reaction_emoji("👌")


if __name__ == "__main__":
    import os

    from yatbaf.exceptions import InvalidTokenError

    try:
        Bot(os.getenv("BOT_TOKEN", ""), [download, upload]).run()
    except InvalidTokenError:
        print("token empty")
