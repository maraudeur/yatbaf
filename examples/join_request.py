from __future__ import annotations

from typing import TYPE_CHECKING

from yatbaf import Bot
from yatbaf import on_chat_join_request

if TYPE_CHECKING:
    from yatbaf.types import ChatJoinRequest


@on_chat_join_request
async def join_request(r: ChatJoinRequest) -> None:
    await r.approve()


if __name__ == "__main__":
    import os

    from yatbaf.exceptions import InvalidTokenError

    try:
        Bot(os.getenv("BOT_TOKEN", ""), [join_request]).run()
    except InvalidTokenError:
        print("tonen empty")
