from __future__ import annotations

from typing import TYPE_CHECKING

from yatbaf import Bot
from yatbaf import on_message
from yatbaf.filters import Command

if TYPE_CHECKING:
    from yatbaf.types import Message


@on_message(filters=[Command("ping")])
async def ping(message: Message) -> None:
    await message.reply("pong")


if __name__ == "__main__":
    import os

    from yatbaf.exceptions import InvalidTokenError

    try:
        Bot(os.getenv("BOT_TOKEN", ""), [ping]).run()
    except InvalidTokenError:
        print("token empty")
