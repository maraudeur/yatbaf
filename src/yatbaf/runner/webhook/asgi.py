from __future__ import annotations

__all__ = ("Asgi",)

import io
import logging
from traceback import format_exc
from typing import TYPE_CHECKING
from typing import Any
from typing import Final
from typing import TypeAlias

from yatbaf.client.multipart import MultipartStream
from yatbaf.client.utils import decode_webhook
from yatbaf.exceptions import JSONDecodeError

from .hooks import shutdown_delete
from .hooks import startup_check
from .hooks import startup_setup

if TYPE_CHECKING:
    from collections.abc import AsyncIterable
    from collections.abc import Awaitable
    from collections.abc import Callable
    from collections.abc import Iterable

    from yatbaf.bot import Bot
    from yatbaf.methods.abc import TelegramMethod

log = logging.getLogger(__name__)

Scope: TypeAlias = "dict[str, Any]"
Receive: TypeAlias = "Callable[[], Awaitable[dict[str, Any]]]"
Send: TypeAlias = "Callable[[dict[str, Any]], Awaitable[None]]"
AsgiApp: TypeAlias = "Callable[[Scope, Receive, Send], Awaitable[None]]"

SECRET_HEADER: Final = "x-telegram-bot-api-secret-token".encode("ascii")


def _prepare_response(
    method: TelegramMethod | None
) -> tuple[Iterable[tuple[bytes, bytes]], bytes | AsyncIterable[bytes]]:
    headers: Iterable[tuple[bytes, bytes]]
    if method is None:
        headers = (
            (b"content-length", b"0"),
            (b"content-type", b"application/json"),
        )
        return (headers, b"")

    method._method = method.__meth_name__
    data, files = method._encode_params()
    if files is None:
        json: bytes = data  # type: ignore[assignment]
        headers = (
            (b"content-length", str(len(json)).encode()),
            (b"content-type", b"application/json"),
        )
        return (headers, json)

    stream = MultipartStream(data, files)  # type: ignore[arg-type]
    headers = ((b"content-type", stream.content_type.encode()),)
    return (headers, stream)


async def _send_response(
    send: Send,
    status: int,
    headers: Iterable[tuple[bytes, bytes]],
    body: bytes | AsyncIterable,
) -> None:
    await send({
        "type": "http.response.start",
        "status": status,
        "headers": headers,
    })
    if isinstance(body, bytes):
        await send({
            "type": "http.response.body",
            "body": body,
            "more_body": False,
        })
    else:
        async for chunk in body:
            await send({
                "type": "http.response.body",
                "body": chunk,
                "more_body": True,
            })
        await send({
            "type": "http.response.body",
            "body": b"",
            "more_body": False,
        })


async def _send_error(send: Send, status: int = 500) -> None:
    await _send_response(
        send=send,
        status=status,
        headers=(
            (b"content-length", b"0"),
            (b"content-type", b"application/json"),
        ),
        body=b"",
    )


async def _read_body(receive: Receive) -> bytes | None:
    result = io.BytesIO()
    while True:
        message = await receive()
        if message["type"] != "http.request":
            return None
        result.write(message["body"])
        if not message.get("more_body", False):
            break
    return result.getvalue()


class Asgi:

    __slots__ = ("_bot",)

    def __init__(self, bot: Bot, /) -> None:
        if bot.config.webhook is None:
            raise ValueError("webhook config not provided.")

        self._bot = bot
        if bot.config.webhook.set_on_startup:
            bot._on_startup.append(startup_setup)
        bot._on_startup.append(startup_check)
        if bot.config.webhook.delete_on_shutdown:
            bot._on_shutdown.append(shutdown_delete)

        bot.config.runner = "webhook"

    async def __call__(
        self, scope: Scope, receive: Receive, send: Send
    ) -> None:
        type_ = scope["type"]
        if type_ == "http":
            match, error_code = self._match(scope)
            if not match:
                await _send_error(send, error_code)  # type: ignore[arg-type]
                return
            await self.http_handler(scope, receive, send)
        elif type_ == "lifespan":
            await self._lifespan_handler(receive, send)

    async def _lifespan_handler(self, receive: Receive, send: Send) -> None:
        await receive()
        started = False
        try:
            async with self._bot.lifespan():
                await send({"type": "lifespan.startup.complete"})
                started = True
                await receive()
        except BaseException:
            event = "shutdown" if started else "startup"
            await send({
                "type": f"lifespan.{event}.failed",
                "message": format_exc(),
            })
            raise

        await send({"type": "lifespan.shutdown.complete"})

    async def http_handler(
        self, scope: Scope, receive: Receive, send: Send
    ) -> None:
        if not self._is_authenticated(scope):
            await _send_error(send, 403)
            return

        body = await _read_body(receive)
        if not body:  # body is empty or client disconnected
            if body is not None:  # body is empty
                await _send_error(send, 400)
            return

        try:
            update = decode_webhook(body)
        except JSONDecodeError as error:
            log.error(f"got invalid json? {error}")
            await _send_error(send, 400)
            return

        result = await self._bot.process_update(update)
        headers, content = _prepare_response(result.response)
        await _send_response(send, 200, headers, content)

    def _match(self, scope: Scope) -> tuple[bool, int | None]:
        # only POST is allowed
        if scope["method"] != "POST":
            return False, 405

        path = self._bot.config.webhook.path  # type: ignore[union-attr]
        if path is not None:
            if scope["path"] != path:
                return False, 404

        return True, None

    def _is_authenticated(self, scope: Scope) -> bool:
        secret = self._bot.config.webhook.secret  # type: ignore[union-attr]
        if secret is not None:
            headers: Iterable[tuple[bytes, bytes]] = scope["headers"]
            for (name, value) in headers:
                if name == SECRET_HEADER:
                    return value.decode("utf_8") == secret
            return False
        return True
