from __future__ import annotations

__all__ = (
    "startup_setup",
    "startup_check",
    "shutdown_delete",
)

from typing import TYPE_CHECKING
from typing import cast

from yatbaf.exceptions import BotException

if TYPE_CHECKING:
    from yatbaf.bot import Bot
    from yatbaf.config import BotConfig
    from yatbaf.config import WebhookConfig
    from yatbaf.types import WebhookInfo


def _is_config_changed(webhook: WebhookInfo, config: BotConfig) -> bool:
    config.webhook = cast("WebhookConfig", config.webhook)

    if webhook.url != config.webhook.url:
        return True
    if set(webhook.allowed_updates or []) != set(config.allowed_updates or []):
        return True
    if webhook.ip_address != config.webhook.ip:
        return True
    if webhook.max_connections != config.webhook.max_connections:
        return True
    return False


async def startup_setup(bot: Bot) -> None:
    bot.config.webhook = cast("WebhookConfig", bot.config.webhook)

    if bot.config.webhook.url is None:
        raise BotException("url for webhook not set")

    if not bot.config.webhook.force_set:
        webhook_info = await bot.get_webhook_info()
        if webhook_info.url != "":
            if not _is_config_changed(webhook_info, bot.config):
                return

    await bot.set_webhook(
        url=bot.config.webhook.url,
        ip_address=bot.config.webhook.ip,
        allowed_updates=bot.config.allowed_updates,
        drop_pending_updates=bot.config.drop_pending_updates,
        max_connections=bot.config.webhook.max_connections,
        secret_token=bot.config.webhook.secret,
    )


async def startup_check(bot: Bot) -> None:
    if not (await bot.get_webhook_info()).url:
        raise BotException("webhook not set.")


async def shutdown_delete(bot: Bot) -> None:
    await bot.delete_webhook()
