from __future__ import annotations

__all__ = ("Webhook",)

import logging
from typing import TYPE_CHECKING

from yatbaf.runner.base import Runner

from .asgi import Asgi

try:
    import uvicorn

    has_uvicorn = True
except ImportError:
    has_uvicorn = False

if TYPE_CHECKING:
    from yatbaf.bot import Bot
    from yatbaf.config import WebhookConfig

log = logging.getLogger(__name__)


class Webhook(Runner):
    """Webhook runner."""

    __slots__ = ("_bot",)

    def __init__(self, bot: Bot, /) -> None:
        """:param bot: :class:`~yatbaf.bot.Bot` instance."""
        self._bot = bot

    async def run(self) -> None:
        if not has_uvicorn:
            log.error("uvicorn is not installed.")
            return

        conf: WebhookConfig = self._bot.config.webhook  # type: ignore[assignment]  # noqa: E501
        port = conf.runner_port or conf.port

        server = uvicorn.Server(
            uvicorn.Config(
                app=Asgi(self._bot),
                host="0.0.0.0",
                port=port,
                access_log=False,
                log_config=None,
                workers=None,
            )
        )
        await server.serve()
