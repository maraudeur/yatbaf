from __future__ import annotations

import asyncio
from abc import ABCMeta
from abc import abstractmethod

try:
    import uvloop

    _loop_factory = uvloop.new_event_loop
except ImportError:
    _loop_factory = None


class Runner(metaclass=ABCMeta):
    __slots__ = ()

    def start(self) -> None:
        try:
            with asyncio.Runner(loop_factory=_loop_factory) as runner:
                runner.run(self.run())
        except KeyboardInterrupt:
            pass

    @abstractmethod
    async def run(self) -> None:
        pass
