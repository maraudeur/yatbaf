from __future__ import annotations

__all__ = ("LongPolling",)

import asyncio
import logging
import signal
from typing import TYPE_CHECKING

from yatbaf.exceptions import NetworkError
from yatbaf.methods import GetUpdates

from .base import Runner

if TYPE_CHECKING:
    from types import FrameType

    from yatbaf.bot import Bot

log = logging.getLogger(__name__)

SIGNALS = (signal.SIGINT, signal.SIGTERM)


class LongPolling(Runner):
    """Long polling runner.

    .. warning::

        Long polling will not work if an outgoing webhook is set up.
    """

    __slots__ = (
        "_bot",
        "_running",
    )

    def __init__(self, bot: Bot, /) -> None:
        """:param bot: :class:`~yatbaf.bot.Bot` instance."""
        self._bot = bot
        self._running = False

        bot.config.runner = "polling"

    async def _get_updates(self, tg: asyncio.TaskGroup, /) -> None:
        bot = self._bot
        method = GetUpdates(
            offset=(-1 if bot.config.drop_pending_updates else None),
            limit=bot.config.polling.limit,
            timeout=bot.config.polling.timeout,
            allowed_updates=bot.config.allowed_updates,
        )
        timeout = method.timeout + 5.0  # type: ignore[operator]
        invoke = bot._api_client.invoke

        offset: int | None = None
        while self._running:
            try:
                updates = (await invoke(method, timeout=timeout)).result
            except Exception as error:
                await bot._on_error(error, _default_error_handler)
                continue

            if updates:
                for update in updates:
                    update_id = update.update_id
                    offset = update_id + 1
                    tg.create_task(
                        bot.process_update(update),
                        name=f"update-{update_id}",
                    )

            method.offset = offset

    async def run(self) -> None:
        if self._running:
            return

        async with self._bot.lifespan():
            self._running = True
            self._set_signals()
            await self._loop()

    async def _loop(self) -> None:
        async with asyncio.TaskGroup() as tg:
            task = asyncio.create_task(self._get_updates(tg))

            while self._running:
                await asyncio.sleep(1)

            try:
                task.cancel()
                await task
            except asyncio.CancelledError:
                pass

    def _set_signals(self) -> None:
        for sig in SIGNALS:
            signal.signal(sig, self._signal_handler)

    def _signal_handler(
        self,
        signum: int,  # noqa: U100
        frame: FrameType | None,  # noqa: U100
    ) -> None:
        self.stop()

    def stop(self) -> None:
        self._running = False


async def _default_error_handler(_: Bot, exc: Exception) -> None:  # noqa: U101
    delay = 5.0
    if isinstance(exc, NetworkError):
        delay = 20.0

    log.error(f"Polling error: {exc}. Next try in {delay} sec.")
    await asyncio.sleep(delay)
