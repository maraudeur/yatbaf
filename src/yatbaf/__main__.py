from .version import __api_version__
from .version import __version__

if __name__ == "__main__":  # pragma: no cover
    print(f"yatbaf {__version__}, Telegram Bot Api {__api_version__}")
