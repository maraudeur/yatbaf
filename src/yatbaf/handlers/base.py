from __future__ import annotations

__all__ = (
    "BaseHandler",
    "HandlerType",
    "Result",
)

from enum import StrEnum
from enum import auto
from typing import TYPE_CHECKING
from typing import Any
from typing import Generic
from typing import final

from msgspec import Struct

from yatbaf.di import Provide
from yatbaf.exceptions import FrozenInstanceError
from yatbaf.exceptions import GuardException
from yatbaf.filters.base import check_compatibility
from yatbaf.filters.base import merge_metadata
from yatbaf.typing import EventT
from yatbaf.utils import ensure_unique
from yatbaf.utils import wrap_middleware

if TYPE_CHECKING:
    from collections.abc import Iterable
    from collections.abc import Mapping
    from collections.abc import Sequence

    from yatbaf.enums import Event
    from yatbaf.filters.base import BaseFilter
    from yatbaf.methods.abc import TelegramMethod
    from yatbaf.types import Update
    from yatbaf.typing import FilterMetadata
    from yatbaf.typing import HandlerCallable
    from yatbaf.typing import HandlerGuard
    from yatbaf.typing import HandlerMiddleware
    from yatbaf.typing import HandlerPriority


class HandlerType(StrEnum):
    UPDATE = auto()

    MESSAGE = auto()
    EDITED_MESSAGE = auto()
    CHANNEL_POST = auto()
    EDITED_CHANNEL_POST = auto()
    BUSINESS_CONNECTION = auto()
    BUSINESS_MESSAGE = auto()
    EDITED_BUSINESS_MESSAGE = auto()
    DELETED_BUSINESS_MESSAGES = auto()
    MESSAGE_REACTION = auto()
    MESSAGE_REACTION_COUNT = auto()
    INLINE_QUERY = auto()
    CHOSEN_INLINE_RESULT = auto()
    CALLBACK_QUERY = auto()
    SHIPPING_QUERY = auto()
    PRE_CHECKOUT_QUERY = auto()
    PAID_MEDIA_PURCHASED = auto()
    POLL = auto()
    POLL_ANSWER = auto()
    MY_CHAT_MEMBER = auto()
    CHAT_MEMBER = auto()
    CHAT_JOIN_REQUEST = auto()
    CHAT_BOOST = auto()
    REMOVED_CHAT_BOOST = auto()


class Result(Struct):
    stop_propagate: bool = True
    response: TelegramMethod | None = None

    def __bool__(self) -> bool:
        return self.stop_propagate


class BaseHandler(Generic[EventT]):

    __slots__ = (
        "_guards",
        "_filters",
        "_middleware",
        "_filters",
        "_priority",
        "_dependencies",
        "_update_type",
        "_middleware_stack",
        "_frozen",
        "_stop_propagate",
        "_name",
    )

    def __init__(
        self,
        update_type: HandlerType,
        filters: Sequence[BaseFilter[EventT]] | None = None,
        guards: Sequence[HandlerGuard[EventT]] | None = None,
        middleware: Sequence[HandlerMiddleware[EventT]] | None = None,
        dependencies: Mapping[str, Provide] | None = None,
        name: str | None = None,
        stop_propagate: bool = True,
    ) -> None:
        """
        :param update_type: Handler type. See :class:`~yatbaf.enums.Event`
        :param filters: *Optional.* A sequence of :class:`~yatbaf.filters.base.BaseFilter`.
        :param guards: *Optional.* A sequence of :class:`~yatbaf.typing.HandlerGuard`.
        :param middleware: *Optional.* A sequence of :class:`~yatbaf.typing.HandlerMiddleware`.
        :param dependencies: *Optional.* A mapping of dependency providers.
        :param name: *Optional.* Handler name.
        """  # noqa: E501
        self._filters = list(filters or [])
        self._guards = list(guards or [])
        self._middleware = list(middleware or [])
        self._middleware_stack: HandlerCallable[EventT] = self._handle
        # content, user, chat
        self._priority: HandlerPriority = (
            (0, (0, 0)),
            (0, (0, 0)),
            (0, (0, 0)),
        )
        self._dependencies = dependencies or {}
        self._update_type = update_type
        self._stop_propagate = stop_propagate
        self._frozen = False
        self._name = name or self.__class__.__name__

    @final
    def __bool__(self) -> bool:
        return True

    @final
    def __lt__(self, other: object) -> bool:
        if not isinstance(other, BaseHandler):
            return NotImplemented

        s_prior = self._priority
        o_prior = other._priority

        # priority (max, min) [content, sender, chat]
        sp = (s_prior[0][1], s_prior[1][1], s_prior[2][1])
        op = (o_prior[0][1], o_prior[1][1], o_prior[2][1])

        # priority max [content, sender, chat]
        sp_max = (sp[0][0], sp[1][0], sp[2][0])
        op_max = (op[0][0], op[1][0], op[2][0])

        # priority min [content, sender, chat]
        sp_min = (sp[0][1], sp[1][1], sp[2][1])
        op_min = (op[0][1], op[1][1], op[2][1])

        # number of filters [chat, sender, content]
        ss = (s_prior[2][0], s_prior[1][0], s_prior[0][0])
        os = (o_prior[2][0], o_prior[1][0], o_prior[0][0])

        if sp_max == op_max:
            if ss == os:
                return sp_min < op_min
            return ss < os
        return sp_max < op_max

    @final
    def __gt__(self, other: object, /) -> bool:
        return NotImplemented

    @property
    def name(self) -> str:
        """Handler name."""
        return self._name

    @property
    def update_type(self) -> HandlerType:
        """Handler type."""
        return self._update_type

    async def _check_guards(self, update: EventT) -> None:
        for guard in self._guards:
            await guard(update)

    async def match(self, update: EventT, /) -> bool:
        if self._filters:
            for filter in self._filters:
                if not await filter.check(update):
                    return False
        return True

    async def handle(self, update: EventT, /) -> Result:
        return await self._middleware_stack(update)

    async def _handle(self, update: EventT, /) -> Result:
        if self._guards:
            try:
                await self._check_guards(update)
            except GuardException:
                return Result(self._stop_propagate)
        return await self._process_update(update)

    def _resolve_guards(
        self,
        guards: Iterable[HandlerGuard[EventT]] | None = None,
    ) -> None:
        self._guards = ensure_unique((*(guards or ()), *self._guards))

    def _resolve_middleware(
        self,
        middleware: Iterable[HandlerMiddleware[EventT]] | None = None,
    ) -> None:
        self._middleware_stack = wrap_middleware(
            self._handle,
            ensure_unique((*(middleware or ()), *self._middleware)),
        )

    def _resolve_filters(self) -> None:
        check_compatibility(self._filters, False)
        self._priority = self._parse_priority(self._filters)

    @staticmethod
    def _parse_priority(filters: Sequence[BaseFilter]) -> HandlerPriority:
        data: FilterMetadata = {}
        for filter in filters:
            data = merge_metadata(data, filter.metadata)

        tmp: list[tuple[int, tuple[int, int]]] = []
        for group in ("content", "sender", "chat"):
            if priority := data.get(group):  # type: ignore[call-overload]
                if isinstance(min_max := priority[1], int):
                    tmp.append((priority[0], (min_max, min_max)))
                else:
                    tmp.append(priority)
            else:
                tmp.append((0, (0, 0)))
        return tuple(tmp)  # type: ignore[return-value]

    def _check_frozen_handler(self) -> None:
        if self._frozen:
            raise FrozenInstanceError(
                f"{self!r} is already registered. "
                "It is not possible to use the same handler twice."
            )

    def __eq__(self, other: object, /) -> bool:
        raise NotImplementedError

    async def _process_update(self, update: EventT, /) -> Result:
        raise NotImplementedError

    def on_registration(  # yapf: disable
        self,
        guards: Iterable[HandlerGuard[EventT]] | None = None,  # noqa: U100
        middleware: Iterable[HandlerMiddleware[EventT]] | None = None,  # noqa: U100,E501
        dependencies: Mapping[str, Provide] | None = None,  # noqa: U100
    ) -> None:
        """Init handler."""
        raise NotImplementedError


class _UpdateHandler(BaseHandler["Update"]):
    """Default handler for Update."""

    __slots__ = ("_handlers",)

    def __init__(self, handlers: Mapping[Event, BaseHandler[Any]]) -> None:
        super().__init__(
            update_type=HandlerType.UPDATE,
            name="yatbaf-update",
        )
        self._handlers = handlers

    def __eq__(self, other: object) -> bool:
        return (  # yapf: disable
            isinstance(other, _UpdateHandler) and (
                other is self or (
                    other._handlers == self._handlers
                )
            )
        )

    async def _process_update(self, update: Update) -> Result:
        type_ = update.event_type
        if handler := self._handlers.get(type_):  # type: ignore[call-overload]
            event = update.event
            if await handler.match(event):
                result = await handler.handle(event)
                return result  # type: ignore[no-any-return]
        return Result()

    def _prepare_handlers(
        self, dependencies: Mapping[str, Provide] | None = None
    ) -> None:
        for handler in self._handlers.values():
            handler.on_registration(dependencies=dependencies)

    def on_registration(
        self,
        guards: Iterable[HandlerGuard[Update]] | None = None,
        middleware: Iterable[HandlerMiddleware[Update]] | None = None,
        dependencies: Mapping[str, Provide] | None = None,
    ) -> None:
        self._check_frozen_handler()
        self._resolve_middleware(middleware)
        self._resolve_guards(guards)
        self._prepare_handlers(dependencies)
        self._frozen = True
