from __future__ import annotations

__all__ = (
    "merge_handlers",
    "parse_handlers",
)

from collections import defaultdict
from typing import TYPE_CHECKING
from typing import Any

from .base import BaseHandler
from .base import _UpdateHandler
from .group import HandlerGroup

if TYPE_CHECKING:
    from collections.abc import Sequence

    from yatbaf.types import Update


def merge_handlers(handlers: Sequence[BaseHandler[Any]]) -> BaseHandler[Update]:
    handlers_dict = parse_handlers(handlers)
    if (update_handler := handlers_dict.get("update")):
        if len(handlers_dict) != 1:
            raise ValueError(
                "Update and Event handlers are mutually exclusive."
            )
        return update_handler
    return _UpdateHandler(handlers_dict)  # type: ignore[arg-type]


def parse_handlers(
    handlers: Sequence[BaseHandler[Any]]
) -> dict[str, BaseHandler[Any]]:
    sorted_: dict[str, list[BaseHandler]] = defaultdict(list)
    for obj in handlers:
        sorted_[obj.update_type].append(obj)

    result = {}
    for type_, list_ in sorted_.items():
        result[type_] = (
            HandlerGroup(update_type=type_, handlers=list_)
            if len(list_) != 1 else list_[0]
        )

    return result
