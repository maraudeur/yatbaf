"""
Exception hierarchy:

BotException
 ├── InvalidTokenError
 ├── FrozenInstanceError
 ├── FilterCompatError
 ├── DependencyError
 └── ClientError
      ├── RequestError
      │    ├── RequestTimeoutError
      │    └── NetworkError
      └── JSONDecodeError

TelegramApiException
 ├── TokenError
 ├── WebhookConflictError
 ├── MethodInvokeError
 │    ├── ChatMigratedError
 │    └── FloodError
 ├── FileDownloadError
 └── InternalError

GuardException

BotWarning
"""

from __future__ import annotations

__all__ = (
    "RequestTimeoutError",
    "TelegramApiException",
    "BotException",
    "JSONDecodeError",
    "MethodInvokeError",
    "FileDownloadError",
    "FloodError",
    "ChatMigratedError",
    "InternalError",
    "TokenError",
    "WebhookConflictError",
    "FrozenInstanceError",
    "FilterCompatError",
    "DependencyError",
    "BotWarning",
    "GuardException",
)

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from yatbaf.types.update import Update


class BotException(Exception):
    """Base bot exception"""


class InvalidTokenError(BotException):
    """Api token is invalid."""


class FrozenInstanceError(BotException):
    """Object instance is frozen."""


class FilterCompatError(BotException):
    """Filters are incompatible."""


class DependencyError(BotException):
    """Dependency init error."""


class ClientError(BotException):
    """Http client related errors."""


class RequestError(ClientError):

    def __init__(self, orig: Exception) -> None:
        """
        :param orig: Original exception.
        """

        super().__init__(str(orig))
        self.orig = orig


class RequestTimeoutError(RequestError):
    """Request timed out."""


class NetworkError(RequestError):
    """Other request errors."""


class JSONDecodeError(ClientError):
    """Invalid json response."""

    def __init__(self, message: str, raw_content: bytes) -> None:
        """
        :param message: Error message.
        :param raw_content: Response content.
        """

        super().__init__(message)
        self.raw_content = raw_content


class TelegramApiException(Exception):
    """Response status gt 200."""

    def __init__(
        self,
        error_code: int,
        description: str,
        update: Update | None = None,
    ) -> None:
        """
        :param error_code: HTTP status code.
        :param description: Error description.
        :param update: *Optional.* Instance of :class:`~yatbaf.types.update.Update`.
        """  # noqa: E501

        super().__init__(f"{description} [{error_code}]")
        self.error_code = error_code
        self.description = description
        self.update = update


class TokenError(TelegramApiException):
    """Wrong token. Status 401"""


class WebhookConflictError(TelegramApiException):
    """Webhook/LongPolling conflict. Status 409"""


class InternalError(TelegramApiException):
    """Internal server error. Status >= 500"""


class FileDownloadError(TelegramApiException):
    """Response with status > 200."""


class MethodInvokeError(TelegramApiException):
    """Status 400, 403, 429."""


class ChatMigratedError(MethodInvokeError):
    """The group has been migrated to a supergroup."""

    def __init__(self, description: str, migrate_to_chat_id: int) -> None:
        """
        :param description: Error description.
        :param migrate_to_chat_id: Supergroup id.
        """

        super().__init__(400, description)
        self.migrate_to_chat_id = migrate_to_chat_id


class FloodError(MethodInvokeError):
    """Too Many Requests. Status 429"""

    def __init__(self, description: str, retry_after: int) -> None:
        """
        :param description: Error description.
        :param retry_after: The number of seconds left to wait before the
            request can be repeated.
        """

        super().__init__(429, description)
        self.retry_after = retry_after


class BotWarning(UserWarning):
    """Base bot warning."""


class GuardException(Exception):
    pass
