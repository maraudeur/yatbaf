from __future__ import annotations

from typing import final

from .abc import TelegramType


@final
class PreparedInlineMessage(TelegramType):
    """Describes an inline message to be sent by a user of a Mini App.

    See https://core.telegram.org/bots/api#preparedinlinemessage
    """

    id: str
    """Unique identifier of the prepared message."""

    expiration_date: int
    """Expiration date of the prepared message, in Unix time.

    .. note::

        Expired prepared messages can no longer be used.
    """
