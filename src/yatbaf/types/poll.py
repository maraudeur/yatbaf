from __future__ import annotations

from typing import final

from yatbaf.enums import PollType
from yatbaf.typing import NoneInt
from yatbaf.typing import NoneStr

from .abc import TelegramType
from .message_entity import MessageEntity
from .poll_option import PollOption

# from datetime import datetime


@final
class Poll(TelegramType):
    """This object contains information about a poll.

    See: https://core.telegram.org/bots/api#poll
    """

    id: str
    """Unique poll identifier."""

    question: str
    """Poll question, 1-300 characters."""

    options: list[PollOption]
    """List of poll options."""

    total_voter_count: int
    """Total number of users that voted in the poll."""

    is_closed: bool
    """``True``, if the poll is closed."""

    is_anonymous: bool
    """``True``, if the poll is anonymous."""

    type: PollType
    """Poll type."""

    allows_multiple_answers: bool
    """``True``, if the poll allows multiple answers."""

    question_entities: list[MessageEntity] | None = None
    """*Optional.* Special entities that appear in the ``question``. Currently,
    only custom emoji entities are allowed in poll questions.
    """

    correct_option_id: NoneInt = None
    """*Optional.* 0-based identifier of the correct answer option. Available
    only for polls in the quiz mode, which are closed, or was sent
    (not forwarded) by the bot or to the private chat with the bot.
    """

    explanation: NoneStr = None
    """*Optional.* Text that is shown when a user chooses an incorrect answer
    or taps on the lamp icon in a quiz-style poll, 0-200 characters.
    """

    explanation_entities: list[MessageEntity] | None = None
    """*Optional.* Special entities like usernames, URLs, bot commands, etc.
    that appear in the explanation.
    """

    open_period: NoneInt = None
    """*Optional.* Amount of time in seconds the poll will be active after
    creation.
    """

    close_date: NoneInt = None
    """*Optional.* Point in time (Unix timestamp) when the poll will be
    automatically closed.
    """
