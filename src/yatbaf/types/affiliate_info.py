from __future__ import annotations

from typing import final

from yatbaf.typing import NoneInt

from .abc import TelegramType
from .chat import Chat
from .user import User


@final
class AffiliateInfo(TelegramType):
    """Contains information about the affiliate that received a commission via
    this transaction.

    See https://core.telegram.org/bots/api#affiliateinfo
    """

    commission_per_mille: int
    """The number of Telegram Stars received by the affiliate for each 1000
    Telegram Stars received by the bot from referred users.
    """

    amount: int
    """Integer amount of Telegram Stars received by the affiliate from the
    transaction, rounded to 0.

    .. note::

        Can be negative for refunds.
    """

    affiliate_user: User | None = None
    """*Optional.* The bot or the user that received an affiliate commission if
    it was received by a bot or a user.
    """

    affiliate_chat: Chat | None = None
    """*Optional.* The chat that received an affiliate commission if it was
    received by a chat.
    """

    nanostar_amount: NoneInt = None
    """*Optional.* The number of 1/1000000000 shares of Telegram Stars received
    by the affiliate; from -999999999 to 999999999.

    .. note::

        Can be negative for refunds.
    """
