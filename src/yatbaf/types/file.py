from __future__ import annotations

from typing import TYPE_CHECKING
from typing import final

from yatbaf.io import aopen
from yatbaf.typing import NoneInt
from yatbaf.typing import NoneStr

from .abc import TelegramType

if TYPE_CHECKING:
    from collections.abc import AsyncIterator


@final
class File(TelegramType):
    """This object represents a file ready to be downloaded.

    See: https://core.telegram.org/bots/api#file
    """

    file_id: str
    """Identifier for this file, which can be used to download or reuse the file."""  # noqa: E501

    file_unique_id: str
    """Unique identifier for this file, which is supposed to be the same over
    time and for different bots.

    .. warning::

        Can't be used to download or reuse the file.
    """

    file_size: NoneInt = None
    """*Optional.* File size in bytes."""

    file_path: NoneStr = None
    """*Optional.* File path."""

    def __aiter__(self) -> AsyncIterator[bytes]:
        bot = self.bot
        if bot.config.local_mode:
            return self._iter_local_file()
        return bot._api_client.iter_file(self.file_path)

    async def read(self) -> bytes:
        """Returns content of the file."""
        result = b""
        async for data in self:
            result += data
        return result

    async def save(self, path: str, /) -> None:
        """Write the file to disk.

        :param path: Path where the file will be saved.
        """
        async with aopen(path, "wb") as f:
            async for data in self:
                await f.write(data)

    async def _iter_local_file(self) -> AsyncIterator[bytes]:
        chunk_size = 64 * 1024
        async with aopen(self.file_path, "rb") as f:
            data = await f.read(chunk_size)
            while data:
                yield data
                data = await f.read(chunk_size)
