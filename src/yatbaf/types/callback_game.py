from typing import final

from .abc import TelegramType


@final
class CallbackGame(TelegramType):
    """A placeholder, currently holds no information.

    See: https://core.telegram.org/bots/api#callbackgame
    """
