from __future__ import annotations

__all__ = (
    "TelegramType",
    "TelegramTypeFile",
)

from contextvars import ContextVar
from typing import TYPE_CHECKING
from typing import Any

from msgspec import Struct

from yatbaf.state import State

if TYPE_CHECKING:
    from collections.abc import AsyncIterator

    from yatbaf.bot import Bot

_bot_ctx: ContextVar[Bot] = ContextVar("_bot_ctx")


class _BaseSlots:
    """:meta private:"""

    __slots__ = ("__usrctx__",)

    if TYPE_CHECKING:
        __usrctx__: dict[str, Any]


class TelegramType(_BaseSlots, Struct, omit_defaults=True):
    """Base class for Telegram types"""

    def __str__(self) -> str:  # pragma: no cover
        return self.__class__.__name__.lower()

    @property
    def _objdata(self) -> dict[str, Any]:
        """:meta private:"""
        try:
            return self.__usrctx__
        except AttributeError:
            obj: dict[str, Any] = {}
            self.__usrctx__ = obj
            return obj

    @property
    def ctx(self) -> State:
        """Request context.

        Use it to share data between middleware/handler in request context::

            @group.middleware
            def middleware(handler):
                async def wrapper(update):
                    update.ctx.foo = "bar"
                    await handler(update)
                return wrapper

            @group.guard
            async def guard(update):
                update.ctx.foo += "baz"

            @group
            async def handler(update):
                assert update.ctx.foo == "barbaz"
        """
        try:
            return self._objdata["ctx"]  # type: ignore[no-any-return]
        except KeyError:
            self._objdata["ctx"] = ctx = State()
            return ctx

    @property
    def bot(self) -> Bot:
        """Bot instance.

        Use to get the current :class:`~yatbaf.bot.Bot` instance inside
        the handler::

            @on_message
            async def handler(message: Message) -> None:
                bot = message.bot
                ...
        """

        try:
            return _bot_ctx.get()
        except LookupError:
            raise RuntimeError("Bot not bound to this instance.") from None

    @staticmethod
    def _bind_bot_obj(bot: Bot) -> None:
        _bot_ctx.set(bot)


class TelegramTypeFile(TelegramType):
    """Base class for models with files."""
    if TYPE_CHECKING:
        file_id: str

    async def __aiter__(self) -> AsyncIterator[bytes]:
        file = await self.bot.get_file(self.file_id)
        async for data in file:
            yield data

    async def read(self) -> bytes:
        """Returns content of the file."""
        return await self.bot.read_file(self.file_id)

    async def save_file(self, path: str, /) -> None:
        """Write the file to disk.

        :param path: Path where the file will be saved.
        """
        await self.bot.save_file(self.file_id, path)
