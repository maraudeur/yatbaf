from __future__ import annotations

from typing import final

from .abc import TelegramType


@final
class CopyTextButton(TelegramType):
    """This object represents an inline keyboard button that copies specified
    text to the clipboard.

    See: https://core.telegram.org/bots/api#copytextbutton
    """

    text: str
    """The text to be copied to the clipboard; 1-256 characters."""
