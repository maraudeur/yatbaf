from __future__ import annotations

from typing import final

from .abc import TelegramType
from .user import User


@final
class PaidMediaPurchased(TelegramType):
    """This object contains information about a paid media purchase."""

    from_: User
    """User who purchased the media."""

    paid_media_payload: str
    """Bot-specified paid media payload."""
