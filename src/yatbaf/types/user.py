from __future__ import annotations

from typing import TYPE_CHECKING
from typing import final

from yatbaf.helpers.states import UserState
from yatbaf.typing import NoneBool
from yatbaf.typing import NoneInt
from yatbaf.typing import NoneStr

from .abc import TelegramType

if TYPE_CHECKING:
    from yatbaf.enums import ParseMode
    from yatbaf.types import MessageEntity
    from yatbaf.types import UserProfilePhotos


@final
class User(TelegramType):
    """This object represents a Telegram user or bot.

    See: https://core.telegram.org/bots/api#user
    """

    id: int
    """Unique identifier for this user or bot."""

    is_bot: bool
    """``True``, if this user is a bot."""

    first_name: str
    """User's or bot's first name."""

    last_name: NoneStr = None
    """*Optional.* User's or bot's last name."""

    username: NoneStr = None
    """*Optional.* User's or bot's username."""

    language_code: NoneStr = None
    """*Optional.* IETF language tag of the user's language."""

    is_premium: NoneBool = None
    """*Optional.* ``True``, if this user is a Telegram Premium user."""

    added_to_attachment_menu: NoneBool = None
    """
    *Optional.* ``True``, if this user added the bot to the attachment menu.
    """

    can_join_groups: NoneBool = None
    """*Optional.* ``True``, if the bot can be invited to groups.

    .. note::

        Returned only in :meth:`get_me <yatbaf.bot.Bot.get_me>`.
    """

    can_read_all_group_messages: NoneBool = None
    """*Optional.* ``True``, if privacy mode is disabled for the bot.

    .. note::

        Returned only in :meth:`get_me <yatbaf.bot.Bot.get_me>`.
    """

    supports_inline_queries: NoneBool = None
    """*Optional.* ``True``, if the bot supports inline queries.

    .. note::

        Returned only in :meth:`get_me <yatbaf.bot.Bot.get_me>`.
    """

    can_connect_to_business: NoneBool = None
    """*Optional.* ``True``, if the bot can be connected to a Telegram Business
    account to receive its messages.

    .. note::

        Returned only in :meth:`~yatbaf.bot.Bot.get_me`.
    """

    has_main_web_app: NoneBool = None
    """*Optional.* ``True``, if the bot has a main Web App.

    .. note::

        Returned only in :meth:`~yatbaf.bot.Bot.get_me`.
    """

    @property
    def state(self) -> UserState:
        """User state."""
        state = self._objdata.get("state")
        if state is None:
            state = UserState(
                user_id=self.id,
                storage=self.bot.storage.get("state"),
            )
            self._objdata["state"] = state
        return state

    async def get_photos(
        self,
        offset: NoneInt = None,
        limit: NoneInt = None,
    ) -> UserProfilePhotos:
        """Get profile photos.

        See: :meth:`get_user_profile_photos <yatbaf.bot.Bot.get_user_profile_photos>`
        """  # noqa: E501

        return await self.bot.get_user_profile_photos(
            user_id=self.id,
            offset=offset,
            limit=limit,
        )

    async def set_emoji_status(
        self,
        emoji_status_custom_emoji_id: NoneStr = None,
        emoji_status_expiration_date: NoneInt = None,
    ) -> bool:
        """See :meth:`~yatbaf.api.ApiMethods.set_user_emoji_status`"""
        return await self.bot.set_user_emoji_status(
            user_id=self.id,
            emoji_status_custom_emoji_id=emoji_status_custom_emoji_id,
            emoji_status_expiration_date=emoji_status_expiration_date,
        )

    async def send_gift(
        self,
        gift_id: int,
        text: NoneStr = None,
        text_parse_mode: ParseMode | None = None,
        text_entities: list[MessageEntity] | None = None,
    ) -> bool:
        """See :meth:`~yatbaf.api.ApiMethods.send_gift`"""
        return await self.bot.send_gift(
            user_id=self.id,
            gift_id=gift_id,
            text=text,
            text_parse_mode=text_parse_mode,
            text_entities=text_entities,
        )
