from __future__ import annotations

__all__ = (
    "ReactionType",
    "ReactionTypeEmoji",
    "ReactionTypeCustomEmoji",
    "ReactionTypePaid",
)

from typing import ClassVar
from typing import Literal
from typing import TypeAlias
from typing import final

from .abc import TelegramType


@final
class ReactionTypeEmoji(TelegramType, tag="emoji"):
    """The reaction is based on an emoji.

    See: https://core.telegram.org/bots/api#reactiontypeemoji
    """

    emoji: str
    """Reaction emoji."""

    type: ClassVar[Literal["emoji"]] = "emoji"
    """Type of the reaction, always *emoji*"""


@final
class ReactionTypeCustomEmoji(TelegramType, tag="custom_emoji"):
    """The reaction is based on a custom emoji.

    See: https://core.telegram.org/bots/api#reactiontypecustomemoji
    """

    custom_emoji_id: str
    """Custom emoji identifier."""

    type: ClassVar[Literal["custom_emoji"]] = "custom_emoji"
    """Type of the reaction, always *custom_emoji*."""


@final
class ReactionTypePaid(TelegramType, tag="paid"):
    """The reaction is paid.

    See: https://core.telegram.org/bots/api#reactiontypepaid
    """

    type: ClassVar[Literal["paid"]] = "paid"
    """Type of the reaction, always *paid*"""


ReactionType: TypeAlias = (  # yapf: disable
    ReactionTypeEmoji
    | ReactionTypeCustomEmoji
    | ReactionTypePaid
)
"""This object describes the type of a reaction.

See: https://core.telegram.org/bots/api#reactiontype
"""
