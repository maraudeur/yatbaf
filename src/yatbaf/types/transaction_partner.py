from __future__ import annotations

__all__ = (
    "TransactionPartner",
    "TransactionPartnerFragment",
    "TransactionPartnerUser",
    "TransactionPartnerAffiliateProgram",
    "TransactionPartnerTelegramAds",
    "TransactionPartnerTelegramApi",
    "TransactionPartnerOther",
)

from typing import ClassVar
from typing import TypeAlias
from typing import final

from yatbaf.typing import NoneInt
from yatbaf.typing import NoneStr

from .abc import TelegramType
from .affiliate_info import AffiliateInfo
from .paid_media import PaidMedia
from .revenue_withdrawal_state import RevenueWithdrawalState
from .user import User


@final
class TransactionPartnerFragment(TelegramType, tag="fragment"):
    """Describes a withdrawal transaction with Fragment.

    See: https://core.telegram.org/bots/api#transactionpartnerfragment
    """

    withdrawal_state: RevenueWithdrawalState | None = None
    """*Optional.* State of the transaction if the transaction is outgoing."""

    type: ClassVar[str] = "fragment"
    """Type of the transaction partner, always `fragment`."""


@final
class TransactionPartnerUser(TelegramType, tag="user"):
    """Describes a transaction with a user.

    See: https://core.telegram.org/bots/api#transactionpartneruser
    """

    user: User
    """Information about the user."""

    affiliate: AffiliateInfo | None = None
    """*Optional.* Information about the affiliate that received a commission
    via this transaction.
    """

    invoice_payload: NoneStr = None
    """*Optional.* Bot-specified invoice payload."""

    subscription_period: NoneInt = None
    """*Optional.* The duration of the paid subscription."""

    paid_media: list[PaidMedia] | None = None
    """*Optional.* Information about the paid media bought by the user."""

    paid_media_payload: NoneStr = None
    """*Optional.* Bot-specified paid media payload."""

    gift: NoneStr = None
    """*Optional.* The gift sent to the user by the bot."""

    type: ClassVar[str] = "user"
    """Type of the transaction partner, always `user`."""


@final
class TransactionPartnerAffiliateProgram(TelegramType, tag="affiliate_program"):
    """Describes the affiliate program that issued the affiliate commission
    received via this transaction.

    See https://core.telegram.org/bots/api#transactionpartneraffiliateprogram
    """

    commission_per_mille: int
    """The number of Telegram Stars received by the bot for each 1000 Telegram
    Stars received by the affiliate program sponsor from referred users.
    """

    sponsor_user: User | None = None
    """*Optional.* Information about the bot that sponsored the affiliate
    program.
    """

    type: ClassVar[str] = "affiliate_program"
    """Type of the transaction partner, always `affiliate_program`."""


@final
class TransactionPartnerTelegramAds(TelegramType, tag="telegram_ads"):
    """Describes a withdrawal transaction to the Telegram Ads platform.

    See: https://core.telegram.org/bots/api#transactionpartnertelegramads
    """

    type: ClassVar[str] = "telegram_ads"
    """Type of the transaction partner, always `telegram_ads`."""


@final
class TransactionPartnerTelegramApi(TelegramType, tag="telegram_api"):
    """Describes a transaction with payment for `paid broadcasting`_.

    See: https://core.telegram.org/bots/api#transactionpartnertelegramapi

    .. _paid broadcasting: https://core.telegram.org/bots/api#paid-broadcasts
    """

    request_count: int
    """The number of successful requests that exceeded regular limits and were
    therefore billed.
    """

    type: ClassVar[str] = "telegram_api"
    """Type of the transaction partner, always `telegram_api`."""


@final
class TransactionPartnerOther(TelegramType, tag="other"):
    """Describes a transaction with an unknown source or recipient.

    See: https://core.telegram.org/bots/api#transactionpartnerother
    """

    type: ClassVar[str] = "other"
    """Type of the transaction partner, always `other`."""


TransactionPartner: TypeAlias = (
    TransactionPartnerFragment
    | TransactionPartnerUser
    | TransactionPartnerAffiliateProgram
    | TransactionPartnerTelegramAds
    | TransactionPartnerTelegramApi
    | TransactionPartnerOther
)
"""This object describes the source of a transaction, or its recipient for
outgoing transactions.

See: https://core.telegram.org/bots/api#transactionpartner
"""
