from __future__ import annotations

from typing import final

from yatbaf.typing import NoneInt

from .abc import TelegramType
from .sticker import Sticker


@final
class Gift(TelegramType):
    """This object represents a gift that can be sent by the bot.

    See https://core.telegram.org/bots/api#gift
    """

    id: str
    """Unique identifier of the gift."""

    sticker: Sticker
    """The sticker that represents the gift."""

    star_count: int
    """The number of Telegram Stars that must be paid to send the sticker."""

    upgrade_star_count: NoneInt = None
    """*Optional.* The number of Telegram Stars that must be paid to upgrade
    the gift to a unique one.
    """

    total_count: NoneInt = None
    """*Optional.* The total number of the gifts of this type that can be sent.

    .. note::

        For limited gifts only.
    """

    remaining_count: NoneInt = None
    """*Optional.* The number of remaining gifts of this type that can be sent.

    .. note:

        For limited gifts only.
    """
