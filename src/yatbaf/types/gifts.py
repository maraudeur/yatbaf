from __future__ import annotations

from typing import final

from .abc import TelegramType
from .gift import Gift


@final
class Gifts(TelegramType):
    """This object represent a list of gifts.

    See https://core.telegram.org/bots/api#gifts
    """

    gifts: list[Gift]
    """The list of gifts."""
