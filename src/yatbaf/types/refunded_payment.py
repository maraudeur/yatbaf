from __future__ import annotations

__all__ = ("RefundedPayment",)

from typing import final

from yatbaf.enums import Currency
from yatbaf.typing import NoneStr

from .abc import TelegramType


@final
class RefundedPayment(TelegramType):
    """This object contains basic information about a refunded payment.

    See: https://core.telegram.org/bots/api#refundedpayment
    """

    currency: Currency
    """Three-letter ISO 4217 currency code, or `XTR` for payments in Telegram
    Stars. Currently, always `XTR`
    """

    total_amount: int
    """Total refunded price in the smallest units of the currency.

    .. important::

        Integer, not float/double.
    """

    invoice_payload: str
    """Bot-specified invoice payload."""

    telegram_payment_charge_id: str
    """Telegram payment identifier."""

    provider_payment_charge_id: NoneStr = None
    """*Optional.* Provider payment identifier."""
