from typing import final

from yatbaf.typing import NoneInt

from .abc import TelegramType


@final
class GiveawayCreated(TelegramType):
    """This object represents a service message about the creation of a
    scheduled giveaway.

    See: https://core.telegram.org/bots/api#giveawaycreated
    """

    prize_star_count: NoneInt = None
    """*Optional.* The number of Telegram Stars to be split between giveaway
    winners.

    .. note::

        For Telegram Star giveaways only.
    """
