from __future__ import annotations

__all__ = (
    "BotConfig",
    "PollingConfig",
    "WebhookConfig",
)

from dataclasses import dataclass
from dataclasses import field
from ipaddress import IPv4Address
from os import getenv
from typing import TYPE_CHECKING
from typing import Literal
from urllib.parse import urlparse

from .utils import extract_bot_id

if TYPE_CHECKING:
    from .enums import Event

URL_ENV = "YATBAF_WEBHOOK_URL"
IP_ENV = "YATBAF_WEBHOOK_IP"
SECRET_ENV = "YATBAF_WEBHOOK_SECRET"


@dataclass(slots=True)
class WebhookConfig:
    url: str = field(default_factory=lambda: getenv(URL_ENV, ""))
    ip: str | None = field(default_factory=lambda: getenv(IP_ENV))
    secret: str | None = field(default_factory=lambda: getenv(SECRET_ENV))
    max_connections: int | None = None

    set_on_startup: bool = True
    force_set: bool = False
    delete_on_shutdown: bool = False

    runner_port: int | None = None

    path: str | None = field(init=False)
    port: int = field(init=False, default=8443)

    def __post_init__(self) -> None:
        self._parse_url()

        if self.force_set:
            self.set_on_startup = True

    def _parse_url(self) -> None:
        url = self.url
        if not url:
            raise ValueError("webhook url not set")

        if not url.startswith("https://") and not url.startswith("http://"):
            url = url.removeprefix("//")
            self.url = f"https://{url}"

        res = urlparse(self.url)
        if self.ip is None:
            try:
                self.ip = str(IPv4Address(res.hostname))
            except Exception:
                pass

        self.port = res.port or self.port
        self.path = res.path or None
        self.url = f"{res.scheme}://{res.hostname}:{self.port}{res.path}"


@dataclass(slots=True)
class PollingConfig:
    timeout: float = 65.0
    limit: int | None = None


@dataclass(slots=True)
class BotConfig:
    token: str
    id: int = field(init=False)
    allowed_updates: list[Event] | None = None
    drop_pending_updates: bool = False
    local_mode: bool = False
    test_environment: bool = False
    webhook: WebhookConfig | None = None
    polling: PollingConfig = field(default_factory=PollingConfig)
    runner: Literal["webhook", "polling", "?"] = field(init=False, default="?")

    def __post_init__(self) -> None:
        self.id = extract_bot_id(self.token)
