from __future__ import annotations

from typing import TYPE_CHECKING
from typing import final

from .abc import TelegramMethod

if TYPE_CHECKING:
    from yatbaf.typing import NoneInt
    from yatbaf.typing import NoneStr


@final
class UnpinChatMessage(TelegramMethod[bool]):
    """See :meth:`yatbaf.bot.Bot.unpin_chat_message`"""

    chat_id: str | int
    message_id: NoneInt = None
    business_connection_id: NoneStr = None
