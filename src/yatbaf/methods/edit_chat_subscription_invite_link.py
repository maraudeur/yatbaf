from __future__ import annotations

from typing import TYPE_CHECKING
from typing import final

from yatbaf.types import ChatInviteLink

from .abc import TelegramMethod

if TYPE_CHECKING:
    from yatbaf.typing import NoneStr


@final
class EditChatSubscriptionInviteLink(TelegramMethod[ChatInviteLink]):
    """See :meth:`~yatbaf.bot.Bot.edit_chat_subscription_invite_link`."""

    chat_id: int | str
    invite_link: str
    name: NoneStr = None
