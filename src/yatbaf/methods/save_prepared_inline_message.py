from __future__ import annotations

from typing import TYPE_CHECKING
from typing import final

from yatbaf.types.prepared_inline_message import PreparedInlineMessage

from .abc import TelegramMethod

if TYPE_CHECKING:
    from yatbaf.types.inline_query_result import InlineQueryResult
    from yatbaf.typing import NoneBool


@final
class SavePreparedInlineMessage(TelegramMethod[PreparedInlineMessage]):
    """See :meth:`~yatbaf.api.ApiMethods.save_prepared_inline_message`"""

    user_id: int
    result: InlineQueryResult
    allow_user_chats: NoneBool = None
    allow_bot_chats: NoneBool = None
    allow_group_chats: NoneBool = None
    allow_channel_chats: NoneBool = None
