from __future__ import annotations

from typing import final

from .abc import TelegramMethod


@final
class RemoveChatVerification(TelegramMethod[bool]):
    """See :meth:`~yatbaf.api.ApiMethods.remove_chat_verification`"""

    chat_id: int | str
