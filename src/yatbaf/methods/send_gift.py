from __future__ import annotations

from typing import TYPE_CHECKING
from typing import final

from .abc import TelegramMethod

if TYPE_CHECKING:
    from yatbaf.enums import ParseMode
    from yatbaf.types import MessageEntity
    from yatbaf.typing import NoneBool
    from yatbaf.typing import NoneStr


@final
class SendGift(TelegramMethod[bool]):
    """See :meth:`~yatbaf.api.ApiMethods.send_gift`"""

    user_id: int
    gift_id: int
    pay_for_upgrade: NoneBool = None
    text: NoneStr = None
    text_parse_mode: ParseMode | None = None
    text_entities: list[MessageEntity] | None = None
