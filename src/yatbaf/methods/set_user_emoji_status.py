from __future__ import annotations

from typing import TYPE_CHECKING
from typing import final

from .abc import TelegramMethod

if TYPE_CHECKING:
    from yatbaf.typing import NoneInt
    from yatbaf.typing import NoneStr


@final
class SetUserEmojiStatus(TelegramMethod[bool]):
    """See :meth:`~yatbaf.api.ApiMethods.set_user_emoji_status`"""

    user_id: int
    emoji_status_custom_emoji_id: NoneStr = None
    emoji_status_expiration_date: NoneInt = None
