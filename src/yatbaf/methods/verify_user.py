from __future__ import annotations

from typing import TYPE_CHECKING
from typing import final

from .abc import TelegramMethod

if TYPE_CHECKING:
    from yatbaf.typing import NoneStr


@final
class VerifyUser(TelegramMethod[bool]):
    """See :meth:`~yatbaf.api.ApiMethods.verify_user`"""

    user_id: int
    custom_description: NoneStr = None
