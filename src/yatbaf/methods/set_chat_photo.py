from __future__ import annotations

from typing import TYPE_CHECKING
from typing import final

from .abc import TelegramMethodWithFile

if TYPE_CHECKING:
    from yatbaf.input_file import InputFile


@final
class SetChatPhoto(TelegramMethodWithFile[bool]):
    """See :meth:`yatbaf.bot.Bot.set_chat_photo`"""

    chat_id: str | int
    photo: InputFile
