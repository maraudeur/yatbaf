from __future__ import annotations

from typing import final

from yatbaf.types import Gifts

from .abc import TelegramMethod


@final
class GetAvailableGifts(TelegramMethod[Gifts]):
    """See :meth:`~yatbaf.api.ApiMethods.get_available_gifts`"""
