from __future__ import annotations

from typing import final

from .abc import TelegramMethod


@final
class EditUserStarSubscription(TelegramMethod[bool]):
    """See :meth:`yatbaf.api.ApiMethods.edit_user_star_subscription`"""

    user_id: int
    telegram_payment_charge_id: str
    is_canceled: bool
