from __future__ import annotations

from typing import final

from .abc import TelegramMethod


@final
class RemoveUserVerification(TelegramMethod[bool]):
    """See :meth:`~yatbaf.api.ApiMethods.remove_user_verification`"""

    user_id: int
