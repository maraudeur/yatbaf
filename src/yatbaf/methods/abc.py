from __future__ import annotations

__all__ = (
    "TelegramMethod",
    "TelegramMethodWithFile",
)

from typing import TYPE_CHECKING
from typing import Any
from typing import ClassVar
from typing import Generic
from typing import TypeAlias
from typing import dataclass_transform
from typing import get_args

from msgspec import Struct
from msgspec import field
from msgspec import json as jsonlib
from msgspec import to_builtins

from yatbaf.input_file import InputFile
from yatbaf.typing import ResultT

if TYPE_CHECKING:
    from collections.abc import AsyncIterable

Data: TypeAlias = "bytes | dict[str, bytes]"
File: TypeAlias = "dict[str, tuple[str, AsyncIterable]]"
Encoded: TypeAlias = "tuple[Data, File | None]"

_encoder = jsonlib.Encoder().encode

_struct_opts = {
    "omit_defaults": True,
    "kw_only": True,
}


@dataclass_transform(kw_only_default=True)
class TelegramMethod(Struct, Generic[ResultT], **_struct_opts):
    """Base class for methods"""

    _method: str | None = field(name="method", default=None)
    """:meta private:"""

    __meth_name__: ClassVar[str]
    __meth_result_model__: ClassVar[type]

    def __init_subclass__(cls, **kwargs: Any) -> None:
        super().__init_subclass__(**kwargs)

        # Skip for `Telegram*` subclasses
        if cls.__name__.startswith("Telegram"):
            return

        cls.__meth_name__ = cls.__name__.lower()
        cls.__meth_result_model__ = (
            get_args(cls.__orig_bases__[0])[0]  # type: ignore[attr-defined]
        )

    def __str__(self) -> str:
        return self.__meth_name__

    @classmethod
    def _get_name(cls) -> str:
        return cls.__meth_name__

    @classmethod
    def _get_result_model(cls) -> type[ResultT]:
        return cls.__meth_result_model__

    def _encode_params(self) -> Encoded:
        return (_encoder(self), None)


class TelegramMethodWithFile(TelegramMethod[ResultT]):
    """Base class for methods with file field"""

    def _encode_params(self) -> Encoded:
        files: dict[str, tuple[str, AsyncIterable]] = {}

        def _hook(obj: object) -> Any:
            if isinstance(obj, InputFile):
                files[obj.attach_id] = (obj.name, obj)
                return f"attach://{obj.attach_id}"

            raise NotImplementedError

        data = to_builtins(self, enc_hook=_hook)

        # no files or it's `file_id`
        if not files:
            return (_encoder(data), None)

        for k, v in data.items():
            data[k] = (
                _encoder(v) if not isinstance(v, str) else v.encode("utf_8")
            )
        return (data, files)
