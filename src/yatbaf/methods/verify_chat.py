from __future__ import annotations

from typing import TYPE_CHECKING
from typing import final

from .abc import TelegramMethod

if TYPE_CHECKING:
    from yatbaf.typing import NoneStr


@final
class VerifyChat(TelegramMethod[bool]):
    """See :meth:`~yatbaf.api.ApiMethods.verify_chat`"""

    chat_id: int | str
    custom_description: NoneStr = None
