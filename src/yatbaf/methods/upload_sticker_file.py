from __future__ import annotations

from typing import TYPE_CHECKING
from typing import final

from yatbaf.types import File

from .abc import TelegramMethodWithFile

if TYPE_CHECKING:
    from yatbaf.enums import StickerFormat
    from yatbaf.input_file import InputFile


@final
class UploadStickerFile(TelegramMethodWithFile[File]):
    """See :meth:`yatbaf.bot.Bot.upload_sticker_file`"""

    user_id: int
    sticker: InputFile
    sticker_format: StickerFormat
