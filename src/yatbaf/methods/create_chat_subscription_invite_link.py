from __future__ import annotations

from typing import TYPE_CHECKING
from typing import final

from yatbaf.types import ChatInviteLink

from .abc import TelegramMethod

if TYPE_CHECKING:
    from yatbaf.typing import NoneStr


@final
class CreateChatSubscriptionInviteLink(TelegramMethod[ChatInviteLink]):
    """See :meth:`yatbaf.bot.Bot.create_chat_subscription_invite_link`."""

    chat_id: int | str
    subscription_price: int
    name: NoneStr = None
    subscription_period: int = 2592000
