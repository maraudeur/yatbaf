from __future__ import annotations

__all__ = (
    "parse_command",
    "parse_command_args",
    "extract_bot_id",
    "ensure_unique",
    "wrap_middleware",
    "to_thread",
)

import asyncio
from functools import partial
from typing import TYPE_CHECKING
from typing import ParamSpec
from typing import TypeVar

from .exceptions import InvalidTokenError

if TYPE_CHECKING:
    from collections.abc import Callable
    from collections.abc import Iterable
    from collections.abc import Sequence

    from .typing import ErrorHandler
    from .typing import MiddlewareCallable

T = TypeVar("T")
R = TypeVar("R")
P = ParamSpec("P")


def parse_command(text: str) -> str | None:
    """Parse bot command.

    :param text: Message text.
    """

    if not text.startswith("/") or text == "/":
        return None

    return text.removeprefix("/").split("@")[0].split(maxsplit=1)[0].lower()


def parse_command_args(text: str) -> list[str]:
    """Parse command args.

    :param text: Message with command.
    """

    return sp[-1].split() if len(sp := text.split(maxsplit=1)) == 2 else []


def extract_bot_id(token: str) -> int:
    try:
        return int(token.split(":")[0])
    except ValueError:
        raise InvalidTokenError() from None


def ensure_unique(objs: Iterable[T]) -> list[T]:
    tmp: list[T] = []

    for obj in objs:
        if obj not in tmp:
            tmp.append(obj)
    return tmp


def wrap_middleware(
    fn: Callable[[T], R],
    middleware: Sequence[MiddlewareCallable[Callable[[T], R]]]
) -> Callable[[T], R]:
    result = fn
    for obj in reversed(middleware):
        result = obj(result)
    return result


async def to_thread(fn: Callable[P, R], *args: P.args, **kwargs: P.kwargs) -> R:
    """Run `fn` in a separate thread."""
    loop = asyncio.get_running_loop()
    return await loop.run_in_executor(None, partial(fn, *args, **kwargs))


def get_error_handler(
    handlers: dict[type[Exception], ErrorHandler], exc: Exception
) -> ErrorHandler | None:
    if not handlers:
        return None

    exc_type = type(exc)
    if handler := handlers.get(exc_type):
        return handler

    for cls in exc_type.__mro__:
        if cls in handlers:
            return handlers[cls]

    return None
