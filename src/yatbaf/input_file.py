from __future__ import annotations

__all__ = ("InputFile",)

from io import BytesIO
from typing import TYPE_CHECKING
from typing import TypeAlias
from uuid import uuid4

from yatbaf.io import aopen

if TYPE_CHECKING:
    from collections.abc import AsyncIterable
    from collections.abc import AsyncIterator
    from pathlib import Path

AnyStr: TypeAlias = "str | bytes"


class InputFile:

    __slots__ = (
        "_attach_id",
        "_data",
        "_path",
        "_name",
    )

    CHUNK_SIZE = 64 * 1024

    def __init__(
        self,
        name: str,
        *,
        path: Path | str | None = None,
        content: AnyStr | AsyncIterable[bytes] | None = None,
    ) -> None:
        if content is not None and path is not None:
            raise ValueError("`content` and `path` are mutually exclusive.")
        if content is None and path is None:
            raise ValueError("you must provide `content` or `path`.")

        self._path = path
        if content is not None and isinstance(content, str):
            content = content.encode("utf-8")
        self._data = content
        self._attach_id = uuid4().hex
        self._name = name

    def __aiter__(self) -> AsyncIterator[bytes]:
        if self._path is not None:
            return self._iter_file()

        content = self._data
        if isinstance(content, bytes):
            return self._iter_buff()

        return aiter(content)  # type: ignore[arg-type]

    async def _iter_file(self) -> AsyncIterator[bytes]:
        size = self.CHUNK_SIZE
        async with aopen(self._path, "rb") as f:  # type: ignore[arg-type]
            data = await f.read(size)
            while data:
                yield data
                data = await f.read(size)

    async def _iter_buff(self) -> AsyncIterator[bytes]:
        size = self.CHUNK_SIZE
        buf = BytesIO(self._data)  # type: ignore[arg-type]
        data = buf.read(size)
        while data:
            yield data
            data = buf.read(size)

    @property
    def name(self) -> str:
        return self._name

    @property
    def attach_id(self) -> str:
        return self._attach_id
