from __future__ import annotations

__all__ = (
    "BaseFilter",
    "And",
    "Or",
    "Not",
    "filter",
)

from abc import ABC
from abc import abstractmethod
from collections import defaultdict
from typing import TYPE_CHECKING
from typing import Generic
from typing import Self
from typing import TypeAlias
from typing import final

from yatbaf.exceptions import FilterCompatError
from yatbaf.typing import EventT

if TYPE_CHECKING:
    from collections.abc import Awaitable
    from collections.abc import Callable
    from collections.abc import Sequence

    from yatbaf.typing import FilterCategory
    from yatbaf.typing import FilterMetadata


class BaseFilter(ABC, Generic[EventT]):
    """Base class for filters."""

    __slots__ = ()

    priority: int
    category: FilterCategory

    def __init_subclass__(cls) -> None:
        if not hasattr(cls, "category"):
            cls.category = "content"
        if not hasattr(cls, "priority"):
            cls.priority = 100

    def __len__(self) -> int:
        return 1

    @final
    def __or__(self, other: object) -> BaseFilter[EventT]:
        if not isinstance(other, BaseFilter):
            return NotImplemented
        return Or(self, other)

    @final
    def __and__(self, other: object) -> BaseFilter[EventT]:
        if not isinstance(other, BaseFilter):
            return NotImplemented
        return And(self, other)

    @final
    def __invert__(self) -> BaseFilter[EventT]:
        return Not(self)

    @final
    @classmethod
    def incompat(cls, filter: type[BaseFilter]) -> None:
        """Mark ``filter`` as incompatible with the current one.

        :param filter: Filter object.
        """
        _conflicts_map[cls].add(filter)
        _conflicts_map[filter].add(cls)

    @property
    def metadata(self) -> FilterMetadata:
        return {self.category: (len(self), self.priority)}

    @abstractmethod
    async def check(self, update: EventT) -> bool:
        """Check filter"""


@final
class And(BaseFilter[EventT]):
    __slots__ = ("f1", "f2")

    def __init__(self, f1: BaseFilter[EventT], f2: BaseFilter[EventT]) -> None:
        self.f1 = f1
        self.f2 = f2

    @property
    def metadata(self) -> FilterMetadata:
        return merge_metadata(self.f1.metadata, self.f2.metadata)

    async def check(self, update: EventT) -> bool:
        return await self.f1.check(update) and await self.f2.check(update)


@final
class Or(BaseFilter[EventT]):
    __slots__ = ("f1", "f2")

    def __init__(self, f1: BaseFilter[EventT], f2: BaseFilter[EventT]) -> None:
        self.f1 = f1
        self.f2 = f2

    @property
    def metadata(self) -> FilterMetadata:
        return merge_metadata(self.f1.metadata, self.f2.metadata)

    async def check(self, update: EventT) -> bool:
        return await self.f1.check(update) or await self.f2.check(update)


@final
class Not(BaseFilter[EventT]):
    __slots__ = ("f",)

    def __init__(self, f: BaseFilter[EventT]) -> None:
        self.f = f

    @property
    def metadata(self) -> FilterMetadata:
        return self.f.metadata

    async def check(self, update: EventT) -> bool:
        return not await self.f.check(update)


_conflicts_map: dict[type[BaseFilter], set[type[BaseFilter]]] = defaultdict(set)


def merge_metadata(p1: FilterMetadata, p2: FilterMetadata) -> FilterMetadata:
    """:meta private:"""
    result = {**p1}
    for p2_group, p2_prior in p2.items():
        prior: tuple[int, int | tuple[int, int]]
        if p1_prior := result.get(p2_group):
            max_p1, min_p1 = _unpack_priority(p1_prior[1])
            max_p2, min_p2 = _unpack_priority(p2_prior[1])

            prior = (
                p1_prior[0] + p2_prior[0],
                (max([max_p1, max_p2]), min(min_p1, min_p2)),
            )
        else:
            prior = p2_prior

        result[p2_group] = prior
    return result


def _unpack_priority(value: int | tuple[int, int]) -> tuple[int, int]:
    """:meta private:"""
    if isinstance(value, int):
        return value, value
    return value


def is_compat(filter1: BaseFilter, filter2: BaseFilter) -> None:
    """:meta private:"""
    if isinstance(filter1, And | Or):
        for filter in (filter1.f1, filter1.f2):
            is_compat(filter, filter2)

    if isinstance(filter2, And | Or):
        for filter in (filter2.f1, filter2.f2):
            is_compat(filter1, filter)

    if conflicts := _conflicts_map[type(filter1)]:
        if type(filter2) in conflicts:
            raise FilterCompatError(
                f"{type(filter1)} cannot be used with {type(filter2)}."
            )


def check_compatibility(filters: Sequence[BaseFilter], any_: bool) -> None:
    """:meta private:"""
    for i, target in enumerate(filters, start=1):
        if isinstance(target, Not):
            target = target.f

        # check filters inside `And` or `Or` filters
        if isinstance(target, And | Or):
            check_compatibility((target.f1, target.f2), isinstance(target, Or))

        # do not check compat between filters inside the `Or` filter
        if any_:
            continue

        for filter in filters[i:]:
            if isinstance(filter, Not):
                filter = filter.f
            is_compat(target, filter)


FilterFn: TypeAlias = "Callable[[EventT], Awaitable[bool]]"


class filter(BaseFilter[EventT]):

    __slots__ = (
        "fn",
        "priority",
        "category",
    )

    def __init__(
        self,
        fn: FilterFn[EventT] | None = None,
        /,
        category: FilterCategory = "content",
        priority: int = 100,
    ) -> None:
        self.category = category
        self.priority = priority
        self.fn: FilterFn[EventT] = fn  # type: ignore[assignment]

    def __call__(self, fn: FilterFn[EventT]) -> Self:
        self.fn = fn
        return self

    async def check(self, update: EventT) -> bool:
        return await self.fn(update)
