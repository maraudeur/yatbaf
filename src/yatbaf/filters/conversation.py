from __future__ import annotations

__all__ = (
    "ActiveConversation",
    "ConversationState",
)

from typing import TYPE_CHECKING

from yatbaf.filters import BaseFilter

if TYPE_CHECKING:
    from yatbaf.types import CallbackQuery
    from yatbaf.types import Message


class ConversationState(BaseFilter):
    __slots__ = (
        "_state",
        "priority",
    )

    def __init__(self, state: str, *, priority: int = 100) -> None:
        self._state = state
        self.priority = priority

    async def check(self, update: Message | CallbackQuery) -> bool:
        state = update.state
        if (cache := state.cache) is not None:
            return cache[0] == self._state
        return (await state.get())[0] == self._state


class ActiveConversation(BaseFilter):
    __slots__ = ("priority",)

    def __init__(self, *, priority: int = 2000) -> None:
        self.priority = priority

    async def check(self, update: Message | CallbackQuery) -> bool:
        state = update.state
        if (cache := state.cache) is not None:
            return cache[0] is not None
        return (await state.get())[0] is not None
