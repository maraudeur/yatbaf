from __future__ import annotations

__all__ = ("aopen",)

import os
from contextlib import asynccontextmanager
from typing import IO
from typing import TYPE_CHECKING
from typing import AnyStr

from .utils import to_thread

if TYPE_CHECKING:
    from collections.abc import AsyncGenerator
    from collections.abc import Callable
    from pathlib import Path


class AsyncFile:

    def __init__(self, fp: IO) -> None:
        self._fp = fp

    async def read(self, size: int = -1) -> bytes:
        return await to_thread(self._fp.read, size)

    async def write(self, data: AnyStr) -> int:
        return await to_thread(self._fp.write, data)

    async def seek(self, offset: int, whence: int | None = os.SEEK_SET) -> int:
        return await to_thread(self._fp.seek, offset, whence)

    async def close(self) -> None:
        await to_thread(self._fp.close)


@asynccontextmanager
async def aopen(
    file: Path | str,
    mode: str = "r",
    buffering: int = -1,
    encoding: str | None = None,
    errors: str | None = None,
    newline: str | None = None,
    closefd: bool = True,
    opener: Callable[[str, str], int] | None = None,
) -> AsyncGenerator[AsyncFile, None]:
    fp = await to_thread(
        open,
        file,
        mode=mode,
        buffering=buffering,
        encoding=encoding,
        errors=errors,
        newline=newline,
        closefd=closefd,
        opener=opener,
    )
    async_file = AsyncFile(fp)
    try:
        yield async_file
    finally:
        await async_file.close()
