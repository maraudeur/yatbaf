from __future__ import annotations

__all__ = ("State",)

from collections.abc import MutableMapping
from typing import TYPE_CHECKING
from typing import Any

if TYPE_CHECKING:
    from collections.abc import Iterator


class State(MutableMapping[str, Any]):
    __slots__ = ("_data",)

    def __init__(self, data: dict[str, Any] | None = None) -> None:
        object.__setattr__(self, "_data", data or {})

    def __len__(self) -> int:
        return len(self._data)

    def __iter__(self) -> Iterator[str]:
        return iter(self._data)

    def __getitem__(self, k: str, /) -> Any:
        return self._data[k]

    def __setitem__(self, k: str, v: Any, /) -> None:
        self._data[k] = v

    def __delitem__(self, k: str) -> None:
        del self._data[k]

    def __getattr__(self, k: str) -> Any:
        try:
            return self._data[k]
        except KeyError as e:
            raise AttributeError from e

    def __setattr__(self, k: str, v: Any) -> None:
        self._data[k] = v

    def __delattr__(self, k: str) -> None:
        try:
            del self._data[k]
        except KeyError as e:
            raise AttributeError from e
