from __future__ import annotations

__all__ = ("TelegramClient",)

import logging
from typing import TYPE_CHECKING
from typing import Final
from typing import cast
from typing import final

from yatbaf.exceptions import ChatMigratedError
from yatbaf.exceptions import FileDownloadError
from yatbaf.exceptions import FloodError
from yatbaf.exceptions import InternalError
from yatbaf.exceptions import MethodInvokeError
from yatbaf.exceptions import TokenError
from yatbaf.exceptions import WebhookConflictError

from .http import HttpClient
from .multipart import MultipartStream
from .utils import decode_error
from .utils import decode_response

if TYPE_CHECKING:
    from collections.abc import AsyncIterator
    from typing import Never

    from yatbaf.methods.abc import TelegramMethod
    from yatbaf.typing import NoneBool
    from yatbaf.typing import NoneInt
    from yatbaf.typing import NoneStr
    from yatbaf.typing import ResultT

    from .abc import AbstractClient
    from .models import ResponseOk

log = logging.getLogger(__name__)

SERVER_URL: Final = "https://api.telegram.org"
"""Default api URL"""
CHUNK_SIZE: Final = 64 * 1024


@final
class TelegramClient:
    """Telegram API client."""

    __slots__ = (
        "_client",
        "_chunk_size",
        "_meth_url",
        "_file_url",
    )

    def __init__(
        self,
        token: str,
        *,
        api_url: NoneStr = None,
        test_environment: NoneBool = None,
        client: AbstractClient | None = None,
        chunk_size: NoneInt = None,
    ) -> None:
        """
        :param token: API token.
        :param api_url: *Optional.* Api server address. Default to :attr:`SERVER_URL`.
        :param test_environment: *Optional.* Pass ``True`` to use `Dedicated test environment`_.
        :param client: *Optional.* Http client.
        :param chunk_size: *Optional.* Chunk length in bytes.

        .. _Dedicated test environment: https://core.telegram.org/bots/features#dedicated-test-environment
        """  # noqa: E501
        self._client = HttpClient() if client is None else client
        self._chunk_size = max(chunk_size or CHUNK_SIZE, 0) or CHUNK_SIZE
        base_url = SERVER_URL if api_url is None else api_url
        creds = f"bot{token}" + ("/test" if test_environment else "")
        self._meth_url = f"{base_url}/{creds}"
        self._file_url = f"{base_url}/file/{creds}"

    async def __call__(self, method: TelegramMethod[ResultT]) -> ResultT:
        return (await self.invoke(method)).result

    async def invoke(
        self,
        method: TelegramMethod[ResultT],
        timeout: float | None = None,
    ) -> ResponseOk[ResultT]:
        """Invoke api method.

        See :ref:`methods`

        :param method: :class:`~yatbaf.methods.abc.TelegramMethod` instance.
        :param timeout: *Optional.* Request read/write timeout.
        """
        headers = {"Content-Type": "application/json"}
        data, files = method._encode_params()
        if files:
            content = MultipartStream(data, files)  # type: ignore[arg-type]
            headers["Content-Type"] = content.content_type
        else:
            content = data  # type: ignore[assignment]

        status, result = await self._client.send_post(
            url=f"{self._meth_url}/{method!s}",
            content=content,
            headers=headers,
            timeout=timeout
        )
        if status != 200:
            log.debug(
                f"Response status code: {status}, "
                f"content: {result!r}"
            )
            self._raise_method_status(result)
        return decode_response(method, result)

    async def iter_file(self, file_path: str, /) -> AsyncIterator[bytes]:
        """Download file from Telegram servers.

        :param file_path: Path to file.

        .. note::

            Use :meth:`~yatbaf.api.ApiMethods.get_file` to get file path.
        """
        url = f"{self._file_url}/{file_path}"
        async with self._client.file_stream(url, self._chunk_size) as response:
            status, content = response
            if status != 200:
                self._raise_file_status(b"".join([b async for b in content]))

            async for data in content:
                yield data

    async def close(self) -> None:
        """:meta private:"""
        log.debug("Shutting down")
        await self._client.close()

    @staticmethod
    def _raise_method_status(content: bytes) -> Never:
        """Raise request error. For internal use only.

        :param content: Raw response content.
        """
        resp = decode_error(content)
        match resp.error_code:
            case 400 if resp.parameters is not None:
                new_chat_id = cast("int", resp.parameters.migrate_to_chat_id)
                raise ChatMigratedError(resp.description, new_chat_id)
            case 400 | 403:
                raise MethodInvokeError(resp.error_code, resp.description)
            case 401 | 421:
                raise TokenError(resp.error_code, resp.description)
            case 409:
                raise WebhookConflictError(409, resp.description)
            case 429 if resp.parameters is not None:
                retry_after = cast("int", resp.parameters.retry_after)
                raise FloodError(resp.description, retry_after)
            case _:
                raise InternalError(resp.error_code, resp.description)

    @staticmethod
    def _raise_file_status(content: bytes) -> Never:
        resp = decode_error(content)
        status = resp.error_code
        if status < 500:
            if status in (401, 421):
                raise TokenError(status, resp.description)
            raise FileDownloadError(status, resp.description)
        raise InternalError(status, resp.description)
