from __future__ import annotations

__all__ = (
    "ResponseError",
    "ResponseOk",
    "ApiResponse",
)

from typing import Generic
from typing import TypeVar
from typing import final

from msgspec import Struct

from yatbaf.types import ResponseParameters
from yatbaf.typing import NoneStr

T = TypeVar("T")


class ApiResponse(Struct):
    """Base object for api response.

    See: https://core.telegram.org/bots/api#making-requests
    """

    ok: bool
    """Request status."""


@final
class ResponseOk(ApiResponse, Generic[T]):
    """This object represents successful api response."""

    result: T
    """Result of the query."""

    description: NoneStr = None
    """Human-readable description of the result."""


@final
class ResponseError(ApiResponse):
    """This object represents unsuccessful api response."""

    error_code: int
    """HTTP status code."""

    description: str
    """Human-readable description of the error."""

    parameters: ResponseParameters | None = None
    """See :class:`~yatbaf.types.response_parameters.ResponseParameters`."""
