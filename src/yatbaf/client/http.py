from __future__ import annotations

__all__ = ("HttpClient",)

import logging
from contextlib import asynccontextmanager
from typing import TYPE_CHECKING
from typing import final

import httpx

from yatbaf.exceptions import NetworkError
from yatbaf.exceptions import RequestTimeoutError

from .abc import AbstractClient

if TYPE_CHECKING:
    from collections.abc import AsyncIterable
    from collections.abc import AsyncIterator
    from typing import Final

log = logging.getLogger(__name__)


@final
class HttpClient(AbstractClient):
    """Default http client."""

    __slots__ = (
        "_client",
        "_connect_timeout",
    )

    def __init__(
        self,
        *,
        timeout: float = 8.0,
        connect_timeout: float = 16.0,
    ) -> None:
        """
        :param timeout: *Optional.* Request read/write timeout.
        :param connect_timeout: *Optional.* Request connect timeout.
        """
        self._connect_timeout = connect_timeout
        self._client: Final = httpx.AsyncClient(
            timeout=httpx.Timeout(
                timeout=timeout,
                connect=connect_timeout,
            ),
        )

    async def send_post(
        self,
        url: str,
        content: bytes | AsyncIterable[bytes] | None = None,
        data: dict[str, bytes | str] | None = None,
        files: dict[str, tuple[str, bytes]] | None = None,
        headers: dict[str, str] | None = None,
        *,
        timeout: float | None = None
    ) -> tuple[int, bytes]:
        timeout = httpx.Timeout(  # type: ignore[assignment]
            timeout=timeout,
            connect=self._connect_timeout,
        ) if timeout is not None else httpx.USE_CLIENT_DEFAULT

        try:
            response = await self._client.post(
                url=url,
                content=content,
                data=data,
                files=files,
                headers=headers,
                timeout=timeout,
            )
            return (response.status_code, response.content)
        except httpx.TimeoutException as error:
            raise RequestTimeoutError(error) from None
        except httpx.RequestError as error:
            raise NetworkError(error) from error

    @asynccontextmanager
    async def file_stream(
        self, url: str, chunk_size: int
    ) -> AsyncIterator[tuple[int, AsyncIterator[bytes]]]:
        try:
            async with self._client.stream("GET", url) as r:
                yield (r.status_code, r.aiter_bytes(chunk_size))
        except httpx.TimeoutException as error:
            raise RequestTimeoutError(error) from None
        except httpx.RequestError as error:
            raise NetworkError(error) from error

    async def close(self) -> None:
        log.debug("Shutting down")
        await self._client.aclose()
