from __future__ import annotations

__all__ = ("AbstractClient",)

from abc import ABC
from abc import abstractmethod
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from collections.abc import AsyncIterable
    from collections.abc import AsyncIterator
    from contextlib import AbstractAsyncContextManager


class AbstractClient(ABC):
    """Abstract http client."""

    __slots__ = ()

    @abstractmethod
    async def send_post(
        self,
        url: str,
        content: bytes | AsyncIterable[bytes] | None = None,
        data: dict[str, bytes | str] | None = None,
        files: dict[str, tuple[str, bytes]] | None = None,
        headers: dict[str, str] | None = None,
        *,
        timeout: float | None = None,
    ) -> tuple[int, bytes]:
        """Send POST request.

        :param url: Request URL.
        :param content: *Optional.* Body of the request.
        :param data: *Optional.* Data to send in the body of the request.
        :param files: *Optional.* Files to send in the body of the request.
        :param headers: *Optional.* Headers to send with the request.
        :param timeout: *Optional.* Read/Write timeout.
        """

    @abstractmethod
    def file_stream(
        self,
        url: str,
        chunk_size: int,
    ) -> AbstractAsyncContextManager[tuple[int, AsyncIterator[bytes]]]:
        """Download file content.

        :param url: File URL.
        :param chunk_size: Chunk length in bytes.
        """

    @abstractmethod
    async def close(self) -> None:
        """Close the client."""
