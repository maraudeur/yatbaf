from __future__ import annotations

__all__ = ("MultipartStream",)

from secrets import token_urlsafe
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from collections.abc import AsyncIterable
    from collections.abc import AsyncIterator


def _render_header(name: str, filename: str | None = None) -> bytes:
    fields: str = f'name="{name}"'
    if filename is not None:
        filename.translate({10: "%0A", 13: "%0D", 34: "%22"})
        fields += f'; filename="{filename}"'
    return f"Content-Disposition: form-data; {fields}\r\n\r\n".encode("ascii")


class MultipartStream:

    def __init__(
        self,
        data: dict[str, bytes],
        files: dict[str, tuple[str, AsyncIterable[bytes]]],
    ) -> None:
        self._data = data
        self._files = files
        self._boundary = token_urlsafe(16)

    @property
    def content_type(self) -> str:
        """Value for the Content-Type header."""
        return f"multipart/form-data; boundary={self._boundary}"

    async def __aiter__(self) -> AsyncIterator[bytes]:
        boundary = self._boundary

        for field, value in self._data.items():
            yield f"--{boundary}\r\n".encode("ascii")
            yield _render_header(field)
            yield value
            yield b"\r\n"

        for field, (name, content) in self._files.items():
            yield f"--{boundary}\r\n".encode("ascii")
            yield _render_header(field, name)
            async for chunk in content:
                yield chunk
            yield b"\r\n"

        yield f"--{boundary}--\r\n".encode("ascii")
