from __future__ import annotations

__all__ = ("Bot",)

import logging
from contextlib import AsyncExitStack
from contextlib import asynccontextmanager
from os import getenv
from typing import TYPE_CHECKING
from typing import Any

from .api import ApiMethods
from .client import TelegramClient
from .config import BotConfig
from .config import PollingConfig
from .handlers.base import Result
from .handlers.utils import merge_handlers
from .state import State
from .storage.registry import StorageRegistry
from .utils import get_error_handler

if TYPE_CHECKING:
    from collections.abc import AsyncGenerator
    from collections.abc import Awaitable
    from collections.abc import Callable
    from collections.abc import Sequence

    from .client.abc import AbstractClient
    from .config import WebhookConfig
    from .di import Provide
    from .enums import Event
    from .handlers.base import BaseHandler
    from .methods.abc import TelegramMethod
    from .storage.abc import AbstractStorage
    from .types import Update
    from .typing import ErrorHandler
    from .typing import HandlerGuard
    from .typing import HandlerMiddleware
    from .typing import Lifespan
    from .typing import NoneInt
    from .typing import ResultT

log = logging.getLogger(__name__)


class Bot(ApiMethods):
    """Bot object."""

    __slots__ = (
        "_state",
        "_api_client",
        "_handler",
        "_storage",
        "_lifespan",
        "_config",
        "_on_startup",
        "_on_shutdown",
        "_before_method_call",
        "_before_process_update",
        "_after_process_update",
        "_error_handlers",
    )

    def __init__(  # yapf: disable
        self,
        token: str | None = None,
        handlers: Sequence[BaseHandler[Any]] | None = None,
        middleware: Sequence[HandlerMiddleware[Update]] | None = None,
        guards: Sequence[HandlerGuard[Update]] | None = None,
        dependencies: dict[str, Provide] | None = None,
        state: dict[str, Any] | None = None,
        storage: dict[str, AbstractStorage] | StorageRegistry | None = None,
        lifespan: Lifespan | Sequence[Lifespan] | None = None,
        on_startup: Sequence[Callable[[Bot], Awaitable[None]]] | None = None,
        on_shutdown: Sequence[Callable[[Bot], Awaitable[None]]] | None = None,
        before_method_call: Callable[[TelegramMethod], Awaitable[None]] | None = None,  # noqa: E501
        before_process_update: Callable[[Update], Awaitable[None]] | None = None,  # noqa: E501
        after_process_update: Callable[[Update, Result], Awaitable[None]] | None = None,  # noqa: E501
        error_handlers: dict[type[Exception], ErrorHandler] | None = None,
        allowed_updates: Sequence[Event] | None = None,
        drop_pending_updates: bool = False,
        webhook_config: WebhookConfig | None = None,
        polling_config: PollingConfig | None = None,
        api_url: str | None = None,
        local_mode: bool = False,
        test_environment: bool = False,
        chunk_size: NoneInt = None,
        client: AbstractClient | None = None,
    ) -> None:
        """
        :param token: Bot API token.
        :param handlers: A sequence of :class:`~yatbaf.handlers.base.BaseHandler`.
        :param middleware: *Optional.* A sequence of :class:`~yatbaf.typing.HandlerMiddleware`.
        :param guards: *Optional.* A sequence of :class:`~yatbaf.typing.HandlerGuard`.
        :param dependencies: *Optional.* A mapping of dependency providers.
        :param state: *Optional.* Use it to store extra state on bot instance.
        :param storage: *Optional.* A registry of :class:`~yatbaf.storage.abc.AbstractStorage`.
        :param on_startup: *Optional*. A sequence of callables to run on
            application startup.
        :param on_shutdown: *Optional*. A sequence of callables to run on
            application shutdown.
        :param before_method_call: *Optional*. A callable to run before any method.
        :param before_process_update: *Optional*. A callable to run before handler.
        :param after_process_update: *Optional*. A callable to run after handler.
        :param error_handlers: *Optional*. A mapping of exception to error handler.
        :param allowed_updates: *Optional.* A list of the update types you want
            your bot to receive. See :class:`~yatbaf.enums.Event` for a
            complete list of available update types. Specify an empty list to
            receive all update types except ``chat_member`` (default). If not
            specified, the previous setting will be used.
        :param drop_pending_updates: Pass ``True`` to drop all pending updates.
        :param webhook_config: *Optional.* Webhook configuration.
        :param polling_config: *Optional.* Long polling configuration.
        :param api_url: *Optional.* Api server address. Default to :attr:`~yatbaf.client.telegram.SERVER_URL`.
        :param local_mode: *Optional.* Pass ``True`` if you are using the
            `Local Bot Api`_ with the ``--local`` option.
        :param test_environment: *Optional.* Pass ``True`` to use `Dedicated test environment`_.
        :param chunk_size: *Optional.* Chunk length in bytes.
        :param client: *Optional.* Http client.

        .. _Dedicated test environment: https://core.telegram.org/bots/features#dedicated-test-environment
        .. _Local Bot Api: https://core.telegram.org/bots/features#local-bot-api
        """  # noqa: E501
        if token is None:
            token = getenv("YATBAF_TOKEN", "")

        self._config = BotConfig(
            token=token,
            allowed_updates=(  # yapf: disable
                allowed_updates
                if allowed_updates is None
                else list(allowed_updates)
            ),
            drop_pending_updates=drop_pending_updates,
            local_mode=local_mode,
            test_environment=test_environment,
            webhook=webhook_config,
            polling=polling_config or PollingConfig(),
        )
        self._api_client = TelegramClient(
            token=token,
            api_url=api_url,
            test_environment=test_environment,
            client=client,
            chunk_size=chunk_size,
        )

        handler = merge_handlers(handlers or [])
        handler.on_registration(
            dependencies=dependencies,
            middleware=middleware,
            guards=guards,
        )
        self._handler = handler
        self._state = State(state)
        self._storage = (  # yapf: disable
            storage if isinstance(storage, StorageRegistry)
            else StorageRegistry(storage)
        )
        self._lifespan = lifespan
        self._on_startup = list(on_startup or [])
        self._on_shutdown = list(on_shutdown or [])
        self._before_method_call = before_method_call
        self._before_process_update = before_process_update
        self._after_process_update = after_process_update
        self._error_handlers = error_handlers or {}

    def __repr__(self) -> str:  # pragma: no cover
        return f"<Bot[id={self._config.id}]>"

    @property
    def config(self) -> BotConfig:
        """Bot config."""
        return self._config

    @property
    def storage(self) -> StorageRegistry:
        """Registry of :class:`~yatbaf.storage.abc.AbstractStorage`."""
        return self._storage

    @property
    def state(self) -> State:
        """Use it to store extra state on bot instance."""
        return self._state

    @asynccontextmanager
    async def lifespan(self) -> AsyncGenerator[None, None]:
        lifespan = self._lifespan
        if lifespan is None:
            lifespan = []
        elif callable(lifespan):
            lifespan = [lifespan]
        try:
            for fn in self._on_startup:
                await fn(self)

            async with AsyncExitStack() as stack:
                for ctx in lifespan:
                    await stack.enter_async_context(ctx(self))
                yield

            for fn in self._on_shutdown:
                await fn(self)
        finally:
            await self.shutdown()

    def run(self) -> None:
        """Run long polling."""
        from yatbaf.runner.long_polling import LongPolling
        LongPolling(self).start()

    def run_webhook(self) -> None:
        """Run webhook."""
        from yatbaf.runner.webhook import Webhook
        Webhook(self).start()

    async def process_update(self, update: Update, /) -> Result:
        """Process incoming update.

        :param update: :class:`~yatbaf.types.update.Update` instance.
        """
        try:
            update._bind_bot_obj(self)
            if (before_hook := self._before_process_update) is not None:
                await before_hook(update)

            result = await self._handler.handle(update)

            if (after_hook := self._after_process_update) is not None:
                await after_hook(update, result)

        # do not propagate handler exceptions to polling/webhook runner
        except Exception as error:
            if hasattr(error, "update"):
                error.update = update
            await self._on_error(error, _default_error_handler)
            result = Result()

        return result

    async def _on_error(self, exc: Exception, default: ErrorHandler) -> None:
        error_handler = get_error_handler(self._error_handlers, exc)
        if error_handler is None:
            error_handler = default

        try:
            await error_handler(self, exc)
        except Exception:
            pass

    async def shutdown(self) -> None:
        """Cleanup resources."""
        await self._api_client.close()

    async def _call(self, method: TelegramMethod[ResultT]) -> ResultT:
        if (hook := self._before_method_call) is not None:
            await hook(method)
        return await self._api_client(method)

    async def read_file(self, file_id: str, /) -> bytes:
        """Returns content of the file.

        :param file_id: File id.
        """
        file = await self.get_file(file_id)
        return await file.read()

    async def save_file(self, file_id: str, path: str) -> None:
        """Use it to write the file to disk.

        :param file_id: File id.
        :param path: Path where the file will be saved.
        """
        file = await self.get_file(file_id)
        await file.save(path)


async def _default_error_handler(_: Bot, exc: Exception) -> None:  # noqa: U101
    log.error(str(exc))
