from __future__ import annotations

__all__ = (
    "EventT",
    "ResultT",
    "EventModel",
    "ReplyMarkup",
    "NoneStr",
    "NoneInt",
    "NoneBool",
    "CallableAny",
    "Wrapper",
    "MiddlewareCallable",
    "MiddlewareCallableArgs",
    "HandlerCallback",
    "HandlerCallable",
    "HandlerGuard",
    "HandlerMiddleware",
    "HandlerMiddlewareArgs",
    "HandlerDependency",
    "HandlerPriority",
    "Scope",
    "ErrorHandler",
    "FilterCategory",
    "FilterMetadata",
    "FilterWeight",
    "Lifespan",
)

from typing import TYPE_CHECKING
from typing import Any
from typing import Concatenate
from typing import Literal
from typing import ParamSpec
from typing import TypeAlias
from typing import TypeVar

if TYPE_CHECKING:
    from collections.abc import Awaitable
    from collections.abc import Callable
    from contextlib import AbstractAsyncContextManager

    from .bot import Bot
    from .handlers.base import Result
    from .methods.abc import TelegramMethod
    from .types import BusinessConnection
    from .types import BusinessMessagesDeleted
    from .types import CallbackQuery
    from .types import ChatBoostRemoved
    from .types import ChatBoostUpdated
    from .types import ChatJoinRequest
    from .types import ChatMemberUpdated
    from .types import ChosenInlineResult
    from .types import ForceReply
    from .types import InlineKeyboardMarkup
    from .types import InlineQuery
    from .types import Message
    from .types import MessageReactionCountUpdated
    from .types import MessageReactionUpdated
    from .types import PaidMediaPurchased
    from .types import Poll
    from .types import PollAnswer
    from .types import PreCheckoutQuery
    from .types import ReplyKeyboardMarkup
    from .types import ReplyKeyboardRemove
    from .types import ShippingQuery
    from .types import Update
    from .types.abc import TelegramType

F = TypeVar("F", bound="Callable")
T = TypeVar("T")
P = ParamSpec("P")

EventT = TypeVar(
    "EventT",
    bound=(
        "Update "
        "| CallbackQuery "
        "| ChatJoinRequest "
        "| ChatMemberUpdated "
        "| InlineQuery "
        "| ChosenInlineResult "
        "| Message "
        "| MessageReactionCountUpdated "
        "| MessageReactionUpdated "
        "| Poll "
        "| PollAnswer "
        "| PreCheckoutQuery "
        "| PaidMediaPurchased "
        "| ShippingQuery "
        "| ChatBoostRemoved "
        "| ChatBoostUpdated "
        "| BusinessConnection "
        "| BusinessMessagesDeleted"
    )
)

ResultT = TypeVar("ResultT", bound="TelegramType | list | int | str | bool")

EventModel: TypeAlias = (
    "CallbackQuery "
    "| ChatJoinRequest "
    "| ChatMemberUpdated "
    "| InlineQuery "
    "| ChosenInlineResult "
    "| Message "
    "| MessageReactionCountUpdated "
    "| MessageReactionUpdated "
    "| Poll "
    "| PollAnswer "
    "| PreCheckoutQuery "
    "| PaidMediaPurchased "
    "| ShippingQuery"
    "| ChatBoostRemoved"
    "| ChatBoostUpdated"
    "| BusinessConnection"
    "| BusinessMessagesDeleted"
)

ReplyMarkup: TypeAlias = (
    "ForceReply "
    "| InlineKeyboardMarkup "
    "| ReplyKeyboardMarkup "
    "| ReplyKeyboardRemove"
)

NoneStr: TypeAlias = "str | None"
NoneInt: TypeAlias = "int | None"
NoneBool: TypeAlias = "bool | None"

CallableAny: TypeAlias = "Callable[..., Any]"
AsyncCallableNone: TypeAlias = "Callable[[T], Awaitable[None]]"
Wrapper: TypeAlias = "Callable[[F], F]"

MiddlewareCallable: TypeAlias = "Callable[[F], F]"
MiddlewareCallableArgs: TypeAlias = "Callable[Concatenate[F, P], F]"

HandlerCallback: TypeAlias = "Callable[Concatenate[EventT, ...], Awaitable[TelegramMethod | None]]"  # noqa: E501
HandlerCallable: TypeAlias = "Callable[[EventT], Awaitable[Result]]"
HandlerGuard: TypeAlias = "Callable[[EventT], Awaitable[None]]"
HandlerMiddleware: TypeAlias = "MiddlewareCallable[HandlerCallable[EventT]]"
HandlerMiddlewareArgs: TypeAlias = "MiddlewareCallableArgs[HandlerCallable[EventT], P]"  # noqa: E501
HandlerDependency: TypeAlias = "CallableAny | type[Any]"

Scope: TypeAlias = "Literal['group', 'handler', 'local']"

ErrorHandler: TypeAlias = "Callable[[Bot, Exception], Awaitable[None]]"

FilterWeight: TypeAlias = "tuple[int, tuple[int, int]]"
HandlerPriority: TypeAlias = "tuple[FilterWeight, FilterWeight, FilterWeight]"

Lifespan: TypeAlias = "Callable[[Bot], AbstractAsyncContextManager[None]]"

FilterCategory: TypeAlias = "Literal['content', 'sender', 'chat']"
FilterMetadata: TypeAlias = "dict[FilterCategory, tuple[int, int | tuple[int, int]]]"  # noqa: E501
