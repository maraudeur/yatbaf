from __future__ import annotations

__all__ = ("Memory",)

from time import time
from typing import final

from msgspec import Struct

from .abc import AbstractStorage


class StorageObject(Struct, omit_defaults=True):
    value: bytes
    expires_at: int | None = None

    @property
    def expired(self) -> bool:
        return (ex := self.expires_at) is not None and ex < int(time())

    def set_ttl(self, ttl: int) -> None:
        self.expires_at = int(time()) + ttl


@final
class Memory(AbstractStorage):
    """In-memory storage."""

    __slots__ = ("_data",)

    def __init__(self) -> None:
        self._data: dict[str, StorageObject] = {}

    async def get(
        self,
        key: str,
        ex: int | None = None,
    ) -> bytes | None:
        obj = self._data.get(key)
        if obj is None:
            return None

        if obj.expired:
            del self._data[key]
            return None

        if ex is not None:
            obj.set_ttl(ex)

        return obj.value

    async def set(
        self,
        key: str,
        value: str | bytes,
        ex: int | None = None,
    ) -> None:
        if isinstance(value, str):
            value = value.encode("utf-8")
        obj = StorageObject(value)
        if ex is not None:
            obj.set_ttl(ex)
        self._data[key] = obj

    async def delete(self, key: str) -> None:
        self._data.pop(key, None)

    async def clear(self) -> None:
        self._data.clear()

    async def delete_expired(self) -> None:
        new = {}
        for k, v in self._data.items():
            if not v.expired:
                new[k] = v
        self._data = new
