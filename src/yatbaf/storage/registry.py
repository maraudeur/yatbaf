from __future__ import annotations

__all__ = ("StorageRegistry",)

from typing import TYPE_CHECKING

from .abc import AbstractStorage
from .memory import Memory

if TYPE_CHECKING:
    from collections.abc import Callable


def _default_factory() -> AbstractStorage:
    return Memory()


class StorageRegistry:
    """Registry for :caass:`~yatbaf.storage.abc.AbstractStorage`."""

    __slots__ = (
        "_storage",
        "_default_factory",
    )

    def __init__(
        self,
        storage: dict[str, AbstractStorage] | None = None,
        default_factory: Callable[[], AbstractStorage] | None = None,
    ) -> None:
        """
        :param storage: *Optional.* A registered storages.
        :param default_factory: *Optional.* Used by :meth:`get` to provide
            a storage.
        """
        self._storage = storage or {}
        self._default_factory = default_factory or _default_factory

    def get(self, name: str, /) -> AbstractStorage:
        """Get storage by name. If no such storage, default factory will be
        used to provide the storage.

        :param name: Name of the storage.
        """
        storage = self._storage.get(name)
        if storage is None:
            storage = self._default_factory()
            self._storage[name] = storage
        return storage
