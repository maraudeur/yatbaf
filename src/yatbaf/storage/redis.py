from __future__ import annotations

__all__ = ("RedisStorage",)

from typing import TYPE_CHECKING

from .abc import AbstractStorage

if TYPE_CHECKING:
    from redis.asycnio import Redis


class RedisStorage(AbstractStorage):

    __slots__ = (
        "_redis",
        "_namespace",
    )

    def __init__(
        self,
        redis: Redis,
        namespace: str | None = None,
    ) -> None:
        """
        :param redis: :class:`redis.asyncio.Redis` instance.
        :param namespce: *Optional.* Storage namespace. Default 'yatbaf'.
        """
        self._redis = redis
        self._namespace = namespace if namespace is not None else "yatbaf"
        self._clear = self._redis.register_script(
            b"""
        local cursor = 0

        repeat
            local result = redis.call('SCAN', cursor, 'MATCH', ARGV[1])
            for _, key in ipairs(result[2]) do
                redis.call('UNLINK', key)
            end
            cursor = tonumber(result[1])
        until cursor == 0
        """
        )

    def _make_key(self, key: str, /) -> str:
        return f"{self._namespace}:{key}"

    async def get(
        self,
        key: str,
        ex: int | None = None,
    ) -> bytes | None:
        return await self._redis.getex(self._make_key(key), ex=ex)

    async def set(
        self,
        key: str,
        value: str | bytes,
        ex: int | None = None,
    ) -> None:
        if isinstance(value, str):
            value = value.encode("utf-8")
        await self._redis.set(self._make_key(key), value, ex=ex)

    async def delete(self, key: str) -> None:
        await self._redis.delete(self._make_key(key))

    async def clear(self) -> None:
        await self._clear(keys=[], args=[f"{self._namespace}:*"])
