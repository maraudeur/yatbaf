from __future__ import annotations

__all__ = ("AbstractStorage",)

from abc import ABC
from abc import abstractmethod


class AbstractStorage(ABC):
    """Key-Value storage backend."""

    @abstractmethod
    async def get(
        self,
        key: str,
        ex: int | None = None,
    ) -> bytes | None:
        """Get a value."""

    @abstractmethod
    async def set(
        self,
        key: str,
        value: str | bytes,
        ex: int | None = None,
    ) -> None:
        """Set or update a value."""

    @abstractmethod
    async def delete(self, key: str) -> None:
        """Delete a value."""

    @abstractmethod
    async def clear(self) -> None:
        """Delete all values."""
