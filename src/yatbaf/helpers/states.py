from __future__ import annotations

__all__ = (
    "State",
    "StateData",
    "ChatState",
    "UserState",
    "ConversationState",
)

from typing import TYPE_CHECKING
from typing import NamedTuple

if TYPE_CHECKING:
    from yatbaf.storage.abc import AbstractStorage


class StateData(NamedTuple):
    value: str | None = None
    extra: str | None = None


class State:
    __slots__ = ("_storage",)

    _key: str = "state"

    def __init__(self, storage: AbstractStorage) -> None:
        self._storage = storage

    async def set(self, state: str | None, data: str | None = None) -> None:
        """Set new state.

        :param state: New state. Pass ``None`` to reset.
        :param data: *Optional.* Extra data.
        """
        if state is None:
            await self._storage.delete(self._key)
        else:
            await self._storage.set(
                self._key, self._create_state_value(state, data)
            )

    async def get(self) -> StateData:
        """Get current state."""
        if (value := await self._storage.get(self._key)) is not None:
            return self._parse_state_value(value)
        return StateData()

    @staticmethod
    def _parse_state_value(state: bytes) -> StateData:
        return StateData(*(state.decode("utf-8")).split("=", maxsplit=1))

    @staticmethod
    def _create_state_value(state: str, data: str | None) -> str:
        return state + (f"={data}" if data is not None else "")


class CachedState(State):
    __slots__ = ("_cache",)

    def __init__(self, storage: AbstractStorage) -> None:
        super().__init__(storage)
        self._cache: StateData | None = None

    @property
    def cache(self) -> StateData | None:
        return self._cache

    async def get(self) -> StateData:
        if (data := self._cache) is None:
            data = await super().get()
            self._cache = data
        return data


class UserState(CachedState):
    """See :attr:`User.state <yatbaf.types.user.User.state>`."""
    __slots__ = ("_key",)

    def __init__(self, user_id: str | int, storage: AbstractStorage) -> None:
        super().__init__(storage)
        self._key = f"user:{user_id}"


class ChatState(CachedState):
    """See :attr:`Chat.state <yatbaf.types.chat.Chat.state>`."""
    __slots__ = ("_key",)

    def __init__(self, chat_id: str | int, storage: AbstractStorage) -> None:
        super().__init__(storage)
        self._key = f"chat:{chat_id}"


class ConversationState(CachedState):
    """See :attr:`Message.state <yatbaf.types.message.Message.state>`,
    :attr:`CallbackQuery.state <yatbaf.types.callback_query.CallbackQuery.state>`.
    """  # noqa: E501
    __slots__ = ("_key",)

    def __init__(
        self,
        chat_id: str | int,
        user_id: str | int,
        business_id: str | int | None,
        thread_id: str | int | None,
        storage: AbstractStorage,
    ) -> None:
        super().__init__(storage)
        self._key = f"conv:{chat_id}.{user_id}.{business_id or '-'}.{thread_id or '-'}"  # noqa: E501
