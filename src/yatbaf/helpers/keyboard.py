from __future__ import annotations

__all__ = (
    "KeyboardBuilder",
    "InlineKeyboardBuilder",
)

from abc import ABC
from abc import abstractmethod
from typing import TYPE_CHECKING
from typing import Generic
from typing import Self
from typing import TypeVar

from yatbaf.types import CallbackGame
from yatbaf.types import CopyTextButton
from yatbaf.types import InlineKeyboardButton
from yatbaf.types import InlineKeyboardMarkup
from yatbaf.types import KeyboardButton
from yatbaf.types import KeyboardButtonPollType
from yatbaf.types import KeyboardButtonRequestChat
from yatbaf.types import KeyboardButtonRequestUsers
from yatbaf.types import LoginUrl
from yatbaf.types import ReplyKeyboardMarkup
from yatbaf.types import SwitchInlineQueryChosenChat
from yatbaf.types import WebAppInfo

if TYPE_CHECKING:
    from yatbaf.enums import PollType
    from yatbaf.types import ChatAdministratorRights
    from yatbaf.typing import NoneBool
    from yatbaf.typing import NoneInt
    from yatbaf.typing import NoneStr

K = TypeVar("K", ReplyKeyboardMarkup, InlineKeyboardMarkup)
B = TypeVar("B")


class _Builder(ABC, Generic[K, B]):
    __slots__ = (
        "_current_row",
        "_markup",
        "_keyboard",
    )

    def __init__(self) -> None:
        self._current_row: list[B] = []
        self._markup: list[list[B]] = [self._current_row]
        self._keyboard: K = self._factory(self._markup)

    @abstractmethod
    def _factory(self, markup: list[list[B]], /) -> K:
        pass

    @property
    def row_length(self) -> int:
        """Length of the current row."""
        return len(self._current_row)

    def new_row(self) -> Self:
        """Add a new row."""
        if self._current_row:
            self._current_row = []
            self._markup.append(self._current_row)
        return self

    def build(self) -> K:
        """Build the keyboard."""
        return self._keyboard


class KeyboardBuilder(_Builder[ReplyKeyboardMarkup, KeyboardButton | str]):
    """See :class:`~yatbaf.types.reply_keyboard_markup.ReplyKeyboardMarkup`."""

    __slots__ = ()

    def _factory(
        self, markup: list[list[KeyboardButton | str]], /
    ) -> ReplyKeyboardMarkup:
        return ReplyKeyboardMarkup(markup)

    def opts(
        self,
        is_persistent: NoneBool = None,
        resize_keyboard: NoneBool = None,
        one_time_keyboard: NoneBool = None,
        input_field_placeholder: NoneStr = None,
        selective: NoneBool = None,
    ) -> Self:
        """Set keyboard options.

        :param is_persistent: *Optional.* Requests clients to always show the
            keyboard when the regular keyboard is hidden. Defaults to ``False``,
            in which case the custom keyboard can be hidden and opened with
            a keyboard icon.
        :param resize_keyboard: *Optional.* Requests clients to resize the
            keyboard vertically for optimal fit (e.g., make the keyboard smaller
            if there are just two rows of buttons). Defaults to ``False``, in
            which case the custom keyboard is always of the same height as the
            app's standard keyboard.
        :param one_time_keyboard: *Optional.* Requests clients to hide the
            keyboard as soon as it's been used. The keyboard will still be
            available, but clients will automatically display the usual
            letter-keyboard in the chat - the user can press a special button
            in the input field to see the custom keyboard again.
            Defaults to ``False``.
        :param input_field_placeholder: *Optional.* The placeholder to be shown
            in the input field when the keyboard is active; 1-64 characters
        :param selective: *Optional.* Use this parameter if you want to show
            the keyboard to specific users only.
        """
        self._keyboard.is_persistent = is_persistent
        self._keyboard.resize_keyboard = resize_keyboard
        self._keyboard.one_time_keyboard = one_time_keyboard
        self._keyboard.input_field_placeholder = input_field_placeholder
        self._keyboard.selective = selective
        return self

    def text(self, text: str, /) -> Self:
        """Add the :class:`~yatbaf.types.keyboard_button.KeyboardButton` button.

        :param text: Text of the button. It will be sent as a message when the
            button is pressed.
        """
        self._current_row.append(text)
        return self

    def request_users(
        self,
        text: str,
        reqeust_id: int,
        user_is_bot: NoneBool = None,
        user_is_premium: NoneBool = None,
        max_quantity: NoneInt = None,
        request_name: NoneBool = None,
        request_username: NoneBool = None,
        request_photo: NoneBool = None,
    ) -> Self:
        """Add the :attr:`~yatbaf.types.keyboard_button.KeyboardButton.reqeust_users` button.

        :param text: Text of the button.
        :param reqeust_id: Signed 32-bit identifier of the request that will
            be received back in the :class:`~yatbaf.type.users_shared.UsersShared`
            object. Must be unique within the message.
        :param user_is_bot: *Optional.* Pass ``True`` to request bots, pass
            ``False`` to request regular users. If not specified, no additional
            restrictions are applied.
        :param user_is_premium: *Optional.* Pass ``True`` to request premium
            users, pass ``False`` to request non-premium users. If not
            specified, no additional restrictions are applied.
        :param max_quantity: *Optional.* The maximum number of users to be
            selected; 1-10. Defaults to 1.
        :param request_name: *Optional.* Pass ``True`` to request the users'
            first and last names.
        :param request_username: *Optional.* Pass ``True`` to request the users'
            usernames.
        :param request_photo: *Optional.* Pass ``True`` to request the users'
            photos.
        """  # noqa: E501
        self._current_row.append(
            KeyboardButton(
                text=text,
                request_users=KeyboardButtonRequestUsers(
                    request_id=reqeust_id,
                    user_is_bot=user_is_bot,
                    user_is_premium=user_is_premium,
                    max_quantity=max_quantity,
                    request_name=request_name,
                    request_username=request_username,
                    request_photo=request_photo,
                )
            )
        )
        return self

    def request_chat(
        self,
        text: str,
        request_id: int,
        chat_is_channel: bool,
        chat_is_forum: NoneBool = None,
        chat_has_username: NoneBool = None,
        chat_is_created: NoneBool = None,
        user_administrator_rights: ChatAdministratorRights | None = None,
        bot_administrator_rights: ChatAdministratorRights | None = None,
        bot_is_member: NoneBool = None,
        request_title: NoneBool = None,
        request_username: NoneBool = None,
        request_photo: NoneBool = None,
    ) -> Self:
        """Add the :attr:`~yatbaf.types.keyboard_button.KeyboardButton.request_chat` button.

        :param text: Text of the button.
        :param reqeust_id: Signed 32-bit identifier of the request, which will
            be received back in the :class:`~yatbaf.type.chat_shared.ChatShared`
            object. Must be unique within the message.
        :param chat_is_channel: Pass ``True`` to request a channel
            chat, pass ``False`` to request a group or a supergroup chat.
        :param chat_is_forum: *Optional.* Pass ``True`` to request a forum
            supergroup, pass ``False`` to request a non-forum chat. If not
            specified, no additional restrictions are applied.
        :param chat_has_username: *Optional.* Pass ``True`` to request a
            supergroup or a channel with a username, pass ``False`` to request
            a chat without a username. If not specified, no additional
            restrictions are applied.
        :param chat_is_created: *Optional.* Pass ``True`` to request a chat
            owned by the user. Otherwise, no additional restrictions are applied.
        :param user_administrator_rights: *Optional.* The required administrator
            rights of the user in the chat. The rights must be a superset of
            ``bot_administrator_rights``. If not specified, no additional
            restrictions are applied.
        :param bot_administrator_rights: *Optional.* The required administrator
            rights of the bot in the chat. The rights must be a subset of
            ``user_administrator_rights``. If not specified, no additional
            restrictions are applied.
        :param bot_is_member: *Optional.* Pass ``True`` to request a chat with
            the bot as a member. Otherwise, no additional restrictions are
            applied.
        :param request_title: *Optional.* Pass ``True`` to request the chat's
            title.
        :param request_username: *Optional.* Pass ``True`` to request the chat's
            username.
        :param reqeust_photo: *Optional.* Pass ``True`` to request the chat's
            photo.
        """  # noqa: E501
        self._current_row.append(
            KeyboardButton(
                text=text,
                request_chat=KeyboardButtonRequestChat(
                    request_id=request_id,
                    chat_is_channel=chat_is_channel,
                    chat_is_forum=chat_is_forum,
                    chat_has_username=chat_has_username,
                    chat_is_created=chat_is_created,
                    user_administrator_rights=user_administrator_rights,
                    bot_administrator_rights=bot_administrator_rights,
                    bot_is_member=bot_is_member,
                    request_title=request_title,
                    request_username=request_username,
                    request_photo=request_photo,
                )
            )
        )
        return self

    def request_contact(self, text: str) -> Self:
        """Add the :attr:`~yatbaf.types.keyboard_button.KeyboardButton.request_contact` button.

        :param text: Text of the button.
        """  # noqa: E501
        self._current_row.append(
            KeyboardButton(
                text=text,
                request_contact=True,
            )
        )
        return self

    def request_location(self, text: str) -> Self:
        """Add the :attr:`~yatbaf.types.keyboard_button.KeyboardButton.request_location` button.

        :param text: Text of the button.
        """  # noqa: E501
        self._current_row.append(
            KeyboardButton(
                text=text,
                request_location=True,
            )
        )
        return self

    def request_poll(self, text: str, type: PollType) -> Self:
        """Add the :attr:`~yatbaf.types.keyboard_button.KeyboardButton.request_poll` button.

        :param text: Text of the button.
        :param type: If ``PollType.QUIZ`` is passed, the user will be allowed
            to create only polls in the quiz mode. If ``PollType.REGULAR`` is
            passed, only regular polls will be allowed. Otherwise, the user will
            be allowed to create a poll of any type.
        """  # noqa: E501
        self._current_row.append(
            KeyboardButton(
                text=text,
                request_poll=KeyboardButtonPollType(type=type),
            )
        )
        return self

    def web_app(self, text: str, url: str) -> Self:
        """Add the :attr:`~yatbaf.types.keyboard_button.KeyboardButton.web_app` button.

        :param text: Text of the button.
        :param url: An HTTPS URL of a Web App to be opened with additional data.
        """  # noqa: E501
        self._current_row.append(
            KeyboardButton(
                text=text,
                web_app=WebAppInfo(url=url),
            )
        )
        return self


# yapf: disable
class InlineKeyboardBuilder(_Builder[InlineKeyboardMarkup, InlineKeyboardButton]):  # noqa: E501
    """See :class:`~yatbaf.types.inline_keyboard_markup.InlineKeyboardMarkup`."""  # noqa: E501
    # yapf: enable

    __slots__ = ()

    def _factory(
        self, markup: list[list[InlineKeyboardButton]], /
    ) -> InlineKeyboardMarkup:
        return InlineKeyboardMarkup(markup)

    def callback(self, text: str, data: str | None = None) -> Self:
        """Add the :attr:`~yatbaf.types.inline_keyboard_button.InlineKeyboardButton.callback_data` button.

        :param text: Label text on the button.
        :param data: *Optional.* Data to be sent in a callback query to the bot
            when the button is pressed, 1-64 bytes. Defaults to ``text``.
        """  # noqa: E501
        data = text if data is None else data
        self._current_row.append(
            InlineKeyboardButton(
                text=text,
                callback_data=data,
            )
        )
        return self

    def url(self, text: str, url: str) -> Self:
        """Add the :attr:`~yatbaf.types.inline_keyboard_button.InlineKeyboardButton.url` button.

        :param text: Label text on the button.
        :param url: HTTP or tg:// URL to be opened when the button is pressed.
        """  # noqa: E501
        self._current_row.append(InlineKeyboardButton(text=text, url=url))
        return self

    def app(self, text: str, url: str) -> Self:
        """Add the :attr:`~yatbaf.types.inline_keyboard_button.InlineKeyboardButton.web_app` button.

        :param text: Label text on the button.
        :param url: An HTTPS URL of a Web App.
            See :class:`~yatbaf.types.web_app_info.WebAppInfo`.
        """  # noqa: E501
        self._current_row.append(
            InlineKeyboardButton(
                text=text,
                web_app=WebAppInfo(url=url),
            )
        )
        return self

    def login(
        self,
        text: str,
        url: str,
        forward_text: NoneStr = None,
        bot_username: NoneStr = None,
        request_write_access: NoneBool = None,
    ) -> Self:
        """Add the :attr:`~yatbaf.types.inline_keyboard_button.InlineKeyboardButton.login_url` button.

        :param text: Label text on the button.
        :param url: An HTTPS URL to be opened with user authorization data
            added to the query string when the button is pressed. If the user
            refuses to provide authorization data, the original URL without
            information about the user will be opened.
        :param forward_text: *Optional.* New text of the button in forwarded
            messages.
        :param bot_username: *Optional.* Username of a bot, which will be used
            for user authorization. If not specified, the current bot's username
            will be assumed. The url's domain must be the same as the domain
            linked with the bot.
        :param request_write_access: *Optional.* Pass ``True`` to request the
            permission for your bot to send messages to the user.
        """  # noqa: E501
        self._current_row.append(
            InlineKeyboardButton(
                text=text,
                login_url=LoginUrl(
                    url=url,
                    forward_text=forward_text,
                    bot_username=bot_username,
                    request_write_access=request_write_access,
                ),
            )
        )
        return self

    def inline_query(self, text: str, query: str) -> Self:
        """Add the :attr:`~yatbaf.types.inline_keyboard_button.InlineKeyboardButton.switch_inline_query` button.

        :param text: Label text on the button.
        """  # noqa: E501
        self._current_row.append(
            InlineKeyboardButton(
                text=text,
                switch_inline_query=query,
            )
        )
        return self

    def inline_query_current(self, text: str, query: str) -> Self:
        """Add the :attr:`~yatbaf.types.inline_keyboard_button.InlineKeyboardButton.switch_inline_query_current_chat` button.

        :param text: Label text on the button.
        """  # noqa: E501
        self._current_row.append(
            InlineKeyboardButton(
                text=text,
                switch_inline_query_current_chat=query,
            )
        )
        return self

    def inline_query_chosen(
        self,
        text: str,
        query: str,
        allow_user_chats: NoneBool = None,
        allow_bot_chats: NoneBool = None,
        allow_group_chats: NoneBool = None,
        allow_channel_chats: NoneBool = None,
    ) -> Self:
        """Add the :attr:`~yatbaf.types.inline_keyboard_button.InlineKeyboardButton.switch_inline_query_chosen_chat` button.

        :param text: Label text on the button.
        :pamra allow_user_chats: *Optional.* ``True``, if private chats with
            users can be chosen.
        :param allow_bot_chats: *Optional.* ``True``, if private chats with
            bots can be chosen.
        :param allow_group_chats: *Optional.* ``True``, if group and supergroup
            chats can be chosen.
        :param allow_channel_chats: *Optional.* ``True``, if channel chats can
            be chosen.
        """  # noqa: E501
        self._current_row.append(
            InlineKeyboardButton(
                text=text,
                switch_inline_query_chosen_chat=SwitchInlineQueryChosenChat(
                    query=query,
                    allow_user_chats=allow_user_chats,
                    allow_bot_chats=allow_bot_chats,
                    allow_group_chats=allow_group_chats,
                    allow_channel_chats=allow_channel_chats,
                ),
            )
        )
        return self

    def copy_text(self, text: str, data: str) -> Self:
        """Add the :attr:`~yatbaf.types.inline_keyboard_button.InlineKeyboardButton.copy_text` button.

        :param text: Label text on the button.
        :param data: The text to be copied to the clipboard; 1-256 characters.
        """  # noqa: E501
        self._current_row.append(
            InlineKeyboardButton(
                text=text,
                copy_text=CopyTextButton(text=data,),
            )
        )
        return self

    def game(self, text: str) -> Self:
        """Add the :attr:`~yatbaf.types.inline_keyboard_button.InlineKeyboardButton.callback_game` button.

        :param text: Label text on the button.
        """  # noqa: E501
        self._current_row.append(
            InlineKeyboardButton(
                text=text,
                callback_game=CallbackGame(),
            )
        )
        return self

    def pay(self, text: str) -> Self:
        """Add the :attr:`~yatbaf.types.inline_keyboard_button.InlineKeyboardButton.pay` button.

        :param text: Label text on the button.
        """  # noqa: E501
        self._current_row.append(InlineKeyboardButton(text=text, pay=True))
        return self
